import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Loading from '../../components/Icon/Loading';
import busyAPI from '../../busyAPI';
import PostRecommendationLink from './PostRecommendationLink';
import './PostRecommendation.less';
import SidebarBlock from '../../app/Sidebar/SidebarBlock';

@withRouter
class NewsRecommendation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recommendedPosts  : [],
      loading           : false,
    };

    this.getRecommendations = this.getRecommendations.bind(this);
  }

  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;
    this.getRecommendations();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getRecommendations() {
    this.setState({
      loading: true,
    });
    this.getPostsByAuthor('crystalhim', 1);
   // TODO - Split games/news when there is more news 
   // this.getPostsByAuthor('whaleshares', 3);
   // this.getPostsByAuthor('beyondbitcoin', 1);
  }

  getPostsByAuthor = (author, limit) => {
    busyAPI
      .sendAsync('database_api', 'get_blog_feed', [author, -1, limit])
      .then(result => {
        const posts = result.map(item => item[1]);
        const recommendedPosts = Array.isArray(posts) ? posts : [];
        if (this._isMounted) {
          this.setState({
            recommendedPosts: [...this.state.recommendedPosts, ...recommendedPosts],          
            loading: false,
          });
        }
      });
  };

  getFilteredPosts = () =>
    this.state.recommendedPosts
      .sort((a, b) => {
        if (a.author === 'whaleshares' && b.author === 'beyondbitcoin') return -1;
        if (a.author === 'beyondbitcoin' && b.author === 'whaleshares') return 1;
        return 0;
      })
      .slice(0, 3);

  navigateToPost = () => {
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);
    }
  };

  renderPosts = () => {
    const filteredRecommendedPosts = this.getFilteredPosts();

    return filteredRecommendedPosts.map(post => (
      <PostRecommendationLink
        post                    = {post}
        navigateToPost          = {this.navigateToPost}
        key                     = {post.id}
        haveImage               = {true}
        author                  = {true}
      />
    ));
  };

  render() {
    if (this.state.recommendedPosts.length === 0) return null;

    const { loading }               = this.state;
    const filteredRecommendedPosts  = this.getFilteredPosts();
    const title                     = <FormattedMessage id="latest_news" defaultMessage="What's Happening?" />;

    if (loading) {
      return <Loading />;
    }

    return (
      <SidebarBlock 
        title   = {title}
        icon    = 'alert'
        content = {this.renderPosts()}
      />
    );
  }
}

export default NewsRecommendation;
