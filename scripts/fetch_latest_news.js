/* delete me when it could be dynamic */

const fetch = require('cross-fetch'); // eslint-disable-line global-require

const wlsjs = require('@whaleshares/wlsjs');
wlsjs.api.setOptions({ url: "https://pubrpc.whaleshares.io" });

const rpc_uri = (api, method, params) =>
  new Promise((resolve, reject) => {
    const encodedParams = encodeURIComponent(Buffer.from(JSON.stringify(params)).toString('base64'));
    const url = `https://pubrpc.whaleshares.io/${api}/${method}/${encodedParams}`;
    // console.log(url);
    fetch(url)
      .then(res => {
        if (res.status >= 400) {
          return reject(new Error('Bad response from server'));
        }

        return res.json();
      })
      .then(res => resolve(res.result))
      .catch(err => reject(err));
  });

const doit = async () => {
  let posts = [];

  const postsWls = await rpc_uri('database_api', 'get_blog_feed', [ 'whaleshares', -1, 3 ]);

  // console.log(`postsWls=${JSON.stringify(postsWls)}`);
  for (item of postsWls) {
    posts.push(item[1]);
  }


  let results = [];
  for (const post of posts) {
    post.active_votes = [];
    results.push(post);
  }

  console.log(JSON.stringify(results));

};


doit();
