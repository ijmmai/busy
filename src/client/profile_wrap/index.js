/**
 * Wrap for User and Pod profile page
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getUser,
  getIsUserFailed,
  getIsUserLoaded,
} from '../reducers';
import { getAccount } from '../user/usersActions';
import ErrorFetching from '../statics/ErrorFetching';
import ErrorUserNotRegistered from '../statics/ErrorUserNotRegistered';
import User from '../user/User';
import Pod from '../pod';

@connect(
  (state, ownProps) => ({
    user: getUser(state, ownProps.match.params.name),
    loaded: getIsUserLoaded(state, ownProps.match.params.name),
    failed: getIsUserFailed(state, ownProps.match.params.name),
  }),
  {
    getAccount,
  },
)
class ProfileWrap extends React.Component {
  static propTypes = {
    match: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    loaded: PropTypes.bool,
    failed: PropTypes.bool,
    fetching: PropTypes.bool,
    getAccount: PropTypes.func,
  };

  static defaultProps = {
    authenticatedUserName: '',
    loaded: false,
    failed: false,
    fetching: false,
    getAccount: () => {},
  };

  state = {
    fail_count: 0,
  };

  componentDidMount() {
    const { user } = this.props;
    if (!user.id && !user.failed) {
      this.props.getAccount(this.props.match.params.name);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.name !== this.props.match.params.name) {
      this.props.getAccount(this.props.match.params.name);
    }
  }

  pageLoadFailed = count => {
    const counted = this.state.fail_count + count;
    if (counted < 3) {
      this.setState({ fail_count: counted });
      return null;
    }
    return <ErrorFetching />;
  };

  render() {
    const { loaded, failed, fetching, user } = this.props;
    this.pageLoadFailed = this.pageLoadFailed.bind(this);

    if (failed && !fetching) {
      const pageLoaded = this.pageLoadFailed(1);
      if (pageLoaded !== null) {
        if (user.created === undefined) return <ErrorUserNotRegistered />;

        return pageLoaded;
      }
    }

    if (user) {
      const UserOrPod = user.is_pod ? <Pod {...this.props} /> : <User {...this.props} />;
      return UserOrPod;
    }

    return null;
  }
}

export default ProfileWrap;
