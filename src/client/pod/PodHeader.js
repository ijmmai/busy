import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './PodHeader.less';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

const PodHeader = ({coverImage, hasCover}) => {
  const style = hasCover
    ? {backgroundImage: `url("${WLS_IMG_PROXY}/1024x256/${coverImage}")`}
    : {backgroundImage: 'url("/images/user-header.jpg")'};
  return (
    <div className={classNames('PodHeader', {'PodHeader--cover': hasCover})} style={style}>
      <div className="PodHeader__container">
        <div className="PodHeader__user">
          <div className="PodHeader__row"/>
        </div>
      </div>
    </div>
  );
};

PodHeader.propTypes = {
  coverImage: PropTypes.string,
  hasCover: PropTypes.bool,
};

PodHeader.defaultProps = {
  coverImage: '',
  hasCover: false,
};

export default PodHeader;
