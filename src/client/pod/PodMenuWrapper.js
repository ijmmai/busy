import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import PodMenu from './PodMenu';

@withRouter
class PodMenuWrapper extends React.Component {
  static propTypes = {
    match: PropTypes.shape().isRequired,
    location: PropTypes.shape().isRequired,
    history: PropTypes.shape().isRequired,
  };

  onChange = key => {
    const {match, history} = this.props;
    const section = key === 'all' ? '' : `/${key}`;
    history.push(`${match.url.replace(/\/(news|members|token|about|settings)?$/, '')}${section}`);
  };

  render() {
    const {match, location, history, ...otherProps} = this.props;
    const current = this.props.location.pathname.split('/')[2];
    const currentKey = current || 'all';
    return <PodMenu defaultKey={currentKey} onChange={this.onChange} {...otherProps} />;
  }
}

export default PodMenuWrapper;
