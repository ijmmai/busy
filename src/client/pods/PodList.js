import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { List, Select } from 'antd';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import * as actions from './actions';
import PodCard from './PodCard';
import ReduxInfiniteScroll from "../vendor/ReduxInfiniteScroll";
import Loading from "../components/Icon/Loading";
import { FIXED_TAGS } from "../../common/constants/settings";
import './PodList.less';
import DMCA from '../../common/constants/dmca.json';

const { Option } = Select;

class PodList extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    // pods: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    // getPodsByMembersAction: PropTypes.func.isRequired,
    // loadingMore: PropTypes.bool.isRequired,
    // hasMore: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    if (this.props.pods === false) {
      this.props.fetchPods();
    }
  }

  render() {
    const { user, pods } = this.props;
    const filteredPods = pods && user.pods ? pods.filter(pod => !user.pods.includes(pod.name)) : pods;

    return (
      <div className="shifted">
        <Helmet>
          <title>{this.props.intl.formatMessage({ id: 'podlist', defaultMessage: 'Browse Pods' })}</title>
        </Helmet>
        <div className="layout-twocol container AccList UseList">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body">
            <div className="main-title">
              <div style={{float: 'left'}}>
                <h1>
                  <FormattedMessage id="podlist" defaultMessage="Browse Pods"/>
                </h1>
              </div>
              <div style={{float: 'right'}}>
                <Select defaultValue="" style={{width: '150px'}} onChange={value => this.props.changeTag(value)}>
                  <Option value="">All Interests</Option>
                  {FIXED_TAGS.map(topic => (
                    <Option key={topic} value={topic}>{topic}</Option>
                  ))}
                </Select>
              </div>
              <div style={{content: '', display: 'table', clear: 'both'}}/>
            </div>
            <div className="Pods">
              {filteredPods && (
                <ReduxInfiniteScroll
                  hasMore={this.props.hasMore}
                  loadingMore={this.props.loadingMore}
                  loader={<Loading />}
                  loadMore={this.props.loadMore}
                  elementIsScrollable={false}
                  items={[
                    <List
                      key="podlist_items"
                      grid={{
                        gutter: 16,
                        xs: 1,
                        sm: 1,
                        md: 2,
                        lg: 2,
                        xl: 2,
                        xxl: 2,
                      }}
                      dataSource={filteredPods}
                      renderItem={item => (
                        <List.Item>
                          <PodCard {...item} />
                        </List.Item>
                      )}
                    />
                  ]}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state.auth,
    ...state.pods,
    ...ownProps,
  };
};

const mapDispatchToProps = {
  changeTag: (tag) => async (dispatch) => {
    dispatch({ type: actions.SET, payload: { key: 'tag', value: tag } });
    dispatch(mapDispatchToProps.fetchPods());
  },
  fetchPods: () => async (dispatch, getState, { busyAPI }) => {
    try {
      dispatch({ type: actions.SET, payload: { key: 'loadingMore', value: true } });

      const {tag} = _.get(getState(), 'pods');
      const muted = _.get(DMCA, 'spammers', []);

      let data = [];
      if (tag) { // filter by tag
        data = await busyAPI.sendAsync('database_api', 'get_pods_by_tag', [tag, '', 50]);
      } else { // no filtering
        data = await busyAPI.sendAsync('database_api', 'lookup_pods', ['', 50]);
      }
      const hasMore = data.length >= 50;
      const lastPodName = data[data.length - 1].name;

      data = _.orderBy(data, ['member_count'], 'desc').filter(pod => pod.allow_join)
                                                      .filter(pod => !muted.includes(pod.name));
      dispatch({ type: actions.SET, payload: { key: 'pods', value: data } });
      dispatch({ type: actions.SET, payload: { key: 'lastPodName', value: lastPodName } });
      dispatch({ type: actions.SET, payload: { key: 'hasMore', value: hasMore } });
    } catch (e) {
      // do nothing
      console.log(e);
    } finally {
      dispatch({ type: actions.SET, payload: { key: 'loadingMore', value: false } });
    }
  },
  loadMore: () => async (dispatch, getState, { busyAPI }) => {
    try {
      const podState = _.get(getState(), 'pods');
      let {tag, pods} = podState;

      const muted = _.get(DMCA, 'spammers', []);

      const startPodName = podState.lastPodName;
      let data = [];

      if (tag) { // filter by tag
        data = await busyAPI.sendAsync('database_api', 'get_pods_by_tag', [tag, startPodName, 50 ]);
      } else { // no filtering
        data = await busyAPI.sendAsync('database_api', 'lookup_pods', [startPodName, 50 ]);
      }

      const hasMore = data.length >= 50;
      const lastPodName = data[data.length - 1].name;
      data.shift(); // remove the first item of an array to avoid duplication
      pods = pods.concat(data);
      pods = _.orderBy(pods, ['member_count'], 'desc').filter(pod => pod.allow_join)
                                                      .filter(pod => !muted.includes(pod.name));
      dispatch({ type: actions.SET, payload: { key: 'hasMore', value: hasMore } });
      dispatch({ type: actions.SET, payload: { key: 'lastPodName', value: lastPodName } });
      dispatch({ type: actions.SET, payload: { key: 'pods', value: pods } });
    } catch (e) {
      // do nothing
      console.log(e);
    } finally {
      dispatch({ type: actions.SET, payload: { key: 'loadingMore', value: false } });      
    }
  },
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(injectIntl(PodList));
