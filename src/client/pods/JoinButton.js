import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class JoinButton extends React.Component {
  render() {
    return (
      <button
        className={classNames('Join', 'Follow', 'Follow--secondary')}
      >
        Join
      </button>
    );
  }
}

export default JoinButton;
