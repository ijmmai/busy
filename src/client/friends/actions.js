import { message } from 'antd';
import { sleep } from '../utils/misc';

export const FRIENDS_SET = '@friends/SET';

export const fetchFriends = () => async (dispatch, getState, {busyAPI}) => {
  try {
    const {auth} = getState();
    const username = auth.user.name;
    const data = await busyAPI.sendAsync('database_api', 'get_friends', [username, '', 1000]);
    dispatch({type: FRIENDS_SET, payload: {key: 'friends', value: data}});
  } catch (e) {
    message.error(e.message, 5);
  }
};

export const fetchPendingReceivedRequests = () => async (dispatch, getState, {busyAPI}) => {
  try {
    const {auth} = getState();
    const username = auth.user.name;
    const data = await busyAPI.sendAsync('database_api', 'get_friend_requests_to', [username, '', 20]);
    dispatch({type: FRIENDS_SET, payload: {key: 'pendingReceivedRequests', value: data}});
  } catch (e) {
    message.error(e.message, 5);
  }
};

export const fetchPendingSentRequests = () => async (dispatch, getState, {busyAPI}) => {
  try {
    const {auth} = getState();
    const username = auth.user.name;
    const data = await busyAPI.sendAsync('database_api', 'get_friend_requests_from', [username, '', 20]);
    dispatch({type: FRIENDS_SET, payload: {key: 'pendingSentRequests', value: data}});
  } catch (e) {
    message.error(e.message, 5);
  }
};

export const fetchUserFriends = (username, onSuccess, onError) => async (dispatch, getState, {busyAPI}) => {
  try {
    const data = await busyAPI.sendAsync('database_api', 'get_friends', [username, "", 20]);
    if (onSuccess) onSuccess(data);
  } catch (e) {
    if (onError) onError(e);
  }
};

export const approvalRequest = (toUsername, accepted, onSuccess) => async (dispatch, getState, {steemAPI, Keys}) => {
  try {
    const {auth} = getState();
    const {keys} = Keys.decrypt();
    const username = auth.user.name;
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}friendAction` : keys.postingKey;
    const action = accepted ? [2, {}] : [3, {}];

    await steemAPI.chainLib.broadcast.sendAsync(
      {
        operations: [
          ['friend_action',
            {
              account: username,
              another: toUsername,
              action,
            },
          ],
        ],
        extensions: [],
      },
      [useKey],
    );
    if (accepted) message.success(`Friend request approved`, 7);
    else message.success(`Friend request rejected`, 7);

    if (onSuccess) onSuccess();
  } catch (e) {
    message.error(e.message, 5);
  }
};

export const unfriend = (toUsername, onSuccess) => async (dispatch, getState, {steemAPI, Keys}) => {
  try {
    dispatch({type: FRIENDS_SET, payload: {key: 'submitting', value: true}});
    // await sleep(5000); // sleep 5s for testing only
    const {auth} = getState();
    const {keys} = Keys.decrypt();
    const username = auth.user.name;
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}friendAction` : keys.postingKey;

    await steemAPI.chainLib.broadcast.sendAsync(
      {
        operations: [
          ['friend_action',
            {
              account: username,
              another: toUsername,
              action: [4, {}],
            },
          ],
        ],
        extensions: [],
      },
      [useKey],
    );

    message.success(`User unfriended`, 7);
    if (onSuccess) onSuccess();
  } catch (e) {
    message.error(e.message, 7);
  } finally {
    dispatch({type: FRIENDS_SET, payload: {key: 'submitting', value: false}});
  }
};

export const cancelFriendRequest = (toUsername, onSuccess) => async (dispatch, getState, {steemAPI, Keys}) => {
  try {
    const {auth} = getState();
    const {keys} = Keys.decrypt();
    const username = auth.user.name;
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}friendAction` : keys.postingKey;

    await steemAPI.chainLib.broadcast.sendAsync(
      {
        operations: [
          [
            'friend_action',
            {
              account: username,
              another: toUsername,
              action: [1, {}],
            },
          ],
        ],
        extensions: [],
      },
      [useKey],
    );

    message.success(`Friend request cancelled`, 7);
    if (onSuccess) onSuccess();
  } catch (e) {
    message.error(e.message, 7);
  }
};

export const sendFriendRequest = (toUsername, onSuccess, onError) => async (dispatch, getState, {steemAPI, Keys}) => {
  try {
    dispatch({type: FRIENDS_SET, payload: {key: 'submitting', value: true}});
    // await sleep(5000); // sleep 5s for testing only
    const [toAccount] = await steemAPI.chainLib.api.getAccountsAsync([toUsername]);
    if (!toAccount) {
      message.error('No account found!', 5);
      return;
    }

    const {auth} = getState();
    const {keys} = Keys.decrypt();
    const username = auth.user.name;
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}friendAction` : keys.postingKey;

    await steemAPI.chainLib.broadcast.sendAsync({
        operations: [
          ['friend_action',
            {
              account: username,
              another: toUsername,
              action: [0, {memo: ''}],
            },
          ],
        ],
        extensions: [],
      },
      [useKey],
    );

    message.success(`Friend request sent`, 7);
    if (onSuccess) onSuccess();
  } catch (e) {
    message.error(e.message, 5);
    if (onError) onError(e);
  } finally {
    dispatch({type: FRIENDS_SET, payload: {key: 'submitting', value: false}});
  }
};
