import _ from 'lodash';
import fetch from "isomorphic-fetch";
import { jsonParse } from "./helpers/formatter";

export const isValidRIPEMD160 = (s) => /\b([a-f0-9]{40})\b/.test(s);

/**
 * Get the base64 encoded of the json for signing and submitting request to CNC gateway
 * @param body
 * @returns {string}
 */
export const getBase64JsonContent = (body) => {
  const content = {data: body, mime_type: 'text/markdown', is_base64: false};
  const content_base64 = (new Buffer(JSON.stringify(content))).toString('base64');
  return content_base64;
};

/**
 * Store to CNC network using cnc gateway
 *
 * @param username Whaleshares account name
 * @param sig signature
 * @param content_base64 text content of the post/comment encoded in base64
 */
export const storeCNCContent = async (username, sig, content_base64) => {
  sig = encodeURIComponent(sig);
  const urlPath = `https://whaleshares.io/cnc_gateway/${username}/${sig}`;
  const res = await fetch(urlPath, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({data: content_base64}),
  });
  const res_json = await res.json();

  if (res_json.status === 'error') throw new Error(res_json.message);

  return res_json.data; // hash
};

export const sendAsync = (api, message, params) =>
  new Promise((resolve, reject) => {
    const encodedParams = encodeURIComponent(Buffer.from(JSON.stringify(params)).toString('base64'));
    const url = `https://pubrpc.whaleshares.io/cnc/${api}/${message}/${[encodedParams]}`;
    fetch(url)
      .then(res => {
        if (res.status >= 400) {
          return reject(new Error('Bad response from server'));
        }

        return res.json();
      })
      .then(res => resolve(res.result))
      .catch(err => reject(err));
  });

/**
 * Fetch the content from offchain to comment
 * @param comment is comment object
 */
export const fetchOffChainContent = (comment) => {
  return new Promise((resolve, reject) => {
    const {author, body, json_metadata} = comment;
    const metadata = jsonParse(json_metadata);
    if (author && (_.get(metadata, 'storage', '') === 'cnc') && isValidRIPEMD160(body)) {
      return sendAsync('database_api', 'get_content', [body])
        .then(result => {
          if (result) {
            // {"jsonrpc":"2.0","result":{"k":"c24fa6021175c8116a34ddb8d72493f93b45edff","v":"test data 1","mime_type":"text/markdown","is_base64":false},"id":"0"}
            const {v} = result;
            comment.body = v;
            return resolve(comment);
          }

          return reject(new Error(`error fetching offchain content for ${body}`));
        })
        .catch(err => reject(err));
    }

    return resolve(comment);
  });
};


/**
 * Fetch the content from offchain to comment
 * @param state is state object return from get_state rpc
 */
export const fetchStateOffChainContents = (state) => {
  return new Promise((resolve, reject) => {
    const {content} = state;
    const keys = [];
    const comment_keys = [];
    Object.keys(content).forEach(key => {
      const comment = content[key];
      const {author, body, json_metadata} = comment;
      const metadata = jsonParse(json_metadata);
      if (author && (_.get(metadata, 'storage', '') === 'cnc') && isValidRIPEMD160(body)) {
        keys.push(body);
        comment_keys.push(key);
      }
    });

    if (keys.length <= 0) return resolve(state);

    return sendAsync('database_api', 'get_contents', [keys])
      .then(result => {
        if (result) {
          // { "jsonrpc" : "2.0",
          //   "result" : [{
          //     "k": "c24fa6021175c8116a34ddb8d72493f93b45edff",
          //     "v": "test data 1",
          //     "mime_type": "text/markdown",
          //     "is_base64": false
          //   }, {
          //     "k": "95fe511a0e34171c842fe7f35a436cb2f4ffaf84",
          //     "v": "test-20200309-1",
          //     "mime_type": "text/markdown",
          //     "is_base64": false
          //   }],
          //   "id": "0"
          // }

          result.forEach((item, index) => {
            if (item) {
              const {v} = item;
              state.content[comment_keys[index]].body = v;
            }
          });

          return resolve(state);
        }

        return reject(new Error(`error fetchStateOffChainContents for ${JSON.stringify(keys)}`));
      })
      .catch(err => reject(err));
  });
};


/**
 * Fetch the contents from offchain to comment
 * @param state is state object return from get_discussions_by_* rpc
 */
export const fetchOffChainContents = (comments) => new Promise((resolve, reject) => {
  const keys = [];
  const idxs = [];
  comments.forEach((item, index) => {
    const obj = item instanceof Array ? item[1] : item;
    const {author, body, json_metadata} = obj;
    const metadata = jsonParse(json_metadata);
    if (author && (_.get(metadata, 'storage', '') === 'cnc') && isValidRIPEMD160(body)) {
      keys.push(body);
      idxs.push(index);
    }
  });

  if (keys.length <= 0) return resolve(comments);

  return sendAsync('database_api', 'get_contents', [keys])
    .then(result => {
      if (result) {
        // { "jsonrpc" : "2.0",
        //   "result" : [{
        //     "k": "c24fa6021175c8116a34ddb8d72493f93b45edff",
        //     "v": "test data 1",
        //     "mime_type": "text/markdown",
        //     "is_base64": false
        //   }, {
        //     "k": "95fe511a0e34171c842fe7f35a436cb2f4ffaf84",
        //     "v": "test-20200309-1",
        //     "mime_type": "text/markdown",
        //     "is_base64": false
        //   }],
        //   "id": "0"
        // }

        result.forEach((item, index) => {
          if (item) {
            const {v} = item;
            if ((comments instanceof Array) && (comments[idxs[index]] instanceof Array)) {
              comments[idxs[index]][1].body = v;
            } else {
              comments[idxs[index]].body = v;
            }
          }
        });
        return resolve(comments);
      }

      return reject(new Error(`error fetchOffChainContents for ${JSON.stringify(keys)}`));
    })
    .catch(err => reject(err));
});

export default {
  getBase64JsonContent,
  storeCNCContent,
  sendAsync,
  fetchStateOffChainContents,
  fetchOffChainContents,
};
