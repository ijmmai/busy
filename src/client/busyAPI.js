import fetch from 'isomorphic-fetch';
import { WLS_RPC_URL } from '../common/constants/settings';

const client = {};

let notifications = () => {};

client.subscribe = cb => {
  notifications = cb;
};

/**
 *
 * @param api database_api or follow_api
 * @param message
 * @param params
 * @returns {Promise<any>}
 */
client.sendAsync = (api, message, params) =>
  new Promise((resolve, reject) => {
    const encodedParams = encodeURIComponent(Buffer.from(JSON.stringify(params)).toString('base64'));
    const url = `${WLS_RPC_URL}/${api}/${message}/${encodedParams}`;
    fetch(url)
      .then(res => {
        if (res.status >= 400) {
          return reject(new Error('Bad response from server'));
        }

        return res.json();
      })
      .then(res => {
        notifications(null, res);
        return resolve(res.result);
      })
      .catch(err => reject(err));
  });

export default client;
