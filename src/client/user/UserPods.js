import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import List from '../components/List/List';
import Affix from '../components/Utils/Affix';
// import FollowButton from '../widgets/FollowButton';
// import JoinButton from '../pods/JoinButton';
// import { getPodsByMembers } from '../app/appActions';
import Avatar from '../components/Avatar';
import '../user/UserList.less';
import * as actions from "../pods/actions";

import {getUserPods} from './userActions';

class UserPods extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    // pods: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    // getPodsByMembersAction: PropTypes.func.isRequired,
    // loadingMore: PropTypes.bool.isRequired,
    // hasMore: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: false,
      pods: false,
      hasMore: false,
      loadingMore: false,
    };
  }

  componentDidMount() {
    const { match } = this.props;
    const username = match.params.name;

    const onSuccess = (data) => {
      this.setState({
        pods: data,
        hasMore: (data.length >= 20),
        loadingMore: false,
      });
    };

    const onError = (err) => {
      this.setState({loadingMore: false});
    };

    if ((username !== this.state.username) || (this.state.pods === false)) {
      this.setState({
        username,
        pods: false,
        hasMore: false,
        loadingMore: false,
      });
      this.props.fetchPods(username, onSuccess, onError);
    }
  }

  render() {
    const columns = [
      {
        title: 'Pod',
        dataIndex: 'pod',
        key: 'pod',
        render: (text, record) => (
          <div>
            <span style={{display: "inline-flex"}}>
              <Avatar size={32} username={record.pod}/>
              <Link to={`/@${record.pod}`} style={{paddingLeft: 5}}>{record.pod}</Link>
            </span>
          </div>
        )
      },
      {
        title: 'Info',
        dataIndex: 'info',
        key: 'info',
        render: (text, record) => (
          <small>
            Joined: {record.joined}<br />
            Posts: {record.total_posts}<br />
            Comments: {record.total_comments}<br />
          </small>
        )
      }
    ];

    return (
      <div className="main-body">
        <div className="main-content AccList UseList Pods">
          {this.state.pods &&
          <List data={this.state.pods} columns={columns} loadMore={this.props.loadMore} hasMore={this.state.hasMore} loadingMore={this.state.loadingMore}/>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
  }
};

const mapDispatchToProps = {
  fetchPods: (username, onSuccess, onError) => async (dispatch, getState, { busyAPI }) => {
    try {
      const data = await busyAPI.sendAsync('database_api', 'get_pod_members_by_user', [username, "", 20 ]);
      if (onSuccess) onSuccess(data);
    } catch (e) {
      if (onError) onError(e);
    }
  },
  loadMore: () => async (dispatch, getState, { busyAPI }) => {
    // TODO: implementation
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(UserPods));
