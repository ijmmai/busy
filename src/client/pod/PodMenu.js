import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import { FormattedMessage } from 'react-intl';
import './PodMenu.less';

class PodMenu extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    defaultKey: PropTypes.string,
  };

  static defaultProps = {
    onChange: () => {
    },
    defaultKey: 'discussions',
  };

  constructor(props) {
    super(props);
    this.state = {
      current: props.defaultKey ? props.defaultKey : 'discussions',
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.defaultKey ? nextProps.defaultKey : 'discussions',
    });
  }

  getItemClasses = key => classNames('PodMenu__item', {'PodMenu__item--active': this.state.current === key});

  handleClick = e => {
    const key = e.currentTarget.dataset.key;
    this.setState({current: key}, () => this.props.onChange(key));
  };

  render() {
    return (
      <div className="PodMenu">
        <div className="menu-layout">
          <Scrollbars
            universal
            autoHide
            renderView={({style, ...props}) => <div style={{...style}} {...props} />}
            style={{width: '100%', height: 48}}
          >
            <ul className="PodMenu__menu">
              <li
                className={this.getItemClasses('all')}
                onClick={this.handleClick}
                role="presentation"
                data-key="all"
              >
                <FormattedMessage id="all" defaultMessage="Posts"/>
              </li>
              <li
                className={this.getItemClasses('news')}
                onClick={this.handleClick}
                role="presentation"
                data-key="news"
              >
                <FormattedMessage id="podnews" defaultMessage="News"/>
              </li>
              <li
                className={this.getItemClasses('members')}
                onClick={this.handleClick}
                role="presentation"
                data-key="members"
              >
                <FormattedMessage id="members" defaultMessage="Members"/>
              </li>
              {/*<li
                className={this.getItemClasses('token')}
                onClick={this.handleClick}
                role="presentation"
                data-key="token"
              >
                <FormattedMessage id="token" defaultMessage="Token"/>
              </li>
              <li
                className={this.getItemClasses('about')}
                onClick={this.handleClick}
                role="presentation"
                data-key="about"
              >
                <FormattedMessage id="about" defaultMessage="About"/>
              </li>*/}
            </ul>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

export default PodMenu;
