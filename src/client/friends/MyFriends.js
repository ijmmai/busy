import React from 'react';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import _ from 'lodash';
import { Form, Input, Button, message } from 'antd';
import requiresLogin from '../auth/requiresLogin';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import Avatar from '../components/Avatar';
import '../styles/modules/account_list.less';


import { fetchFriends, unfriend } from './actions';
import { fetchPendingReceivedRequests } from './actions';


@requiresLogin
@connect(
  state => ({
    ...state.friends,
  }),
  {
    fetchFriends,
    unfriend,
  },
)
class MyFriends extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.friends === false) {
      this.props.fetchFriends();
    }
  }

  handleUnfriend = toUsername => {
    const onSuccess = () => {
      this.props.fetchFriends();
    };

    this.props.unfriend(toUsername, onSuccess);
  };

  render() {
    const renderFriendsRow = this.props.friends
      ? this.props.friends.map(item => (
          <tr key={item}>
            <td className="acc-pic">
              <Avatar username={item} size={32} />
            </td>
            <td className="acc-name">
              <Link to={'/@' + item}>@{item}</Link>
            </td>
            <td className="actions">
              <Button
                className="RelAction"
                onClick={() => {
                  this.handleUnfriend(item);
                }}
              >
                <FormattedMessage id="unfriend" defaultMessage="Unfriend" />
              </Button>
            </td>
          </tr>
        ))
      : null;

    const renderFriends = !_.isEmpty(this.props.friends) ? (
      <table>
        <thead>
          <tr>
            <th className="acc-pic" />
            <th className="acc-name">
              <FormattedMessage id="name" defaultMessage="Name" />
            </th>
            <th className="actions">
              <FormattedMessage id="actions" defaultMessage="Actions" />
            </th>
          </tr>
        </thead>
        <tbody>{renderFriendsRow}</tbody>
      </table>
    ) : (
      <div className="message">
        <FormattedMessage id="nofriends" defaultMessage="You have no friends yet." />
      </div>
    );

    return (
      <div className="shifted">
        <Helmet>
          <title>My Friends</title>
        </Helmet>
        <div className="layout-twocol container AccList UseTable">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body MyFriends">
            <div className="main-title">
              <h1>
                <FormattedMessage id="my_friends" defaultMessage="My Friends" />
              </h1>
            </div>
            <div className="main-content">{renderFriends}</div>
          </div>
        </div>
      </div>
    );
  }
}
export default MyFriends;
