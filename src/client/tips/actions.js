import {message} from 'antd';

export const SET = '@tips/SET';

const LIMIT = 20;

export const fetchTipsReceived = () => async (dispatch, getState, { busyAPI }) => {
  try {
    dispatch({type: SET, payload: {key: 'loading', value: true}});

    const {auth} = getState();
    const username = auth.user.name;
    const data = await busyAPI.sendAsync('database_api', 'get_tips_to', [username, -1, LIMIT]);
    const hasMore = (data.length >= LIMIT);

    dispatch({type: SET, payload: {key: 'tips_received', value: data}});
    dispatch({type: SET, payload: {key: 'tips_received_hasMore', value: hasMore}});
  } catch (e) {
    message.error(e.message);
  } finally {
    dispatch({type: SET, payload: {key: 'loading', value: false}});
  }
};

export const fetchTipsReceivedMore = () => async (dispatch, getState, { busyAPI }) => {
  try {
    dispatch({type: SET, payload: {key: 'loading', value: true}});

    const {auth, tips} = getState();
    const username = auth.user.name;

    const {tips_received} = tips;
    const lastIndex = tips_received.length > 0 ? tips_received[tips_received.length - 1][0] : 0;

    const limit = (lastIndex >= LIMIT)? LIMIT : lastIndex;
    let data = await busyAPI.sendAsync('database_api', 'get_tips_to', [username, lastIndex, limit]);
    const hasMore = (data.length >= LIMIT);
    data.shift(); // remove the first item of an array to avoid duplication
    data = tips_received.concat(data);

    dispatch({type: SET, payload: {key: 'tips_received', value: data}});
    dispatch({type: SET, payload: {key: 'tips_received_hasMore', value: hasMore}});
  } catch (e) {
    message.error(e.message);
  } finally {
    dispatch({type: SET, payload: {key: 'loading', value: false}});
  }
};

export const fetchTipsSent = () => async (dispatch, getState, { busyAPI }) => {
  try {
    dispatch({type: SET, payload: {key: 'loading', value: true}});

    const {auth} = getState();
    const username = auth.user.name;
    const data = await busyAPI.sendAsync('database_api', 'get_tips_from', [username, -1, LIMIT]);
    const hasMore = (data.length >= LIMIT);

    dispatch({type: SET, payload: {key: 'tips_sent', value: data}});
    dispatch({type: SET, payload: {key: 'tips_sent_hasMore', value: hasMore}});
  } catch (e) {
    message.error(e.message);
  } finally {
    dispatch({type: SET, payload: {key: 'loading', value: false}});
  }
};

export const fetchTipsSentMore = () => async (dispatch, getState, { busyAPI }) => {
  try {
    dispatch({type: SET, payload: {key: 'loading', value: true}});

    const {auth, tips} = getState();
    const username = auth.user.name;

    const {tips_sent} = tips;
    const lastIndex = tips_sent.length > 0 ? tips_sent[tips_sent.length - 1][0] : 0;

    const limit = (lastIndex >= LIMIT)? LIMIT : lastIndex;
    let data = await busyAPI.sendAsync('database_api', 'get_tips_from', [username, lastIndex, limit]);
    const hasMore = (data.length >= LIMIT);
    data.shift(); // remove the first item of an array to avoid duplication
    data = tips_sent.concat(data);

    dispatch({type: SET, payload: {key: 'tips_sent', value: data}});
    dispatch({type: SET, payload: {key: 'tips_sent_hasMore', value: hasMore}});
  } catch (e) {
    message.error(e.message);
  } finally {
    dispatch({type: SET, payload: {key: 'loading', value: false}});
  }
};
