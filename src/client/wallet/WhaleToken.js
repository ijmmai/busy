import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Form, Input, Select, Modal, Button } from 'antd';
import ScatterJS, { Network } from 'scatterjs-core';
import ScatterEOS from 'scatterjs-plugin-eosjs2';
import { Api, JsonRpc } from 'eosjs';
import { closeWhaletoken, setScatter } from './walletActions';
import { notify } from '../app/Notification/notificationActions';
import { getIsAuthenticated, getAuthenticatedUser, getIsWhaletokensVisible, getScatter } from '../reducers';
import { WhaleTokens, cctis } from '../../common/constants/whaletokens';
import './Transfer.less';
import './WhaleToken.less';

const Option = Select.Option;

const network = Network.fromJson({
  blockchain: 'eos',
  host: 'nodes.get-scatter.com',
  port: 443,
  protocol: 'https',
  chainId: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
});

@injectIntl
@connect(
  state => ({
    visible: getIsWhaletokensVisible(state),
    authenticated: getIsAuthenticated(state),
    user: getAuthenticatedUser(state),
    scatter: getScatter(state),
  }),
  {
    closeWhaletoken,
    notify,
    setScatter,
  },
)
@Form.create()
class WhaleToken extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    visible: PropTypes.bool,
    authenticated: PropTypes.bool.isRequired,
    user: PropTypes.shape().isRequired,
    form: PropTypes.shape().isRequired,
    closeWhaletoken: PropTypes.func,
    notify: PropTypes.func,
    scatter: PropTypes.shape(),
    setScatter: PropTypes.func,
  };

  static defaultProps = {
    visible: false,
    scatter: {},
    closeWhaletoken: () => {},
    notify: () => {},
    setScatter: () => {},
  };

  state = {
    currency: 0,
    oldAmount: 0,
    error: '',
    type: 'redeem',
	  was_visible: false,
  };

  getMemoPlaceholder = () => {
    const { intl, ccti } = this.props;
	  const token_type = ccti ? 'CCTI' : 'WhaleToken';
    switch (this.state.type) {
      case 'redeem':
        return 'Post URL for '+token_type+' Redemption';
      case 'transfer':
        return intl.formatMessage({
          id: 't_memo_placeholder',
          defaultMessage: 'Optional memo',
        });
      case 'issue':
        return intl.formatMessage({
          id: 'i_memo_placeholder',
          defaultMessage: 'Optional memo',
        });
      default:
        return '';
    }
  };

  validateAmount = (rule, value, callback) => {
    const { intl } = this.props;

    if (value && value <= 0) {
      callback([
        new Error(
          intl.formatMessage(
            {
              id: 'amount_error_empty',
              defaultMessage: 'Amount is required',
            },
            {
              username: value,
            },
          ),
        ),
      ]);
      return;
    }
    callback();
  };

  validateMemo = (rule, value, callback) => {
    const { intl } = this.props;

    if (this.state.type === 'redeem' && (!value || !value.startsWith('https://whaleshares.io/'))) {
      callback([
        new Error(
          intl.formatMessage(
            {
              id: 'invalid_post_url',
              defaultMessage: 'Invalid WhaleShares Post URL',
            },
            {
              username: value,
            },
          ),
        ),
      ]);
      return;
    }
    callback();
  };

  validateUsername = (rule, value, callback) => {
    const { intl } = this.props;

    if (!value) {
      callback();
      return;
    }

    if (value.length < 12) {
      callback([
        new Error(
          intl.formatMessage(
            {
              id: 'username_too_short',
              defaultMessage: 'Username {username} is too short',
            },
            {
              username: value,
            },
          ),
        ),
      ]);
      return;
    }
    if (value.length > 12) {
      callback([
        new Error(
          intl.formatMessage(
            {
              id: 'username_too_long',
              defaultMessage: 'Username {username} is too long',
            },
            {
              username: value,
            },
          ),
        ),
      ]);
      return;
    }
    callback();
  };

  handleScatter = (values, sct) => {
    const rpc = new JsonRpc(network.fullhost());
    const requiredFields = { accounts: [network] };
	  const { ccti } = this.props;
	  const tokens = ccti ? cctis : WhaleTokens;
    sct
      .getIdentity(requiredFields)
      .then(() => {
        const account = sct.identity.accounts.find(x => x.blockchain === 'eos');
        const eos = sct.eos(network, Api, { rpc, beta3: true });
        eos
          .transact(
            {
              actions: [
                {
                  account: tokens[this.state.currency].contract,
                  name: 'transfer',
                  authorization: [
                    {
                      actor: account.name,
                      permission: account.authority,
                    },
                  ],
                  data: {
                    from: account.name,
                    to: values.to,
                    quantity: `${parseFloat(values.amount).toFixed(tokens[this.state.currency].precision)} ${
                      tokens[this.state.currency].symbol
                    }`,
                    memo: values.memo ? values.memo : '',
                  },
                },
              ],
            },
            {
              blocksBehind: 3,
              expireSeconds: 30,
            },
          )
          .then(() => {
            this.props.notify(
              this.props.intl.formatMessage({
                id: 'sent',
                defaultMessage: 'Sent',
              }),
              'success',
            );
            this.props.form.resetFields();
            return this.props.closeWhaletoken();
          })
          .catch(error => {
            this.props.notify(
              this.props.intl.formatMessage(
                {
                  id: 'transaction_error',
                  defaultMessage: 'Transaction Error: {error}',
                },
                {
                  error: error.message,
                },
              ),
              'error',
            );
          });
      })
      .catch(error => {
        this.props.notify(
          this.props.intl.formatMessage(
            {
              id: 'transaction_error',
              defaultMessage: 'Transaction Error: {error}',
            },
            {
              error,
            },
          ),
          'error',
        );
      });
  };

  handleContinueClick = app => {
    if (typeof window === 'undefined') return;
    const { form, ccti } = this.props;
	  const tokens = ccti ? cctis : WhaleTokens;
    form.validateFields({ force: true }, async (errors, values) => {
      if (!errors) {
        if (app === 'scatter') {
          let scatter;
          if (this.props.scatter !== null) scatter = this.props.scatter;
          else {
            ScatterJS.plugins(new ScatterEOS());
            scatter = await ScatterJS.scatter.connect('Whaleshares').then(connected => {
              if (!connected) return false;
              const sct = ScatterJS.scatter;
              window.ScatterJS = null;
              this.props.setScatter(sct);
              return sct;
            });
          }
          this.handleScatter(values, scatter);
        } else {
          const ops = {
            network: {
              chain: 'eos',
            },
            actions: [
              {
                account: tokens[this.state.currency].contract,
                name: 'transfer',
                authorization: [{ actor: values.from, permission: 'active' }],
                data: {
                  from: values.from,
                  to: values.to,
                  quantity: `${parseFloat(values.amount).toFixed(tokens[this.state.currency].precision)} ${
                    tokens[this.state.currency].symbol
                  }`,
                  memo: values.memo ? values.memo : '',
                },
              },
            ],
          };
          window.whalevault
            .promiseRequestSignBuffer(
              'WhaleTokens Service',
              `eos:${values.from}`,
              ops,
              'Active',
              'Transfer Whaletoken',
              'tx',
            )
            .then(response => {
              if (!response.success) {
                if (response.message && response.message.error && response.message.error.what) {
                  let strError = response.message.error.what;
                  if (response.message.error.details && response.message.error.details.length > 0) {
                    if (response.message.error.details[0].message)
                      strError += ` :: ${response.message.error.details[0].message}`;
                  }
                  response.message = strError;
                }
                return this.props.notify(
                  this.props.intl.formatMessage(
                    {
                      id: 'transaction_error',
                      defaultMessage: 'Transaction Error: {error}',
                    },
                    {
                      error: response.message,
                    },
                  ),
                  'error',
                );
              }
              this.props.notify(
                this.props.intl.formatMessage({
                  id: 'sent',
                  defaultMessage: 'Sent',
                }),
                'success',
              );
              this.props.form.resetFields();
              return this.props.closeWhaletoken();
            });
        }
      }
    });
  };

  handleCurrencyChange = value => {
    const { form, ccti } = this.props;
	  const tokens = ccti ? cctis : WhaleTokens;
    this.setState({ currency: value }, () =>
      form.setFieldsValue({
        currency: value,
        to: tokens[value].sendTo,
		    amount: tokens[value].fixed ? tokens[value].fixed : ''
      }),
    );
  };

  handleCancelClick = () => this.props.closeWhaletoken();

  handleAmountChange = event => {
    let { value } = event.target;
    const { oldAmount, currency } = this.state;
	  const { ccti } = this.props;
	  const tokens = ccti ? cctis : WhaleTokens;

	  if (tokens[currency].fixed) value = tokens[currency].fixed;
	  
    this.setState({
      oldAmount: tokens[currency].amount.test(value) ? value : oldAmount,
    });
    this.props.form.setFieldsValue({
      amount: tokens[currency].amount.test(value) ? value : oldAmount,
    });
    this.props.form.validateFields(['amount']);
  };

  render() {
    const { intl, visible, authenticated, user, ccti } = this.props;
    const { getFieldDecorator } = this.props.form;
    if (!authenticated) return <div />;
	
    const tokens = ccti ? cctis : WhaleTokens;
    const token_header = ccti ? 'Transfer CCTI' : 'Transfer WhaleTokens';
	  
    if (this.state.was_visible != visible) {
      this.setState({ currency: 0, oldAmount: 0, was_visible: visible }, 
                    () => this.handleCurrencyChange(this.state.currency));
    }

    const currencyPrefix = getFieldDecorator('currency', {
      initialValue: this.state.currency,
    })(
      <Select onChange={this.handleCurrencyChange}>
        {tokens.map((whaletoken, i) => (
          <Option key={whaletoken.symbol} value={i}>
		    {whaletoken.symbol}
          </Option>
        ))}
      </Select>,
    );
    return (
      <Modal
        visible={visible}
        title={token_header}
        onCancel={this.handleCancelClick}
        footer={[
          <Button key="back" className="modal_button_cancel" onClick={this.handleCancelClick}>
            {intl.formatMessage({ id: 'cancel', defaultMessage: 'Cancel' })}
          </Button>,
          <Button
            key="whalevault"
            type="primary"
            loading={false}
            className="modal_button"
            onClick={() => this.handleContinueClick('whalevault')}
          >
            {intl.formatMessage({ id: 'send_wv', defaultMessage: 'Send (WhaleVault)' })}
          </Button>,
          <Button
            key="scatter"
            className="modal_button"
            type="primary"
            loading={false}
            onClick={() => this.handleContinueClick('scatter')}
          >
            {intl.formatMessage({ id: 'send_scatter', defaultMessage: 'Send (Scatter)' })}
          </Button>,
        ]}
      >
        <Form className="Transfer" hideRequiredMark layout="vertical">
          <small>
            {intl.formatMessage({
              id: 'whalevault_required',
              defaultMessage: 'This app requires WhaleVault or Scatter to send tokens',
            })}
          </small>
          <Form.Item style={{ marginBottom: 0, marginTop: 20 }}>
            <Form.Item
              style={{ display: 'inline-block', width: 'calc(50% - 12px)' }}
              label={<FormattedMessage id="token" defaultMessage="Token" />}
            >
              {currencyPrefix}
            </Form.Item>
            <span style={{ display: 'inline-block', width: '24px', textAlign: 'center' }} />
            <Form.Item
              style={{ display: 'inline-block', width: 'calc(50% - 12px)' }}
              label={<FormattedMessage id="transfer_issue" defaultMessage="Action" />}
            >
              <Select
                value={this.state.type}
                onChange={v =>
                  this.setState(
                    { type: v },
                    () =>
                      this.state.type === 'redeem'
                        ? this.props.form.setFieldsValue({
                            to: tokens[this.state.currency].sendTo,
                            amount: tokens[this.state.currency].fixed ? tokens[this.state.currency].fixed : ''
                          })
                        : null,
                  )
                }
              >
                <Option key="redeem" value="redeem">
                  {intl.formatMessage({ id: 'redeem', defaultMessage: 'Redeem' })}
                </Option>
                <Option key="transfer" value="transfer">
                  {intl.formatMessage({ id: 'transfer', defaultMessage: 'Transfer' })}
                </Option>
                {tokens[this.state.currency].voter === user.name && (
                  <Option key="issue" value="issue">
                    {intl.formatMessage({ id: 'issue', defaultMessage: 'Issue' })}
                  </Option>
                )}
              </Select>
            </Form.Item>
          </Form.Item>
          <Form.Item label={<FormattedMessage id="from" defaultMessage="From" />}>
            {getFieldDecorator('from', {
              rules: [
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'from_error_empty',
                    defaultMessage: 'From is required',
                  }),
                },
                { validator: this.validateUsername },
              ],
            })(
              <Input
                type="text"
                placeholder={intl.formatMessage({
                  id: 'from_placeholder',
                  defaultMessage: 'Your EOS account',
                })}
              />,
            )}
          </Form.Item>
          <Form.Item label={<FormattedMessage id="to" defaultMessage="To" />}>
            {getFieldDecorator('to', {
              rules: [
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'to_error_empty',
                    defaultMessage: 'Recipient is required',
                  }),
                },
                { validator: this.validateUsername },
              ],
              initialValue: tokens[this.state.currency].sendTo,
            })(
              <Input
                type="text"
                placeholder={intl.formatMessage({
                  id: 'to_placeholder',
                  defaultMessage: 'Payment recipient',
                })}
                disabled={this.state.type === 'redeem'}
              />,
            )}
          </Form.Item>
          <Form.Item label={<FormattedMessage id="amount" defaultMessage="Amount" />}>
            {getFieldDecorator('amount', {
              trigger: '',
              rules: [
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'amount_error_empty',
                    defaultMessage: 'Amount is required',
                  }),
                },
                {
                  pattern: tokens[this.state.currency].amount,
                  message: intl.formatMessage({
                    id: 'amount_error_format',
                    defaultMessage: `Incorrect format. Use a dot as decimal separator. Use at most ${
                      tokens[this.state.currency]
                    } decimal places.`,
                  }),
                },
                { validator: this.validateAmount },
              ],
		          initialValue: tokens[this.state.currency].fixed ? tokens[this.state.currency].fixed : '',
            })(
              <Input
                disabled={this.state.type === 'redeem' && tokens[this.state.currency].fixed}
                onChange={this.handleAmountChange}
                placeholder={intl.formatMessage({
                  id: 'amount_placeholder',
                  defaultMessage: 'How much do you want to send?',
                })}
              />,
            )}
          </Form.Item>
          <Form.Item label={<FormattedMessage id="memo" defaultMessage="Memo" />}>
            {getFieldDecorator('memo', {
              rules: [{ validator: this.validateMemo }],
            })(<Input.TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder={this.getMemoPlaceholder()} />)}
          </Form.Item>
          {this.state.error.length > 0 && <small>{this.state.error}</small>}
        </Form>
      </Modal>
    );
  }
}

export default WhaleToken;
