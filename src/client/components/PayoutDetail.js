import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl';
import _ from 'lodash';
import WLSDisplay from './Utils/WLSDisplay';
import { calculatePayout } from '../vendor/steemitHelpers';

const AmountWithLabel = ({ id, defaultMessage, nonzero, amount }) =>
  _.isNumber(amount) &&
  (nonzero ? amount !== 0 : true) && (
    <div>
      <FormattedMessage
        id={id}
        defaultMessage={defaultMessage}
        values={{
          amount: <WLSDisplay value={amount} />,
        }}
      />
    </div>
  );

AmountWithLabel.propTypes = {
  id: PropTypes.string.isRequired,
  defaultMessage: PropTypes.string.isRequired,
  nonzero: PropTypes.bool,
  amount: PropTypes.number,
};

AmountWithLabel.defaultProps = {
  nonzero: false,
  amount: 0,
};

const PayoutDetail = ({ intl, post }) => {
  const {
    payoutLimitHit,
    isPayoutDeclined,
    pastPayouts,
  } = calculatePayout(post);

  if (isPayoutDeclined) {
    return <FormattedMessage id="payout_declined" defaultMessage="Payout declined" />;
  }

  return (
    <div>
      {payoutLimitHit && (
        <FormattedMessage
          id="payout_limit_reached"
          defaultMessage="Payout limit reached on this post"
        />
      )}
      <div>
        <AmountWithLabel
          id="total_tips_amount"
          defaultMessage="Total Tips: {amount}"
          amount={pastPayouts}
        />
      </div>
    </div>
  );
};

PayoutDetail.propTypes = {
  intl: PropTypes.shape().isRequired,
  post: PropTypes.shape().isRequired,
};

export default injectIntl(PayoutDetail);
