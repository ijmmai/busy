import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage, FormattedRelative, FormattedDate, FormattedTime } from 'react-intl';
import BTooltip from '../components/BTooltip';
import Avatar from '../components/Avatar';

const PowerUpTransaction = ({ timestamp, amount, to, from, currentUsername }) => {
  let message;
  if(from === to) {
    message = (
      <span>
        <FormattedMessage id="powered_up" defaultMessage="Powered up " />
      </span>
    );
  } else if(to === currentUsername) {
    message = (
      <span>
        <FormattedMessage id="powered_up_by" defaultMessage="Powered up by " />
        <Link to={`/@${from}`}>
          <span className="username"> {from}</span>
        </Link>
      </span>
    );
  } else {
    message = (
      <span>
        <FormattedMessage id="powered_up" defaultMessage="Powered up " />
        <Link to={`/@${to}`}>
          <span className="username"> {to}</span>
        </Link>
      </span>
    );
  }

  return (
    <div className="UserWalletTransactions__transaction">
      <div className="UserWalletTransactions__avatar">
        <Avatar username={from} size={40} />
      </div>
      <div className="UserWalletTransactions__content">
        <div className="UserWalletTransactions__content-recipient">
          {message}
          <span className="UserWalletTransactions__payout">{amount}</span>
        </div>
        <span className="UserWalletTransactions__timestamp">
          <BTooltip
            title={
              <span>
                <FormattedDate value={`${timestamp}Z`} /> <FormattedTime value={`${timestamp}Z`} />
              </span>
            }
          >
            <span>
              <FormattedRelative value={`${timestamp}Z`} />
            </span>
          </BTooltip>
        </span>
      </div>
    </div>
  );
};

PowerUpTransaction.propTypes = {
  timestamp: PropTypes.string.isRequired,
  amount: PropTypes.element.isRequired,
  to: PropTypes.string.isRequired,
  from: PropTypes.string.isRequired,
  currentUsername: PropTypes.string.isRequired,
};

export default PowerUpTransaction;
