import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from "react-intl";
import Reward from 'react-rewards';
import Slider from '../Slider/Slider';
import Payout from './Payout';
import Buttons from './Buttons';
import Confirmation from './Confirmation';
import './StoryFooter.less';

@injectIntl
class StoryFooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sliderVisible: false,
      sliderValue: 0,
      memo: '',
    };
    this.handleMemoChange = this.handleMemoChange.bind(this);
  }

  static propTypes = {
    intl: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    post: PropTypes.shape().isRequired,
    postState: PropTypes.shape().isRequired,
    ownPost: PropTypes.bool,
    pendingLike: PropTypes.bool,
    pendingFollow: PropTypes.bool,
    pendingBookmark: PropTypes.bool,
    saving: PropTypes.bool,
    full: PropTypes.bool,
    onLikeClick: PropTypes.func,
    onEditClick: PropTypes.func,
    handlePostPopoverMenuClick: PropTypes.func,
    mood: PropTypes.string,
  };

  static defaultProps = {
    pendingLike: false,
    ownPost: false,
    pendingFollow: false,
    pendingBookmark: false,
    saving: false,
    full: false,
    onLikeClick: () => {},
    onEditClick: () => {},
    handlePostPopoverMenuClick: () => {},
    mood: '',
  };

  handleLikeClick = () => {
    if (!this.state.sliderVisible) {
      this.setState(prevState => ({ sliderVisible: !prevState.sliderVisible }));
    }
  };

  handleLikeConfirm = () => {
    const onSuccess = () => {
      this.reward.rewardMe();
    };

    this.setState({ sliderVisible: false }, () => {
      this.props.onLikeClick(this.props.post, this.state.sliderValue, this.state.memo, onSuccess);
    });
  };

  handleEditClick = () => this.props.onEditClick(this.props.post);

  handleSliderCancel = () => this.setState({ sliderVisible: false });

  handleSliderChange = value => {
    this.setState({ sliderValue: value });
  };

  handleMemoChange = e => {
    this.setState({memo: e.target.value});
  };

  render() {
    const {
      intl, user, post, postState, pendingLike, ownPost, pendingFollow, pendingBookmark, saving, full,
      handlePostPopoverMenuClick, mood
    } = this.props;

    const maxValue = parseFloat(_.get(user, 'reward_steem_balance', "0.000 WLS").split(' ')[0]);
    const hasTipBalance = maxValue > 0.000;

    return (
      <div className="StoryFooter">
        <div style={{pointerEvents: 'none'}}>
          <Reward
            ref={(ref) => {
              this.reward = ref
            }}
            type='memphis'
            springAnimation={false}
          >
            <div/>
          </Reward>
        </div>
        <div className="StoryFooter__actions">
          <Payout post={post} />
          {(!this.state.sliderVisible) && (
            <Buttons
              full={full}
              post={post}
              postState={postState}
              pendingLike={pendingLike}
              pendingFollow={pendingFollow}
              pendingBookmark={pendingBookmark}
              saving={saving}
              ownPost={ownPost}
              onLikeClick={this.handleLikeClick}
              onEditClick={this.handleEditClick}
              handlePostPopoverMenuClick={handlePostPopoverMenuClick}
              mood={mood}
              hasTipBalance={hasTipBalance}
            />
          )}
        </div>
        {this.state.sliderVisible && (
          <div>
            <Slider
              maxValue={maxValue}
              value={this.state.sliderValue}
              onChange={this.handleSliderChange}
              handleMemoChange={this.handleMemoChange}
            />
          </div>
        )}
        {this.state.sliderVisible && (
          <Confirmation onConfirm={this.handleLikeConfirm} onCancel={this.handleSliderCancel} />
        )}
      </div>
    );
  }
}

export default StoryFooter;
