import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import UserWalletSummary from '../wallet/UserWalletSummary';
import WalletSidebar from '../components/Sidebar/WalletSidebar';
// import UserWalletMenu from '../components/UserWalletMenu';
import { getUserDetailsKey } from '../helpers/stateHelpers';
import { simple_daily_bandwidth_limit } from '../vendor/steemitHelpers';
// import UserWalletTransactions from '../wallet/UserWalletTransactions';
// import Loading from '../components/Icon/Loading';
import {
  getUser,
  getAuthenticatedUser,
  getAuthenticatedUserName,
  getTotalVestingShares,
  getTotalVestingFundSteem,
  getUsersTransactions,
  getUsersAccountHistory,
  getUsersAccountHistoryLoading,
  getLoadingGlobalProperties,
} from '../reducers';
import { getGlobalProperties, getUserAccountHistory } from '../wallet/walletActions';
import { getAccount } from './usersActions';
import formatter from '../helpers/steemitFormatter';

@withRouter
@connect(
  (state, ownProps) => ({
    user:
      ownProps.isCurrentUser || ownProps.match.params.name === getAuthenticatedUserName(state)
        ? getAuthenticatedUser(state)
        : getUser(state, ownProps.match.params.name),
    authenticatedUserName: getAuthenticatedUserName(state),
    totalVestingShares: getTotalVestingShares(state),
    totalVestingFundSteem: getTotalVestingFundSteem(state),
    usersTransactions: getUsersTransactions(state),
    usersAccountHistory: getUsersAccountHistory(state),
    usersAccountHistoryLoading: getUsersAccountHistoryLoading(state),
    loadingGlobalProperties: getLoadingGlobalProperties(state),
  }),
  {
    getGlobalProperties,
    getUserAccountHistory,
    getAccount,
  },
)
class Wallet extends Component {
  static propTypes = {
    location: PropTypes.shape().isRequired,
    totalVestingShares: PropTypes.string.isRequired,
    totalVestingFundSteem: PropTypes.string.isRequired,
    user: PropTypes.shape().isRequired,
    getGlobalProperties: PropTypes.func.isRequired,
    getUserAccountHistory: PropTypes.func.isRequired,
    getAccount: PropTypes.func.isRequired,
    usersTransactions: PropTypes.shape().isRequired,
    usersAccountHistory: PropTypes.shape().isRequired,
    usersAccountHistoryLoading: PropTypes.bool.isRequired,
    loadingGlobalProperties: PropTypes.bool.isRequired,
    isCurrentUser: PropTypes.bool,
    authenticatedUserName: PropTypes.string,
  };

  static defaultProps = {
    isCurrentUser: false,
    authenticatedUserName: '',
  };

  state = {
    type: 'wallet',
  };

  componentDidMount() {
    const {
      totalVestingShares,
      totalVestingFundSteem,
      usersTransactions,
      user,
      isCurrentUser,
      authenticatedUserName,
    } = this.props;
    const username = isCurrentUser ? authenticatedUserName : this.props.location.pathname.match(/@(.*)(.*?)\//)[1];

    if (_.isEmpty(totalVestingFundSteem) || _.isEmpty(totalVestingShares)) {
      this.props.getGlobalProperties();
    }

    // if (_.isEmpty(usersTransactions[getUserDetailsKey(username)])) {
    //   this.props.getUserAccountHistory(username);
    // }

    if (_.isEmpty(user)) {
      this.props.getAccount(username);
    }
  }

  onMenuChange = type => {
    this.setState({ type });
  };

  render() {
    const {
      user,
      totalVestingShares,
      totalVestingFundSteem,
      loadingGlobalProperties,
      // usersTransactions,
      // usersAccountHistoryLoading,
      // usersAccountHistory,
    } = this.props;
    const { type } = this.state;
    const userKey = getUserDetailsKey(user.name);
    // const transactions = _.get(usersTransactions, userKey, []);
    // const actions = _.get(usersAccountHistory, userKey, []);

    // simple_daily_bandwidth_limit
    const vesting_steem = formatter.vestToSteem(user.vesting_shares, totalVestingShares, totalVestingFundSteem);
    const daily_bw_limit = simple_daily_bandwidth_limit(Math.floor(vesting_steem * 1000));
    const daily_bandwidth = user.daily_bandwidth;
    const last_daily_bandwidth_update = new Date(user.last_daily_bandwidth_update + 'Z').getTime();
    const elapsed_seconds = Math.floor((Date.now() - last_daily_bandwidth_update) / 1000);
    const regenerated_bw = Math.floor((daily_bw_limit * elapsed_seconds) / 86400);
    const current_daily_bw = Math.min(daily_bandwidth + regenerated_bw, daily_bw_limit);
    const current_daily_bw_percent = Math.floor((current_daily_bw * 100) / daily_bw_limit);

    return (
      <div className="UserWallet">
        <div className="WalletEssentials">
          <WalletSidebar />
          <UserWalletSummary
            user={user}
            loading={user.fetching}
            totalVestingShares={totalVestingShares}
            totalVestingFundSteem={totalVestingFundSteem}
            loadingGlobalProperties={loadingGlobalProperties}
            currentDailyBandwith={current_daily_bw}
            dailyBandwidthLimit={daily_bw_limit}
            dailyBandwidthPercent={current_daily_bw_percent}
          />
        </div>
        {/*<UserWalletMenu onChange={this.onMenuChange} defaultKey={type} />*/}
        {/*{transactions.length === 0 && usersAccountHistoryLoading ? (*/}
        {/*<Loading style={{ marginTop: '20px' }} />*/}
        {/*) : (*/}
        {/*<UserWalletTransactions*/}
        {/*transactions={transactions}*/}
        {/*actions={actions}*/}
        {/*type={type}*/}
        {/*currentUsername={user.name}*/}
        {/*totalVestingShares={totalVestingShares}*/}
        {/*totalVestingFundSteem={totalVestingFundSteem}*/}
        {/*/>*/}
        {/*)}*/}
        

        
        
      </div>
    );
  }
}

export default Wallet;
