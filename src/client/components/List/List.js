import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import Loading from '../Icon/Loading';
import ReduxInfiniteScroll from '../../vendor/ReduxInfiniteScroll';

const List = ({ columns, data, hasMore, loadingMore, loadMore, listKey='table' }) => {
  if (columns.length === 0) return null;

  return (
    <ReduxInfiniteScroll
      hasMore={hasMore}
      loadingMore={loadingMore}
      loader={<Loading />}
      loadMore={loadMore}
      items={[<Table key={listKey} rowKey="id" pagination={false} columns={columns} dataSource={data} />]}
    />
  );
};

List.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  loadMore: PropTypes.func.isRequired,
  loadingMore: PropTypes.bool.isRequired,
  hasMore: PropTypes.bool.isRequired,
  listKey: PropTypes.string,
};

export default List;
