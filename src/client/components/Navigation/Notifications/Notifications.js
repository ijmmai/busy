import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';
import NotificationItem from './NotificationItem';
import './Notifications.less';
import './Notification.less';
import Loading from '../../Icon/Loading';

const displayLimit = 6;

class Notifications extends React.Component {
  static propTypes = {
    // notifications: PropTypes.array,
    loadingNotifications: PropTypes.bool,
    lastSeenTimestamp: PropTypes.number,
    currentAuthUsername: PropTypes.string,
    onNotificationClick: PropTypes.func,
  };

  static defaultProps = {
    notifications: [],
    loadingNotifications: false,
    lastSeenTimestamp: 0,
    currentAuthUsername: '',
    onNotificationClick: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      displayedNotifications: _.slice(props.notifications, 0, displayLimit),
    };

    this.notificationsContent = null;

    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.handleNotificationsClick = this.handleNotificationsClick.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    // const { notifications, lastSeenTimestamp } = this.props;
    // const latestNotification = _.get(notifications, 0);
    // const timestamp = _.get(latestNotification, 'timestamp');
    //
    // if (timestamp > lastSeenTimestamp) {
    //   this.props.getUpdatedSCUserMetadata(); // TODO: set last_known_timestamp in local storage
    // }
  }

  componentWillReceiveProps(nextProps) {
    const differentNotifications = !_.isEqual(
      _.size(this.props.notifications),
      _.size(nextProps.notifications),
    );
    const emptyDisplayedNotifications = _.isEmpty(this.state.displayedNotifications);

    if (differentNotifications || emptyDisplayedNotifications) {
      this.setState({
        displayedNotifications: _.slice(nextProps.notifications, 0, displayLimit),
      });
    } else {
      // const latestNotification = _.get(nextProps.notifications, 0);
      // const timestamp = _.get(latestNotification, 'timestamp');
      //
      // if (timestamp > nextProps.lastSeenTimestamp) {
      //   this.props.getUpdatedSCUserMetadata(); // TODO: set last_known_timestamp in local storage
      // }
    }
  }

  onScroll() {
    const { notifications } = this.props;
    const { displayedNotifications } = this.state;
    const contentElement = this.notificationsContent;
    const topScrollPos = contentElement.scrollTop;
    const totalContainerHeight = contentElement.scrollHeight;
    const containerFixedHeight = contentElement.offsetHeight;
    const bottomScrollPos = topScrollPos + containerFixedHeight;
    const bottomPosition = totalContainerHeight - bottomScrollPos;
    const threshold = 100;
    const hasMore = displayedNotifications.length !== notifications.length;

    if (bottomPosition < threshold && hasMore) {
      this.handleLoadMore();
    }
  }

  handleLoadMore() {
    const { notifications } = this.props;
    const { displayedNotifications } = this.state;
    const moreNotificationsStartIndex = displayedNotifications.length;
    const moreNotifications = _.slice(
      notifications,
      moreNotificationsStartIndex,
      moreNotificationsStartIndex + displayLimit,
    );
    this.setState({
      displayedNotifications: displayedNotifications.concat(moreNotifications),
    });
  }

  handleNotificationsClick(e) {
    const openedInNewTab = _.get(e, 'metaKey', false) || _.get(e, 'ctrlKey', false);
    if (!openedInNewTab) {
      this.props.onNotificationClick();
    }
  }

  render() {
    const {
      notifications,
      currentAuthUsername,
      lastSeenTimestamp,
      onNotificationClick,
      loadingNotifications,
    } = this.props;
    const { displayedNotifications } = this.state;
    const displayEmptyNotifications = _.isEmpty(notifications) && !loadingNotifications;

    return (
      <div className="Notifications">
        <div
          className="Notifications__content"
          onScroll={this.onScroll}
          ref={element => {
            this.notificationsContent = element;
          }}
        >
          {loadingNotifications && <Loading style={{ padding: 20 }} />}
          {_.map(displayedNotifications, (item) => {
            const [idx, obj] = item;
            const key = `notification_${idx}`;
            // const read = lastSeenTimestamp >= notification.timestamp;
            // const [opt_type, opt_obj] = obj.op;

            return <NotificationItem key={key} idx={idx} obj={obj} currentAuthUsername={currentAuthUsername} onClick={this.handleNotificationsClick} />;
          })}
          {displayEmptyNotifications && (
            <div className="Notification Notification__empty">
              <FormattedMessage
                id="notifications_empty_message"
                defaultMessage="You currently have no notifications."
              />
            </div>
          )}
        </div>
        <div className="Notifications__footer">
          <Link to="/notifications" onClick={onNotificationClick}>
            <FormattedMessage id="see_all" defaultMessage="See All" />
          </Link>
        </div>
      </div>
    );
  }
}

export default Notifications;
