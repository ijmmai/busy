import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import PodHeader from './PodHeader';
import UserHeaderLoading from '../components/UserHeaderLoading';
import Hero from '../components/Hero';

const PodHero = ({
  authenticated,
  user,
  username,
  isSameUser,
  coverImage,
  hasCover,
  isPopoverVisible,
  onSelect,
  handleVisibleChange,
}) => (
  <div>
    <Switch>
      <Route
        path="/@:name"
        render={() => (
          <div>
            {user.fetching ? (
              <UserHeaderLoading />
            ) : (
              <PodHeader
                username={username}
                handle={user.name}
                vestingShares={parseFloat(user.vesting_shares)}
                isSameUser={isSameUser}
                coverImage={coverImage}
                hasCover={hasCover}
                isPopoverVisible={isPopoverVisible}
                onSelect={onSelect}
                handleVisibleChange={handleVisibleChange}
              />
            )}
          </div>
        )}
      />
      <Route render={() => (authenticated ? <Hero /> : <div />)} />
    </Switch>
  </div>
);

PodHero.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  user: PropTypes.shape().isRequired,
  username: PropTypes.string.isRequired,
  isSameUser: PropTypes.bool,
  coverImage: PropTypes.string,
  hasCover: PropTypes.bool,
  isPopoverVisible: PropTypes.bool,
  onSelect: PropTypes.func,
  handleVisibleChange: PropTypes.func,
};

PodHero.defaultProps = {
  isSameUser: false,
  coverImage: '',
  hasCover: false,
  isPopoverVisible: false,
  onSelect: () => {},
  handleVisibleChange: () => {},
};

export default PodHero;
