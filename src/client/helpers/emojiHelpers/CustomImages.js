const fs = require('fs');
const path = require('path');

// const IMG_PATH = `https://gitlab.com/Kennybll/wlslib/raw/master/src/emoji/images/`;
const IMG_PATH = `https://whaleshares.io/emojis/`;

const images = fs.readdirSync(path.resolve(__dirname, '../../../../assets/emojis'));

const names = images.map(name => ({
  name: `${name.split('.')[0].split('_').map(title => title.substr(0, 1).toUpperCase() + title.substr(1, title.length - 1)).join(' ')}`,
  short_names: [`wls_${name.split('.')[0]}`],
  text: '',
  emoticons: [],
  keywords: name.split('.')[0].split('_'),
  imageUrl: `${IMG_PATH}${name}`
}));

const namesObject = {};

images.forEach(name => {
  namesObject[`wls_${name.split('.')[0]}`] = {
    name: `${name.split('.')[0].split('_').map(title => title.substr(0, 1).toUpperCase() + title.substr(1, title.length - 1)).join(' ')}`,
    short_names: [`wls_${name.split('.')[0]}`],
    text: '',
    emoticons: [],
    keywords: name.split('.')[0].split('_'),
    imageUrl: `${IMG_PATH}${name}`
  };
});

fs.writeFileSync(
  path.resolve(__dirname, 'CustomImages.json'),
  JSON.stringify(names)
);

fs.writeFileSync(
  path.resolve(__dirname, 'CustomImagesObject.json'),
  JSON.stringify(namesObject)
);
