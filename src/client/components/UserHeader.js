import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// import { getProxyImageURL } from '../helpers/image';
import './UserHeader.less';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

const UserHeader = ({ coverImage, hasCover }) => {
  const style = hasCover
    ? { backgroundImage: `url("${WLS_IMG_PROXY}/1024x256/${coverImage}")` }
    : { backgroundImage: 'url("/images/user-header.jpg")' };
  return (
    <div className={classNames('UserHeader', { 'UserHeader--cover': hasCover })} style={style}>
      <div className="UserHeader__container">
        <div className="UserHeader__user">
          <div className="UserHeader__row" />
        </div>
      </div>
    </div>
  );
};

UserHeader.propTypes = {
  coverImage: PropTypes.string,
  hasCover: PropTypes.bool,
};

UserHeader.defaultProps = {
  coverImage: '',
  hasCover: false,
};

export default UserHeader;
