import { createClient } from '../common/lightrpc';
import { WLS_RPC_URL } from '../common/constants/settings';

const options = {
  timeout: 15000,
};

const client = createClient(WLS_RPC_URL, options);
client.sendAsync = (message, params) =>
  new Promise((resolve, reject) => {
    client.send(message, params, (err, result) => {
      if (err !== null) return reject(err);
      return resolve(result);
    });
  });

const chainLib = require('@whaleshares/wlsjs');

chainLib.api.setOptions({ url: WLS_RPC_URL });
chainLib.config.set('address_prefix', 'WLS');
chainLib.config.set('chain_id', 'de999ada2ff7ed3d3d580381f229b40b5a0261aec48eb830e540080817b72866');

// export default client;
export default {
  sendAsync: client.sendAsync,
  chainLib,
};
