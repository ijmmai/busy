import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage, FormattedRelative } from 'react-intl';
import _ from 'lodash';
import { Form, Input, Button } from 'antd';
import requiresLogin from '../auth/requiresLogin';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import Avatar from '../components/Avatar';
import {
  sendFriendRequest,
  approvalRequest,
  fetchPendingReceivedRequests,
  fetchPendingSentRequests,
  cancelFriendRequest
} from './actions';
import '../styles/modules/account_list.less';
import './SendFriendRequest.less';

@requiresLogin
@connect(
  state => ({
    ...state.friends,
  }),
  {
    sendFriendRequest,
    approvalRequest,
    fetchPendingSentRequests,
    fetchPendingReceivedRequests,
    cancelFriendRequest,

  },
)
class SendFriendRequest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      toUsername: '',
    };
  }

  componentDidMount() {
    window.scrollTo(0,0);
    if (this.props.match.params.toUsername) {
      this.setState({toUsername: this.props.match.params.toUsername});
    }
    if (this.props.pendingSentRequests === false) {
      this.props.fetchPendingSentRequests();
    }
  }

  handleChange = event => {
    const target = event.target;
    const name = target.name;
    let value = target.type === 'checkbox' ? target.checked : target.value;

    if (name === 'username') {
      value = value.toLowerCase();
    }

    this.setState({ [name]: value });
  };

  handleApproval = (toUsername, accepted) => {
    const onSuccess = () => {
      // this.props.fetchPendingReceivedRequests();
      this.setState({toUsername: ""})
    };

    this.props.approvalRequest(toUsername, accepted, onSuccess);
  };

  handleSend = () => {
    const onSuccess = () => {
      this.props.fetchPendingSentRequests();
    };

    let isFriend = false;
    if (this.props.pendingReceivedRequests !== false) {
      this.props.pendingReceivedRequests.forEach(request => {
       if (this.state.toUsername === request.account) isFriend = true;
      });
    }

    if (isFriend) this.handleApproval(this.state.toUsername, true);
    else this.props.sendFriendRequest(this.state.toUsername, onSuccess);
  };

  handleCancel = toUsername => {
    const onSuccess = () => {
      this.props.fetchPendingSentRequests();
    };

    this.props.cancelFriendRequest(toUsername, onSuccess);
  };

  render() {
    const {pendingSentRequests} = this.props;
    const renderPendingSentRequestsRow = !_.isEmpty(pendingSentRequests)
      ? pendingSentRequests.map(item => (
          <tr key={item.another}>
            <td className="acc-pic">
              <Avatar username={item.another} size={32} />
            </td>
            <td className="acc-name">
              <Link to={`/@${item.another}`}>@{item.another}</Link>
            </td>
            <td className="date">
              <FormattedRelative value={`${item.created}Z`} />
            </td>
            <td className="actions">
              <Button
                className="RelAction"
                onClick={() => {
                  this.handleCancel(item.another);
                }}
              >
                Cancel
              </Button>
            </td>
          </tr>
        ))
      : null;

    const renderPendingSentRequests = !_.isEmpty(this.props.pendingSentRequests) ? (
      <table className="AvatarNameDateActions">
        <thead>
          <tr>
            <th />
            <th><FormattedMessage id="name" defaultMessage="Name" /></th>
            <th><FormattedMessage id="date" defaultMessage="Date" /></th>
            <th><FormattedMessage id="actions" defaultMessage="Actions" /></th>
          </tr>
        </thead>
        <tbody>{renderPendingSentRequestsRow}</tbody>
      </table>
    ) : (
      <div className="message">
        <FormattedMessage id="no_requests_out" defaultMessage="No pending outgoing friend requests yet." />
      </div>
    );

    return (
      <div className="shifted">
        <Helmet>
          <title>SendFriendRequest</title>
        </Helmet>
        <div className="layout-twocol container">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body SendFriendRequest">
            <div className="main-title">
              <h1>
                <FormattedMessage id="send_friend_request" defaultMessage="Send Friend Request" />
              </h1>
            </div>
            <div className="main-content SendFriendRequest__content">
              <Form layout="vertical">
                <Input
                  name="toUsername"
                  type="text"
                  placeholder="Enter username you send request to"
                  addonBefore="@"
                  value={this.state.toUsername}
                  onChange={this.handleChange}
                />
              </Form>
              <Button
                className="Action Action--primary"
                onClick={() => {
                  this.handleSend();
                }}
              >
                <FormattedMessage id="send_friend_request" defaultMessage="Send Friend Request" />
              </Button>
            </div>
            <div className="AccList UseTable">
              <h3><FormattedMessage id="requests_sent" defaultMessage="Requests I Sent"/></h3>
              <div className="main-content">{renderPendingSentRequests}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SendFriendRequest;
