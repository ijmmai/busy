import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { usernameURLRegex } from '../../helpers/regexHelpers';
import formatter from '../../helpers/steemitFormatter';
import Loading from '../../components/Icon/Loading';
import busyAPI from '../../busyAPI';
import PostRecommendationLink from './PostRecommendationLink';
import './PostRecommendation.less';
import SidebarBlock from '../../app/Sidebar/SidebarBlock';
import { fetchOffChainContents } from "../../CncApi";

@withRouter
class PostRecommendation extends Component {
  static propTypes = {
    location        : PropTypes.shape().isRequired,
    match           : PropTypes.shape().isRequired,
    isAuthFetching  : PropTypes.bool.isRequired,
  };

  static defaultProps = {
    isAuthFetching  : false,
  };

  constructor(props) {
    super(props);

    this.state = {
      recommendedPosts  : [],
      loading           : false,
      currentAuthor     : '',
    };

    this.getRecommendations = this.getRecommendations.bind(this);
  }

  _isMounted = false;

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    const { location, isAuthFetching } = this.props;
    if (!isAuthFetching && location.pathname !== '/') {
      this.getRecommendations();
    }
    this._isMounted = true;
  }

  componentDidUpdate(nextProps) {
   if (this.props.isAuthFetching !== nextProps.isAuthFetching) {
      this.getRecommendations();
   }
  }

  getRecommendations() {
    const { location } = this.props;

    const author = location.pathname.match(usernameURLRegex)[1];

    this.setState({loading: true});
    this.getPostsByAuthor(author);
  }

  getPostsByAuthor = author => {
    busyAPI
      .sendAsync('database_api', 'get_blog_feed', [author, -1, 4])
      .then(result => fetchOffChainContents(result))
      .then(result => {
        const posts = result.map(item => item[1]);
        const recommendedPosts = Array.isArray(posts) ? posts : [];
        if(this._isMounted) {
          this.setState({
            currentAuthor : author,
            loading       : false,
            recommendedPosts,
          });
        }
      });
  };

  getFilteredPosts = () => {
    const { pathname } = this.props.location;

    return this.state.recommendedPosts
      .filter(post => formatter.reputation(post.author_reputation) > -1 && !pathname.includes(post.permlink))
      .slice(0, 3);
  };

  navigateToPost = author => {
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);

      if (author !== this.state.currentAuthor) {
        this.getPostsByAuthor(author);
      } 
    }
  };

  renderPosts = () => {
    const filteredRecommendedPosts = this.getFilteredPosts();

    return filteredRecommendedPosts.map(post => (
      <PostRecommendationLink
        key                     = {post.id}
        navigateToPost          = {this.navigateToPost}
        post                    = {post}
      />
    ));
  };

  render() {
    if (this.state.recommendedPosts.length === 0) return null;
    const { loading, currentAuthor }  = this.state;
    const filteredRecommendedPosts    = this.getFilteredPosts();
    const title                       = <Link role="presentation" to={`/@${currentAuthor}`}>
                                          <FormattedMessage 
                                            id              = "recommended_posts" 
                                            defaultMessage  = "More from {author}"
                                            values          = {{author:currentAuthor}} 
                                          />
                                        </Link>;

    if (loading) {
      return <Loading />;
    }

    return (
      <SidebarBlock 
        title   = {title}
        icon    = 'read'
        content = {this.renderPosts()}
      />
    );
  }
}

export default PostRecommendation;
