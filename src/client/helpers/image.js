import filesize from 'filesize';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

const IMG_PROXY = WLS_IMG_PROXY+'/0x0/';
const IMG_PROXY_PREVIEW = WLS_IMG_PROXY+'/800x600/';
const IMG_PROXY_SMALL = WLS_IMG_PROXY+'/40x40/';

// /pimgp/a/:author/p/:permlink/:widthxheight/url
const POST_IMG_PROXY = WLS_IMG_PROXY+'/pimgp/';

export const MAXIMUM_UPLOAD_SIZE = 5242880;
export const MAXIMUM_UPLOAD_SIZE_HUMAN = filesize(MAXIMUM_UPLOAD_SIZE);

export const getProxyImageURL = (is_post_proxy, author, permlink, url, type) => {
  let fullurl = url;

  /**
   * fix for protocol-relative URL, eg.,
   * //futuristictoday.com/wp-content/uploads/2017/12/Robots-to-revolutionize-farming-ease-labor-woes.jpg
   */
  try {
    if (fullurl.startsWith("//")) {
      fullurl=`https:${fullurl}`;
    }
  } catch (e) {
    // do nothing
  }


  if (is_post_proxy) {
    if (type === 'preview') {
      return `${POST_IMG_PROXY}a/${author}/p/${permlink}/800x600/${fullurl}`;
    } else if (type === 'small') {
      return `${POST_IMG_PROXY}a/${author}/p/${permlink}/40x40/${fullurl}`;
    }

    return `${POST_IMG_PROXY}a/${author}/p/${permlink}/0x0/${fullurl}`;
  } else {
    if (type === 'preview') {
      return `${IMG_PROXY_PREVIEW}${fullurl}`;
    } else if (type === 'small') {
      return `${IMG_PROXY_SMALL}${fullurl}`;
    }
    return `${IMG_PROXY}${fullurl}`;
  }
};

export const isValidImage = file => file.type.match('image/.*') && file.size <= MAXIMUM_UPLOAD_SIZE;

export default null;
