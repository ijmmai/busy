import React, { Component } from 'react';
import Topics from '../../components/Sidebar/Topics';

class NavigationRight extends Component {
  render() {
    const url = this.props.location.pathname.split("/");
    const topicSelected = url.length === 3  ? url[2] : '' ;
    return (
      <div>
        <Topics topicSelected={topicSelected} />
      </div>
    );
  }
}

export default NavigationRight;
