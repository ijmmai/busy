import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Feed from '../feed/Feed';
import { getIsAuthenticated, getAuthenticatedUser, getFeed, getUser } from '../reducers';
import { fetchFriends } from '../friends/actions';
import {
  getFeedLoadingFromState,
  getFeedFetchedFromState,
  getFeedHasMoreFromState,
  getFeedFromState,
} from '../helpers/stateHelpers';
import { getFeedContent, getMoreFeedContent } from '../feed/feedActions';
import { showPostModal, hidePostModal } from '../app/appActions';
import EmptyUserProfile from '../statics/EmptyUserProfile';
import EmptyUserOwnProfile from '../statics/EmptyUserOwnProfile';
import PostModal from '../post/PostModalContainer';

@withRouter
@connect(
  (state, ownProps) => ({
    ...state.friends,
    authenticated     : getIsAuthenticated(state),
    authenticatedUser : getAuthenticatedUser(state),
    feed              : getFeed(state),
    user              : getUser(state, ownProps.match.params.name),
  }),
  {
    showPostModal,
    hidePostModal,
    getFeedContent,
    getMoreFeedContent,
    fetchFriends,
  },
)
class UserProfile extends React.Component {
  static propTypes = {
    authenticated     : PropTypes.bool.isRequired,
    authenticatedUser : PropTypes.shape().isRequired,
    feed              : PropTypes.shape().isRequired,
    match             : PropTypes.shape().isRequired,
    showPostModal     : PropTypes.func.isRequired,
    hidePostModal     : PropTypes.func.isRequired,
  };

  static defaultProps = {
    limit     : 10,
    location  : {},
  };

  componentDidMount() {
    const { feed, match } = this.props;
    const sortBy    = this.props.user.is_pod ? 'podfeed' : 'posts';
    const username  = match.params.name;
    const content   = getFeedFromState(sortBy, username, feed);

    if (_.isEmpty(content)) this.props.getFeedContent({ sortBy, category: username });
    if( this.props.friends === false) this.props.fetchFriends();
  }

  componentWillReceiveProps(nextProps) {
    if (typeof window === 'undefined') return;
    const { feed, match } = nextProps;
    const username        = nextProps.match.params.name;
    const content         = getFeedFromState(this.sortBy, username, feed);

    const sortBy = this.props.user.is_pod ? 'podfeed' : 'posts';

    if (_.isEmpty(content) && (match.url !== this.props.match.url)) {
      if (window) window.scrollTo(0, 0);

      this.props.getFeedContent({ sortBy, category: username });
      this.props.hidePostModal();
    }
  }

  render() {
    const { authenticated, authenticatedUser, feed, match, friends } = this.props;
    const username              = match.params.name;
    const isOwnProfile          = authenticated && username === authenticatedUser.name;
    const sortBy                = this.props.user.is_pod ? 'podfeed' : 'posts';
    const content               = getFeedFromState(sortBy, username, feed);
    const isFetching            = getFeedLoadingFromState(sortBy, username, feed);
    const fetched               = getFeedFetchedFromState(sortBy, username, feed);
    const hasMore               = getFeedHasMoreFromState(sortBy, username, feed);
    const loadMoreContentAction = () => this.props.getMoreFeedContent({ sortBy, category: username });
    const podMember             = authenticatedUser
                                  && authenticatedUser.pods
                                  && authenticatedUser.pods.includes(username)
    const showFeed              = true;  //(sortBy === 'podfeed' && !podMember && this.props.user.json_metadata.private) ? false : true;
    const isFriend              = friends && friends.includes(username);
    const feedType              = (isOwnProfile || podMember ) ? 'fulllisting' : isFriend ? 'friendslisting' : 'publiclisting';

    return (
      <div>
        <div className="profile">
         {!showFeed ? (
          <div>
            <FormattedMessage id='not_member' defaultMessage='Join this pod to collaborate with its members'/><br/>
            Check the <Link to={`@${username}/news`}>News-tab</Link> for public posts by the pod owner
          </div>
        ) : (<></>)}
        {true ? (
          <>
            <Feed
              content           = {content}
              isFetching        = {isFetching}
              hasMore           = {hasMore}
              loadMoreContent   = {loadMoreContentAction}
              showPostModal     = {this.props.showPostModal}
              feedType          = {feedType}
            />
            {_.isEmpty(content) && fetched && isOwnProfile && <EmptyUserOwnProfile />}
            {_.isEmpty(content) && fetched && !isOwnProfile && <EmptyUserProfile user={username}/>}
          </>
        ) : ( <></>
        )}
        </div>
        {<PostModal />}
      </div>
    );
  }
}

export default UserProfile;
