import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Popover } from 'antd';
import { FormattedMessage } from 'react-intl';
import steemAPI from '../steemAPI';

const showMemo = async (event) => {
    let user = event.currentTarget.parentNode.dataset.user;
    let memo = event.currentTarget.parentNode.dataset.memo;
    if (!user || user == '' || !memo || memo == '') return;
    setTimeout(async function() {
      let elems = document.getElementsByClassName('ant-popover-title');
      for (let i = 0; i < elems.length; i++) {
        let elem = elems[i];
        if (elem.textContent.endsWith('Memo')) {
          elem = elem.parentElement.children[1];
          if (memo.startsWith('#') && elem.textContent == '') {
            let key = localStorage.getItem('auth') != null ? JSON.parse(localStorage.getItem('auth')).keys.postingKey : '';
            if (key != '' && !key.endsWith(':')) {
              try { 
                memo = steemAPI.chainLib.memo.decode(key, memo).substring(1); 
              } catch (e) {
                memo = '['+e.message+']';
              }
            } else
            if (window.whalevault) {
              let response = await window.whalevault.promiseRequestDecryptMemo('wls_busy', 'wls:'+user, memo, 'posting', 'tip_memo');
              if (response.success) memo = response.result; else memo = '['+response.message+']';
            } else
            if (!/[^#a-z1-9]/i.test(memo)/*validate format for safe href insertion!*/) elem.innerHTML = '<a target="_blank" href="memo:'+memo+'">[memo requires WhaleVault to decrypt]</a>';
          }
          if (elem.textContent == '') elem.textContent = memo;
        }
      }
    }, 100);
}

const ShowMemo = (props) => {
  if (!props.memo) return null;
  let is_enc = props.memo.startsWith('#');
  let display_text = is_enc ? '[encrypted]' : props.memo;
  let display_title = is_enc ? 'Encrypted Memo' : 'Memo';

  let innerText = props.children;
  if (innerText == null) {
    if (props.icon) innerText = ( <Icon type="file-text" /> ); else
      {
        if (is_enc) innerText = ( <span style={{color: 'red'}}><b>[ENCRYPTED MESSAGE]</b></span> ); else
          innerText = (props.memo.length > 140) ? props.memo.substring(0,135)+' ...' : props.memo;
        innerText = ( <div className="Notification__text__memo">
                        <Icon type="file-text" />&nbsp;
                        <FormattedMessage
                         id="notification_memo_field"
                         defaultMessage="Memo: {text}"
                         values={{text: innerText}}
                        />
                      </div> );
      }
  }

  if (!props.icon && !is_enc && (props.memo.length < 140)) {
    return ( <span title={display_text}>{innerText}</span> );
  }

  return ( <span title={display_text} data-user={props.user} data-memo={props.memo}><Popover content="" title={display_title} onClick={showMemo} trigger="click">{innerText}</Popover></span> );
}

const propTypes = {
  user: PropTypes.string,
  memo: PropTypes.string,
  icon: PropTypes.bool,
};

const defaultProps = {
  user: '',
  memo: '',
  icon: true,
};

export default ShowMemo;
