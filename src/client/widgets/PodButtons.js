import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import { Button, Popover } from "antd";
import Slider from '../components/Slider/Slider';
import Confirmation from '../components/StoryFooter/Confirmation';
import { getAuthenticatedUser, getMutedList, getPendingMutes } from "../reducers";
import { createPodJoinLink } from '../utils/wlsuri';
import { WALLET_URL } from '../../common/constants/settings';
import { muteUser, tipUser } from "../user/userActions";
import withAuthAction from "../auth/withAuthActions";
import './PodButtons.less';

const ButtonGroup = Button.Group;

class PodButtons extends React.Component {
  static propTypes = {
    pod               : PropTypes.string,
    user              : PropTypes.shape().isRequired,
    intl              : PropTypes.shape().isRequired,
    onActionInitiated : PropTypes.func.isRequired,
    tipUser           : PropTypes.func,
    muteUser          : PropTypes.func,
  };

  static defaultProps = {
    authenticatedUserName: undefined,
    pod: undefined,
    tipUser: () => {},
    muteUser: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      sliderVisible: false,
      sliderValue: 0,
      memo: '',
    };

    this.handleMuteClick = this.handleMuteClick.bind(this);
    this.handleMemoChange = this.handleMemoChange.bind(this);    
  }

  handleTipConfirm = () => {
    const {pod} = this.props;
    this.setState({sliderVisible: false});
    this.props.onActionInitiated(() => {
      this.props.tipUser(pod, this.state.sliderValue, this.state.memo);
    });
  };

  handleSliderCancel = () => {
    this.setState({sliderVisible: false});
  };

  handleVisibleChange = visible => {
    this.setState({sliderVisible: visible});
  };

  handleSliderChange = value => {
    this.setState({sliderValue: value});
  };

  handleMemoChange = e => {
    this.setState({memo: e.target.value});
  };

  createMuteText(isFollowed, pending) {
    let followingText = this.props.intl.formatMessage({ id: 'mute', defaultMessage: 'Mute' });
    if (isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'muted', defaultMessage: 'Muted' });
    } else if (isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'unmute', defaultMessage: 'Unmute' });
    } else if (isFollowed && pending) {
      followingText = this.props.intl.formatMessage({ id: 'unmuting', defaultMessage: 'Unmuting' });
    } else if (!isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'mute', defaultMessage: 'Mute' });
    } else if (!isFollowed && pending) {
      followingText = this.props.intl.formatMessage({ id: 'muting', defaultMessage: 'Muting' });
    }
    return followingText;
  }

  handleMuteClick(e) {
    e.preventDefault();
    const { pod } = this.props;
    const isMuted = this.props.muted ? this.props.muted.includes(pod) : false;
    this.props.onActionInitiated(() => {
      if (isMuted) {
        this.props.muteUser(pod, 'null');
      } else {
        this.props.muteUser(pod);
      }
    });
  }

  render() {
    const {intl, user, pod, pods, pendingMutes, muted} = this.props;
    if ((typeof pod === `undefined`) || (typeof user === `undefined`) || (user.name === pod)) return null;

    const pendingMute = pendingMutes.includes(pod);
    const mute        = muted ? muted.includes(pod) : false;

    let editButton = null;
    if (user.name === pod) {
      editButton = (
        <a target="_blank" rel="noopener noreferrer" href={`https://wallet.whaleshares.io/pod_update`}>
          <Button type="dashed" size="small">
            <FormattedMessage id="edit_pod" defaultMessage="Edit Pod"/>
          </Button>
        </a>
      );
    }

    let muteButton = null;
    if (user.name !== pod) {
      muteButton = (
         <Button type="dashed" size="small" onClick={this.handleMuteClick}>
            {this.createMuteText(mute, pendingMute)}
         </Button>
      );
    }

    let joinButton = null;
    {
      const isMember = pods && pods.includes(pod);
      if (isMember) joinButton = null;
      else joinButton = (
        <a className="RelAction" href={`${WALLET_URL}${createPodJoinLink(pod)}`} target="_blank"
           rel="noopener noreferrer">
          <Button type="dashed" size="small">
            <FormattedMessage id="join" defaultMessage="Join"/>
          </Button>
        </a>
      );
    }

    const maxValue = parseFloat(_.get(user, 'reward_steem_balance', "0.000 WLS").split(' ')[0]);

    return (
      <ButtonGroup className="PodButtons">
        {editButton}
        {joinButton}
        <Popover
          title="Tip Pod"
          trigger="click"
          visible={this.state.sliderVisible}
          onVisibleChange={this.handleVisibleChange}
          content={
            <>
              <Slider
                maxValue={maxValue}
                value={this.state.sliderValue}
                onChange={this.handleSliderChange}
                handleMemoChange={this.handleMemoChange}
              />
              <Confirmation onConfirm={this.handleTipConfirm} onCancel={this.handleSliderCancel}/>
            </>
          }
        >
          <Button type="dashed" size="small">Tip</Button>
        </Popover>

       {muteButton} 

      </ButtonGroup>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    user: getAuthenticatedUser(state),
    pendingMutes: getPendingMutes(state),
    muted: getMutedList(state),
    pods: _.get(state, 'auth.user.pods', [])
  };
};
const mapDispatchToProps = {
  tipUser, muteUser
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  withAuthAction,
  withConnect,
  injectIntl,
)(PodButtons);

