import _ from "lodash";
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from "react-intl";
import Reward from 'react-rewards';
import Slider from '../Slider/Slider';
import Buttons from './Buttons';
import Confirmation from './Confirmation';
import './CommentFooter.less';

@injectIntl
class CommentFooter extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      memo              : '',
      replyFormVisible  : false,
      sliderValue       : 0,
      sliderVisible     : false,
    };

    this.handleMemoChange = this.handleMemoChange.bind(this);
  }
  static propTypes = {
    comment       : PropTypes.shape().isRequired,
    editable      : PropTypes.bool,
    editing       : PropTypes.bool,
    intl          : PropTypes.shape().isRequired,
    onEditClick   : PropTypes.func,
    onLikeClick   : PropTypes.func,
    onReplyClick  : PropTypes.func,
    pendingVotes  : PropTypes.arrayOf(
                    PropTypes.shape({
                      id      : PropTypes.number,
                      percent : PropTypes.number,
                    }),
                  ),    
    replying      : PropTypes.bool,
    user          : PropTypes.shape().isRequired,
  };

  static defaultProps = {
    editable      : false,
    editing       : false,
    onEditClick   : () => {},
    onLikeClick   : () => {},
    onReplyClick  : () => {},
    ownPost       : false,
    pendingLike   : false,
    pendingVotes  : [],
    replying      : false,
  };

  handleLikeClick = () => {
    if (!this.state.sliderVisible) {
      this.setState(prevState => ({ sliderVisible: !prevState.sliderVisible }));
    }
  };

  handleLikeConfirm = () => {
    const onSuccess = () => {
      this.reward.rewardMe();
    };

    this.setState({ sliderVisible: false }, () => {
      this.props.onLikeClick(this.props.comment.id, this.state.sliderValue, this.state.memo, onSuccess);
    });
  };

  handleSliderCancel = () => this.setState({ sliderVisible: false });

  handleSliderChange = value => {
    this.setState({ sliderValue: value });
  };

  handleMemoChange = e => {
    this.setState({memo: e.target.value});
  };

  render() {
    const {intl, user, comment, editable, editing, replying, pendingVotes, isRestricted, maxTip} = this.props;
    const {sliderVisible} = this.state;
    const maxValue        = parseFloat(_.get(user, 'reward_steem_balance', "0.000 WLS"));
    const ownPost         = user.name === comment.author;

    return (
      <div className="CommentFooter">
        <div style={{pointerEvents: 'none'}}>
          <Reward
            ref={(ref) => {
              this.reward = ref
            }}
            type='memphis'
            springAnimation={false}
          >
            <div/>
          </Reward>
        </div>
        {sliderVisible && (
          <div>
            <Slider
              handleMemoChange  = {this.handleMemoChange}
              maxValue          = {maxValue}
              onChange          = {this.handleSliderChange}
              value             = {this.state.sliderValue}
            />
            <Confirmation onConfirm={this.handleLikeConfirm} onCancel={this.handleSliderCancel} />
          </div>
        )}
        {(!this.state.sliderVisible) && (
          <Buttons
            comment       = {comment}
            editable      = {editable}
            editing       = {editing}
            isRestricted  = {isRestricted}
            onEditClick   = {this.props.onEditClick}
            onLikeClick   = {this.handleLikeClick}
            onReplyClick  = {this.props.onReplyClick}
            ownPost       = {ownPost}
            pendingVotes  = {pendingVotes}
            replying      = {replying}
            user          = {user}
          />
        )}
      </div>
    );
  }
}

export default CommentFooter;
