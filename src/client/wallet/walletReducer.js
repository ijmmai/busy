import _ from 'lodash';
import * as walletActions from './walletActions';
import { actionsFilter, ACTIONS_DISPLAY_LIMIT } from '../helpers/accountHistoryHelper';
import { getUserDetailsKey } from '../helpers/stateHelpers';

const initialState = {
  transferVisible: false,
  transferTo: '',
  totalVestingShares: '',
  totalVestingFundSteem: '',
  usersTransactions: {},
  usersAccountHistory: {},
  usersAccountHistoryLoading: true,
  loadingGlobalProperties: true,
  accountHistoryFilter: [],
  currentDisplayedActions: [],
  currentFilteredActions: [],
  whaletokensVisible: false,
  scatter: null,
  globalProperties: false,
  claimVestingRewardLoading: false,
};

export default function walletReducer(state = initialState, action) {
  switch (action.type) {
    case walletActions.OPEN_TRANSFER:
      return {
        ...state,
        transferVisible: true,
        transferTo: action.payload,
      };
    case walletActions.CLOSE_TRANSFER:
      return {
        ...state,
        transferVisible: false,
      };
    case walletActions.OPEN_WHALETOKEN:
      return {
        ...state,
        whaletokensVisible: true,
      };
    case walletActions.CLOSE_WHALETOKEN:
      return {
        ...state,
        whaletokensVisible: false,
      };
    case walletActions.SET_SCATTER.SUCCESS:
      return {
        ...state,
        scatter: action.payload,
      };
    case walletActions.GET_GLOBAL_PROPERTIES.START:
      return {
        ...state,
        loadingGlobalProperties: true,
      };
    case walletActions.GET_GLOBAL_PROPERTIES.SUCCESS: {
      return {
        ...state,
        totalVestingFundSteem: action.payload.total_vesting_fund_steem,
        totalVestingShares: action.payload.total_vesting_shares,
        globalProperties: action.payload,
        loadingGlobalProperties: false,
      };
    }
    case walletActions.GET_GLOBAL_PROPERTIES.ERROR: {
      return {
        ...state,
        loadingGlobalProperties: false,
      };
    }
    case walletActions.GET_USER_ACCOUNT_HISTORY.START:
      return {
        ...state,
        usersAccountHistoryLoading: true,
      };
    case walletActions.GET_USER_ACCOUNT_HISTORY.SUCCESS:
      return {
        ...state,
        usersTransactions: {
          ...state.usersTransactions,
          [getUserDetailsKey(action.payload.username)]: action.payload.userWalletTransactions,
        },
        usersAccountHistory: {
          ...state.usersAccountHistory,
          [getUserDetailsKey(action.payload.username)]: action.payload.userAccountHistory,
        },
        usersAccountHistoryLoading: false,
      };
    case walletActions.GET_USER_ACCOUNT_HISTORY.ERROR:
      return {
        ...state,
        usersAccountHistoryLoading: false,
      };
    case walletActions.UPDATE_ACCOUNT_HISTORY_FILTER: {
      const usernameKey = getUserDetailsKey(action.payload.username);
      const currentUserActions = state.usersAccountHistory[usernameKey];
      const initialActions = _.slice(currentUserActions, 0, ACTIONS_DISPLAY_LIMIT);
      const initialFilteredActions = _.filter(initialActions, userAction =>
        actionsFilter(userAction, action.payload.accountHistoryFilter, action.payload.username),
      );

      return {
        ...state,
        accountHistoryFilter: action.payload.accountHistoryFilter,
        currentDisplayedActions: initialActions,
        currentFilteredActions: initialFilteredActions,
        usersAccountHistory: {
          ...state.usersAccountHistory,
          [getUserDetailsKey(action.payload.username)]: [],
        },
        usersAccountHistoryLoading: false,
      };
    }
    case walletActions.SET_INITIAL_CURRENT_DISPLAYED_ACTIONS: {
      const currentUserActions = state.usersAccountHistory[getUserDetailsKey(action.payload)];

      return {
        ...state,
        currentDisplayedActions: _.slice(currentUserActions, 0, ACTIONS_DISPLAY_LIMIT),
      };
    }
    case walletActions.ADD_MORE_ACTIONS_TO_CURRENT_DISPLAYED_ACTIONS:
      return {
        ...state,
        currentDisplayedActions: _.concat(state.currentDisplayedActions, action.payload.moreActions),
        currentFilteredActions: _.concat(state.currentFilteredActions, action.payload.filteredMoreActions),
        loadingMoreUsersAccountHistory: false,
      };
    case walletActions.UPDATE_FILTERED_ACTIONS:
      return {
        ...state,
        currentFilteredActions: action.payload,
      };
    case walletActions.CLAIM_DAILY_REWARD.START:
      return {
        ...state,
        claimVestingRewardLoading: true,
      };
    case walletActions.CLAIM_DAILY_REWARD.SUCCESS: {
      return {
        ...state,
        claimVestingRewardLoading: false,
      };
    }
    case walletActions.CLAIM_DAILY_REWARD.ERROR: {
      return {
        ...state,
        claimVestingRewardLoading: false,
      };
    }
    default:
      return state;
  }
}

export const getIsTransferVisible = state => state.transferVisible;
export const getTransferTo = state => state.transferTo;
export const getTotalVestingShares = state => state.totalVestingShares;
export const getTotalVestingFundSteem = state => state.totalVestingFundSteem;
export const getUsersTransactions = state => state.usersTransactions;
export const getUsersAccountHistoryLoading = state => state.usersAccountHistoryLoading;
export const getLoadingGlobalProperties = state => state.loadingGlobalProperties;
export const getUsersAccountHistory = state => state.usersAccountHistory;
export const getAccountHistoryFilter = state => state.accountHistoryFilter;
export const getCurrentDisplayedActions = state => state.currentDisplayedActions;
export const getCurrentFilteredActions = state => state.currentFilteredActions;
export const getIsWhaletokensVisible = state => state.whaletokensVisible;
export const getScatter = state => state.scatter;
