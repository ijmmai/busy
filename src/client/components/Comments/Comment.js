import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { injectIntl, FormattedRelative, FormattedDate, FormattedTime, FormattedMessage } from 'react-intl';
import { message } from 'antd';
import BTooltip from '../../components/BTooltip';
import formatter from '../../helpers/steemitFormatter';
import { MAXIMUM_UPLOAD_SIZE_HUMAN } from '../../helpers/image';
import { sortComments } from '../../helpers/sortHelpers';
import CommentForm from './CommentForm';
import EmbeddedCommentForm from './EmbeddedCommentForm';
import Avatar from '../Avatar';
import Body from '../Story/Body';
import CommentFooter from '../CommentFooter/CommentFooter';
import HiddenCommentMessage from './HiddenCommentMessage';
import { jsonParse } from '../../helpers/formatter';
import './Comment.less';

const ConditionalWrapper = ({condition, wrapper, children}) => 
  condition ? wrapper(children) : children;

@injectIntl
class Comment extends React.Component {
  static propTypes = {
    comment           : PropTypes.shape().isRequired,
    commentsChildren  : PropTypes.shape(),
    depth             : PropTypes.number,
    intl              : PropTypes.shape().isRequired,
    notify            : PropTypes.func,
    onLikeClick       : PropTypes.func,
    onSendComment     : PropTypes.func,
    parent            : PropTypes.shape().isRequired,
    pendingVotes      : PropTypes.arrayOf(
                          PropTypes.shape({
                            id: PropTypes.number,
                            percent: PropTypes.number,
                          }),
                        ),   
    rewriteLinks      : PropTypes.bool,
    rootPostAuthor    : PropTypes.string,
    sort              : PropTypes.oneOf(['BEST', 'NEWEST', 'OLDEST']),
    user              : PropTypes.shape().isRequired,
  };

  static defaultProps = {
    commentsChildren  : undefined,
    depth             : 0,
    notify            : () => {},
    onLikeClick       : () => {},
    onSendComment     : () => {},
    pendingVotes      : [],
    rewriteLinks      : false,
    rootPostAuthor    : undefined,
    sort              : 'BEST',
  };

  constructor(props) {
    super(props);
    this.state = {
      collapsed               : false,
      commentFormText         : '',
      editOpen                : false,
      replyOpen               : false,
      showCommentFormLoading  : false,
      showHiddenComment       : false,
    };

    this.handleSubmitComment = this.handleSubmitComment.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      const { comment } = this.props;
      const { hash }    = window.location;

      const anchorLink = `#@${comment.author}/${comment.permlink}`;
      if (hash.indexOf(anchorLink) === 0 || comment.focus) {
        if (hash.endsWith('-edit')) {
          this.handleEditClick();
        }
        this.focus();
      }
    }
  }

  setSelf = c => {
    this.self = c;
  };

  focus = () => {
    if (this.self && window) {
      this.self.scrollIntoView(true);
      document.body.scrollTop -= 54 * 2; // twice the height of Topnav
      this.self.classList.add('Comment--focus');
    }
  };

  handleAnchorClick = () => this.focus();

  handleReplyClick = () =>
    this.setState({
      editOpen  : !this.state.replyOpen ? false : this.state.editOpen,
      replyOpen : !this.state.replyOpen,
    });

  handleEditClick = () =>
    this.setState({
      editOpen  : !this.state.editOpen,
      replyOpen : !this.state.editOpen ? false : this.state.replyOpen,
    });

  handleCollapseClick = () =>
    this.setState({
      collapsed: !this.state.collapsed,
    });

  handleImageInvalid = () => {
    const { formatMessage } = this.props.intl;
    this.props.notify(
      formatMessage(
        {
          id: 'notify_uploading_image_invalid',
          defaultMessage: 'This file is invalid. Only image files with maximum size of {size} are supported',
        },
        { size: MAXIMUM_UPLOAD_SIZE_HUMAN },
      ),
      'error',
    );
  };

  handleSubmitComment(parentPost, commentValue, storage, isUpdating, originalComment) {
    const { intl } = this.props;
    this.setState({ showCommentFormLoading: true });

    return this.props
      .onSendComment(parentPost, commentValue, storage, isUpdating, originalComment)
      .then(() => {
        if (isUpdating) {
          message.success(
            intl.formatMessage({
              id              : 'notify_comment_updated',
              defaultMessage  : 'Comment updated',
            }),
          );
        } else {
          message.success(
            intl.formatMessage({
              id              : 'notify_comment_sent',
              defaultMessage  : 'Comment submitted',
            }),
          );
        }

        this.setState({
          commentFormText         : '',
          editOpen                : false,
          replyOpen               : false,
          showCommentFormLoading  : false,
        });
      })
      .catch(() => {
        this.setState({
          commentFormText         : commentValue,
          editOpen                : false,
          replyOpen               : true,
          showCommentFormLoading  : false,
        });
        return {
          error: true,
        };
      });
  }

  handleEditComment = (parentPost, commentValue, storage) => {
    this.handleSubmitComment(parentPost, commentValue, storage, true, this.props.comment);
  };

  handleShowHiddenComment = () => {
    this.setState({
      showHiddenComment: true,
    });
  };

  render() {
    const {
      comment,
      commentsChildren,
      depth,
      intl,
      isRestricted,
      isUpdating,
      parent,
      pendingVotes,
      rewriteLinks,
      rootPostAuthor,
      sort,
      user,
    } = this.props;
    const { showHiddenComment } = this.state;

    const anchorId                = `@${comment.author}/${comment.permlink}`;
    const commentAuthorReputation = formatter.reputation(comment.author_reputation);
    const editable                = comment.author === user.name && comment.cashout_time !== '1969-12-31T23:59:59';
    const showCommentContent      = commentAuthorReputation >= 0 || showHiddenComment;

    let content = null;

    // filter large header markdown/html tags from comments
    let commentBody = comment.body
                        .replace(/^[#]{1,5} /gm,'')
                        .replace(/<\/?h[0-5]>/gm,'')    
                        .replace(/^[-]{3}?/gm,'<hr>')       
                        .replace(/(^[-]{1,}?)/gm,'<br>$1');       

    const avatarSize = comment.depth === 1 ? 40 : 32;

    if (this.state.editOpen) {
      const metadata = jsonParse(comment.json_metadata);
      const storage = _.get(metadata, 'storage', '');
      content = (
        <EmbeddedCommentForm
          inputValue  = {comment.body}
          isLoading   = {this.state.showCommentFormLoading}
          onClose     = {this.handleEditClick}
          onSubmit    = {this.handleEditComment}
          parentPost  = {parent}
          storage     = {storage}
        />
      );
    } else {
      content = this.state.collapsed ? (
        <div className="Comment__content__collapsed">
          <FormattedMessage id="comment_collapsed" defaultMessage="Comment collapsed" />
        </div>
      ) : (
        <Body is_post_proxy author={comment.author} permlink={comment.permlink}
              rewriteLinks={rewriteLinks} body={commentBody} />
      );
    }

    return (
      <div ref={this.setSelf} className="Comment" id={anchorId}>
        <span role="presentation" className="Comment__visibility" onClick={this.handleCollapseClick}>
          {this.state.collapsed ? <i className="iconfont icon-addition" /> : <i className="iconfont icon-offline" />}
        </span>
        <Link to={`/@${comment.author}`} style={{ height: avatarSize }}>
          <Avatar username={comment.author} size={avatarSize} />
        </Link>
        <div className="Comment__text">
          <ConditionalWrapper
            condition = {rootPostAuthor==comment.author}
            wrapper   = {children => <div className="Comment__op">{children}</div>}
          >
            <Link to={`/@${comment.author}`}>
              <span className="username">{comment.author}</span>
            </Link>
            <span className="Comment__date">
              <BTooltip
                title={
                  <span>
                    <FormattedDate value={`${comment.created}Z`} /> <FormattedTime value={`${comment.created}Z`} />
                  </span>
                }
              >
                <span className="Comment__anchor">
                  <FormattedRelative value={`${comment.created}Z`} />
                </span>
              </BTooltip>
            </span>
            <div className="Comment__content">
              {showCommentContent ? content : <HiddenCommentMessage onClick={this.handleShowHiddenComment} />}
            </div>
            <CommentFooter
              comment       = {comment}
              editable      = {editable}
              editing       = {this.state.editOpen}
              isRestricted  = {isRestricted}
              isUpdating    = {isUpdating}
              onEditClick   = {this.handleEditClick}
              onLikeClick   = {this.props.onLikeClick}
              onReplyClick  = {this.handleReplyClick}
              pendingVotes  = {pendingVotes}
              replying      = {this.state.replyOpen}
              user          = {user}
            />
          </ConditionalWrapper>

          {this.state.replyOpen &&
            user.name && (
              <CommentForm
                inputValue  = {this.state.commentFormText}
                isLoading   = {this.state.showCommentFormLoading}
                isSmall     = {comment.depth !== 1}
                onSubmit    = {this.handleSubmitComment}
                parentPost  = {comment}
                username    = {user.name}
              />
            )}
          <div
            className={classNames('Comment__replies', {
              'Comment__replies--no-indent': depth >= 1,
              'Comment__replies--never-indent': depth >= 5,
            })}
          >
            {!this.state.collapsed &&
              commentsChildren &&
              commentsChildren[comment.id] &&
              sortComments(commentsChildren[comment.id], sort).map(child => (
                <Comment
                  comment           = {child}
                  commentsChildren  = {commentsChildren}
                  depth             = {depth + 1}
                  intl              = {this.props.intl}
                  isRestricted      = {isRestricted}
                  key               = {child.id}
                  notify            = {this.props.notify}
                  onLikeClick       = {this.props.onLikeClick}
                  onSendComment     = {this.props.onSendComment}
                  parent            = {comment}
                  pendingVotes      = {pendingVotes}
                  rewriteLinks      = {rewriteLinks}
                  rootPostAuthor    = {rootPostAuthor}
                  sort              = {sort}
                  user              = {user}
                />
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Comment;
