import PropTypes from "prop-types";
import React from "react";
import { Card, Rate, Icon, Divider } from 'antd';
import './PodBadge.less';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

const PodBadge = ({type, name, level, badgeImage, count}) => {
  return (
    <Card className="PodBadge" bordered={false} size='small'>
      <Card.Meta
        avatar={
          <div className="badge-image"
               style={{backgroundImage: `url(${WLS_IMG_PROXY}/32x32/${badgeImage})`}}/>
        }
        description={
          <>
            <Rate className="rate" disabled defaultValue={level + 1}
                  character={<Icon type="star" theme="filled" style={{fontSize: 12}}/>}/>
            <br />
            <span>{name} <Divider type='vertical' /> {count} {type}(s)</span>
          </>
        }
      />
    </Card>
  );
};

PodBadge.propTypes = {
  type: PropTypes.string,       /* rank type: post or comment */
  name: PropTypes.string,       /* rank title eg., beginner, advanced... */
  level: PropTypes.number,      /* 0 to 5 */
  badgeImage: PropTypes.string, /* url to the badge image */
  count: PropTypes.number,      /* total number of comments or posts */
};

PodBadge.defaultProps = {
  type: '',
  name: '',
  level: 0,
  badgeImage: 'https://whaleshares.io/images/icons/icon-128x128.png',
  count: 0,
};

export default PodBadge;
