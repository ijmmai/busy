import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedDate } from 'react-intl';
import List from '../components/List/List';
import Avatar from '../components/Avatar';
import busyAPI from '../busyAPI';

export default class PodMembers extends React.Component {
  static propTypes = {
    match: PropTypes.shape().isRequired,
  };

  static limit = 50;

  state = {
    members     : [],
    hasMore     : false,
    loadingMore : false,
  }


  componentDidMount() {
    this.fetch();
  }

  fetch = () => {
    const { match }   = this.props;
    const { members } = this.state;  
    this.setState({ loadingMore: true });
 
    busyAPI
      .sendAsync('database_api', 'lookup_pod_members', [
        match.params.name,
        members.length === 0 ? '' : members[members.length - 1],
        PodMembers.limit,
      ])
      .then(m => this.setState({ 
                  members     : [...members, ...m],
                  hasMore     : m >= PodMembers.limit,
                  loadingMore : false 
                 })
      );
  }

  render() {
    const columns = [
      {
        title     : '',
        dataIndex : 'avatar',
        key       : 'avatar',
        width     : 1,
        render    : (text, record) => <Avatar size={32} username={record.account} />
      },
      {
        title     : 'Name',
        dataIndex : 'account',
        key       : 'account',
        className : 'account',
        render    : (text, record) => <Link to={'/@' + record.account}>{text}</Link>,
      },
      {
        title     : 'Joined',
        dataIndex : 'joined',
        key       : 'joined',
        className : 'joined',
        render    : (text, record) => 
                      <FormattedDate
                        value = {new Date(record.joined)}
                        year  = 'numeric'
                        month = 'short'
                        day   = '2-digit'
                      />,
      },
      {
        title     : 'Posts',
        dataIndex : 'total_posts',
        key       : 'total_posts',
        className : 'total_posts',
      },
      {
        title     : 'Comments',
        dataIndex : 'total_comments',
        key       : 'total_comments',
        className : 'total_comments',
      },
    ];

    return (
      <div className="main-body">
        <div className="main-content AccList UseList Pods">
          <List
            listKey     = 'memberlist'
            data        = {this.state.members}
            columns     = {columns}
            loadMore    = {this.fetch}
            hasMore     = {this.state.hasMore}
            loadingMore = {this.state.loadingMore}
          />
        </div>
      </div>
    );
  }
}
