import url from 'url';
import busyAPI from '../../client/busyAPI';
import renderAmpPage from '../renderers/ampRenderer';
import { fetchOffChainContent } from "../../client/CncApi";

const debug = require('debug')('busy:server');

export default function createAmpHandler(template) {
  return async function ampResponse(req, res) {
    try {
      let result = await busyAPI.sendAsync('database_api', 'get_content',
        [req.params.author, req.params.permlink]);
      result = await fetchOffChainContent(result);
      if (result.author === '') return res.sendStatus(404);
      const appUrl = (process.env.NODE_ENV !== 'production') ? url.format({
        protocol: req.protocol,
        host: req.get('host'),
      }) : 'https://whaleshares.io';

      const page = renderAmpPage(result, appUrl, template);
      return res.send(page);
    } catch (error) {
      debug('Error while parsing AMP response', error);
      return res.status(500).send('500 Internal Server Error');
    }
  };
}
