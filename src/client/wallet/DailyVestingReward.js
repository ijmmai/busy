/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from "react-redux";
import { compose } from 'redux';
import { Button, Card, Collapse, Empty, Form, Icon, Input, InputNumber, Spin, Statistic, Tooltip } from 'antd';
import Reward from 'react-rewards';
import { claimDailyReward, claimDailyRewardToTipBalance } from './walletActions';
import { reload } from '../auth/authActions';
import { notify } from '../app/Notification/notificationActions';
import { truncateDecimal } from '../helpers/formatter';
import './DailyVestingReward.less';

const { Countdown } = Statistic;
const { Panel } = Collapse;

class DailyVestingReward extends React.Component {
  handleSubmit = (event) => {
    event.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { amount, to, memo } = values;
        this.props.claimDailyReward(amount, to, memo)
          .then(() => {
            this.props.reload();
            this.props.notify('Claimed', 'success');
            this.props.form.resetFields(); // reset form
            this.reward.rewardMe();
          })
          .catch(() => {
            this.props.notify('Oops! Could not claim, try again later', 'error');
          });
      }
    });
  };

  constructor(props) {
    super(props);
    this.state = {
      dailyRewardRecipient: "Claim to My Whalestake",
    };
  }

  handleClaimToTipBalance = (event) => {
    event.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { amount } = values;
        this.props.claimDailyRewardToTipBalance(amount)
          .then(() => {
            this.props.reload();
            this.props.notify('Claimed', 'success');
            this.props.form.resetFields(); // reset form
            this.reward.rewardMe();
          })
          .catch(() => {
            this.props.notify('Oops! Could not claim, try again later', 'error');
          });
      }
    });
  };

  handleRecipientChange = (event) => {
    if (event.target.value.length > 0) {
      this.setState({'dailyRewardRecipient': 'Power Up My Friend'});
    } else {
      this.setState({'dailyRewardRecipient': 'Claim to My Whalestake'});
    }
  }

  timerEnded = () => {
    //TODO refresh DailyVestingReward component instead of page
    window.location.reload(false);
  }

  render() {
    const { form, globalProperties, user, remain, claimedAmount } = this.props;
    const { dailyRewardRecipient } = this.state;
    const { getFieldDecorator } = form;
    const cycle_window = 28800; // 24 hours - not including missing blocks
    const block_remains = cycle_window - (globalProperties.head_block_number % cycle_window);
    const deadline = (new Date(`${globalProperties.time}Z`).getTime()) + (block_remains*3*1000);

    let renderContent = null;
    if ((user.vr_claimed_cycle >= globalProperties.vr_cycle) && (remain.toFixed(3) === "0.000")) {
      renderContent = <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Waiting for next cycle..." />;
    } else {
      renderContent = (
        <Form onSubmit={this.handleSubmit}>
          <Form.Item
            label={
              <span>
                Amount to claim
              </span>
            }
            extra={
              <small>
                max:&nbsp;
                <Button type="link" onClick={() => {
                  form.setFieldsValue({ [ `amount` ]: remain });
                }}>{remain.toFixed(3)}</Button>
                &nbsp;(claimed: {claimedAmount.toFixed(3)})
              </small>
            }
          >
            {getFieldDecorator('amount', {
              rules: [ { required: true, message: 'Please enter the amount!' } ],
            })(
              <InputNumber
                style={{ width: '100%' }}
                size="large"
                // disabled={submitting}
                placeholder="0.000"
                precision={3}
                min={0.001}
                max={remain}
              />,
            )}
          </Form.Item>
          <Collapse
            bordered={false}
            defaultActiveKey={['1']}
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
          >
            <Panel header="Power Up a Friend">
              <Form.Item
                label={
                  <span>
                    Power Up&nbsp;
                    <Tooltip title="Any Whaleshares account to receive the reward. This Power Up will instantly increase their DaBa. Leave empty to claim to your own account">
                      <Icon type="question-circle-o"/>
                    </Tooltip>
                  </span>
                }
              >
                {getFieldDecorator('to', {
                  rules: [ { required: false, message: 'Please input recipient!' } ],
                })(
                  <Input
                    // disabled={submitting}
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                    placeholder="Recipient"
                    onChange={this.handleRecipientChange}
                  />,
                )}
              </Form.Item>
              <Form.Item
                label="Memo (public)"
              >
                {getFieldDecorator('memo', {
                  rules: [ { required: false } ],
                })(
                  <Input
                    // disabled={submitting}
                    prefix={<Icon type="message" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                    placeholder="Optional message"
                    maxLength={140}/>,
                )}
              </Form.Item>
            </Panel>
          </Collapse>
          <Form.Item>
            <Button
              block
              type="primary"
              htmlType="submit"
            >
              {dailyRewardRecipient}
            </Button>

            <Button
              block
              type="secondary"
              onClick={this.handleClaimToTipBalance}
            >
              Claim to My Tip Balance
            </Button>
          </Form.Item>
        </Form>
      );
    }

    return (
      <Spin spinning={this.props.claimVestingRewardLoading}>
        <Card title="Daily Reward">{/* TODO: fix card Meta to use icon in title */}
          
          {/* <p>Cycle: <span style={{float: 'right'}}>{globalProperties.vr_cycle}</span></p> */}
          <div className="info">Cycle ends in <Countdown value={deadline} onFinish={this.timerEnded}/></div>

          {renderContent}
          <div style={{pointerEvents: 'none'}}>
            <Reward
              ref={(ref) => {
                this.reward = ref
              }}
              type='confetti'
              springAnimation={false}
            >
              <div/>
            </Reward>
          </div>
        </Card>
      </Spin>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { auth, wallet } = state;
  const { globalProperties } = wallet;
  const { user } = auth;

  let max = 0; // max WLS reward this cycle
  let remain = 0; // max WLS claimable
  let claimedAmount = 0;
  if (user && globalProperties) {
    const { vr_cycle, vr_snaphost_shares, vr_snaphost_payout } = globalProperties;

    let account_vshares = 0;
    if (user.vr_snaphost_next_cycle < vr_cycle) {
      account_vshares = parseFloat(user.vesting_shares.split(' ')[0]);
    } else if (user.vr_snaphost_cycle < vr_cycle) {
      account_vshares = parseFloat(user.vr_snaphost_shares.split(' ')[0]);
    }

    const fund = parseFloat(vr_snaphost_payout.split(' ')[0]);
    const total_vest = parseFloat(vr_snaphost_shares.split(' ')[0]);
    max = account_vshares * fund / total_vest;
    max = truncateDecimal(max, 3);
    claimedAmount = (user.vr_claimed_cycle === vr_cycle) ? parseFloat(user.vr_claimed_amount.split(' ')[0]) : 0;
    remain = (~~(max*1000) - ~~(claimedAmount*1000))/1000; // max - claimedAmount;
    remain = truncateDecimal(remain, 3);

    // console.log(`fund=${fund}, account_vshares=${account_vshares}, total_vest=${total_vest}, max=${max}, claimedAmount=${claimedAmount}, remain=${remain}`);
  }

  return {
    ...ownProps,
    ...wallet,
    user,
    max,
    claimedAmount,
    remain
  };
};
const mapDispatchToProps = {
  claimDailyReward,
  claimDailyRewardToTipBalance,
  reload,
  notify
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withForm = Form.create({ name: 'dailyReward' });
export default compose(
  withConnect,
  withForm
)(DailyVestingReward);
