import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import { message } from 'antd';
import { MAXIMUM_UPLOAD_SIZE_HUMAN } from '../../helpers/image';
import { sortComments } from '../../helpers/sortHelpers';
import Loading from '../Icon/Loading';
import SortSelector from '../SortSelector/SortSelector';
import CommentForm from './CommentForm';
import Comment from './Comment';
import './Comments.less';

const { Item } = SortSelector;

@injectIntl
class Comments extends React.Component {
  static propTypes = {
    authenticated     : PropTypes.bool.isRequired,
    comments          : PropTypes.arrayOf(PropTypes.shape()),
    commentsChildren  : PropTypes.shape(),
    intl              : PropTypes.shape().isRequired,
    loading           : PropTypes.bool,
    notify            : PropTypes.func,
    onLikeClick       : PropTypes.func,
    onSendComment     : PropTypes.func,
    parentPost        : PropTypes.shape(),
    pendingVotes      : PropTypes.arrayOf(
                          PropTypes.shape({
                            id: PropTypes.number,
                            percent: PropTypes.number,
                          }),
                        ),    
    rewriteLinks      : PropTypes.bool,
    show              : PropTypes.bool,
    user              : PropTypes.shape().isRequired,
    username          : PropTypes.string,
  };

  static defaultProps = {
    comments          : [],
    commentsChildren  : undefined,
    loading           : false,
    notify            : () => {},
    onLikeClick       : () => {},
    onSendComment     : () => {},
    parentPost        : undefined,
    pendingVotes      : [],
    rewriteLinks      : false,
    show              : false,
    username          : undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      commentFormText         : '',
      commentSubmitted        : false,
      isRestricted            : null,
      showCommentFormLoading  : false,
      sort                    : 'OLDEST',
    };

    this.detectSort           = this.detectSort.bind(this);
    this.handleSortChange     = this.handleSortChange.bind(this);
    this.handleSubmitComment  = this.handleSubmitComment.bind(this);
    this.isRestrictedCallback = this.isRestrictedCallback.bind(this);
    this.setSort              = this.setSort.bind(this);
  }

  componentDidMount() {
    this.detectSort(this.props.comments);
  }

  componentWillReceiveProps(nextProps) {
    this.detectSort(nextProps.comments);
  }

  setSort(sort) {
    this.setState({
      sort,
    });
  }

  detectSort(comments) {
    if (comments.length > 0 && comments.filter(comment => comment.comment_tips.length > 0).length === 0) {
      this.setSort('OLDEST');
    }
  }

  handleSortChange(type) {
    this.setSort(type);
  }
  
  handleImageInvalid = () => {
    const { formatMessage } = this.props.intl;
    this.props.notify(
      formatMessage(
        {
          id: 'notify_uploading_image_invalid',
          defaultMessage: 'This file is invalid. Only image files with maximum size of {size} are supported',
        },
        { size: MAXIMUM_UPLOAD_SIZE_HUMAN },
      ),
      'error',
    );
  };

  handleSubmitComment(parentPost, commentValue, storage) {
    const { intl } = this.props;

    this.setState({ showCommentFormLoading: true });
    return this.props
      .onSendComment(parentPost, commentValue, storage)
      .then(() => {
        message.success(
          intl.formatMessage({
            id: 'notify_comment_sent',
            defaultMessage: 'Comment submitted',
          }),
        );

        this.setState({
          commentFormText         : '',
          commentSubmitted        : true,
          showCommentFormLoading  : false,
        });
      })
      .catch(() => {
        this.setState({
          commentFormText         : commentValue,
          showCommentFormLoading  : false,
        });
        return {
          error: true,
        };
      });
  }

  isRestrictedCallback(data) {
    // TODO:triggers a "Cannot update during an existing state transition" warning
    this.setState({isRestricted:data});
  }

  render() {
    const {
      authenticated,
      comments,
      commentsChildren,
      loading,
      onLikeClick,
      pendingVotes,
      rewriteLinks,
      show,
      user,
      username,
    } = this.props;
    const { isRestricted, sort } = this.state;

    const restrictedChecked = this.state.isRestricted !== undefined;

    return (
      <div className="Comments">
        <div className="Comments__header">
          {Boolean(comments.length) && (
            <>
              <h2>
                <FormattedMessage id="comments" defaultMessage="Comments" />
              </h2>
              <SortSelector sort={sort} onChange={this.handleSortChange}>
                <Item key="BEST">
                  <FormattedMessage id="sort_best" defaultMessage="Best" />
                </Item>
                <Item key="NEWEST">
                  <FormattedMessage id="sort_newest" defaultMessage="Newest" />
                </Item>
                <Item key="OLDEST">
                  <FormattedMessage id="sort_oldest" defaultMessage="Oldest" />
                </Item>         
              </SortSelector>
            </>
          )}
        </div>

        {authenticated && username && restrictedChecked && (
          <CommentForm
            inputValue      = {this.state.commentFormText}
            isLoading       = {this.state.showCommentFormLoading}
            onSubmit        = {this.handleSubmitComment}
            parentCallback  = {this.isRestrictedCallback}
            parentPost      = {this.props.parentPost}
            submitted       = {this.state.commentSubmitted}
            top
            username        = {username}
          />
        )}
        {loading && <Loading />}
        {!loading &&
          show &&
          comments &&
          restrictedChecked &&
          sortComments(comments, sort).map(comment => (
            <Comment
              authenticated     = {authenticated}
              comment           = {comment}
              commentsChildren  = {commentsChildren}
              depth             = {0}
              isRestricted      = {isRestricted}
              key               = {comment.id}
              notify            = {this.props.notify}
              onLikeClick       = {onLikeClick}
              onSendComment     = {this.props.onSendComment}
              parent            = {this.props.parentPost}
              pendingVotes      = {pendingVotes}
              rewriteLinks      = {rewriteLinks}
              rootPostAuthor    = {this.props.parentPost && this.props.parentPost.author}
              sort              = {sort}
              user              = {user}
              username          = {username}
            />
          ))}
      </div>
    );
  }
}

export default Comments;
