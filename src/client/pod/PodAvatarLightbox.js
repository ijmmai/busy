import React from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-image-lightbox';
import PodAvatar from './PodAvatar';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

class PodAvatarLightbox extends React.Component {
  static propTypes = {
    podname: PropTypes.string,
    size: PropTypes.number,
  };

  static defaultProps = {
    podname: undefined,
    size: 100,
  };

  state = {
    open: false,
  };

  handleAvatarClick = () => this.setState({ open: true });

  handleCloseRequest = () => this.setState({ open: false });

  render() {
    const {podname, size} = this.props;

    return (
      <div className="AvatarLightbox">
        <a role="presentation" onClick={this.handleAvatarClick}>
          <PodAvatar podname={podname} size={size}/>
        </a>
        {this.state.open && (
          <Lightbox
            mainSrc={`${WLS_IMG_PROXY}/pod_profileimage/${podname}/128x128`}
            onCloseRequest={this.handleCloseRequest}
          />
        )}
      </div>
    );
  }
}

export default PodAvatarLightbox;
