import { createCipher, createDecipher } from 'crypto';

export default class Keys {
  password = null;
  constructor(password = null) {
    this.setPassword(password);
  }
  isLocked() {
    return this.password === null;
  }
  lock() {
    this.password = null;
    return this;
  }
  setPassword(password) {
    this.password = password;
    return this;
  }
  remove() {
    localStorage.removeItem('auth');
    return this;
  }
  encrypt(username, keys) {
    try {
      const hasWhaleVault = keys.postingKey.endsWith(':');
      if (this.password === '') {
        localStorage.setItem('auth', JSON.stringify({ username, keys, hasPass: this.password !== '', hasWhaleVault }));
        return;
      }
      const cipher = createCipher('aes-256-cbc', this.password);
      const encrypted = Buffer.concat([cipher.update(new Buffer(JSON.stringify(keys), 'utf8')), cipher.final()]);
      localStorage.setItem(
        'auth',
        JSON.stringify({ username, keys: encrypted, hasPass: this.password !== '', hasWhaleVault }),
      );
    } catch (e) {
      throw new Error(e.message);
    }
  }
  decrypt() {
    try {
      const encrypted = JSON.parse(localStorage.getItem('auth'));
      if (!encrypted.hasPass) {
        return encrypted;
      } else if (this.isLocked()) {
        return {
          username: encrypted.username,
          keys: null,
          hasPass: encrypted.hasPass,
          hasWhaleVault: encrypted.hasWhaleVault,
        };
      }
      const decipher = createDecipher('aes-256-cbc', this.password);
      const decrypted = Buffer.concat([decipher.update(new Buffer(encrypted.keys, 'utf8')), decipher.final()]);
      return {
        username: encrypted.username,
        keys: JSON.parse(decrypted.toString()),
        hasPass: this.password !== '',
        hasWhaleVault: encrypted.hasWhaleVault,
      };
    } catch (e) {
      return false;
    }
  }
  testPassword() {
    try {
      const decrypted = this.decrypt();
      if (decrypted && decrypted.keys && decrypted.keys.postingKey) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }
}
