import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getIsAuthenticated,
  getIsLocked,
  getHasPass,
} from '../reducers';
import { openLoginModal } from './authActions';
import LoginModal from '../components/LoginModal';

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default function withAuthActions(WrappedComponent) {
  @connect(state => ({
    authenticated: getIsAuthenticated(state),
    isLocked: getIsLocked(state),
    hasPass: getHasPass(state),
  }), {
    openLoginModal
  })
  class Wrapper extends React.Component {
    static propTypes = {
      authenticated: PropTypes.bool,
      isLocked: PropTypes.bool,
      hasPass: PropTypes.bool,
      openLoginModal: PropTypes.func,
    };

    static defaultProps = {
      authenticated: false,
      isLocked: true,
      hasPass: true,
      openLoginModal: () => {},
    };

    constructor(props) {
      super(props);
      this.state = {
        displayLoginModal: false,
      };

      this.handleActionInit = this.handleActionInit.bind(this);
      this.displayLoginModal = this.displayLoginModal.bind(this);
      this.hideLoginModal = this.hideLoginModal.bind(this);
    }

    displayLoginModal() {
      this.setState({
        displayLoginModal: true,
      });
    }

    hideLoginModal() {
      this.setState({
        displayLoginModal: false,
      });
    }

    handleActionInit(callback, failCallback = () => Promise.reject()) {
      if (this.props.authenticated && (!this.props.isLocked || !this.props.hasPass)) {
        return callback();
      } else if(this.props.authenticated) {
        this.props.openLoginModal();
        return failCallback();
      }
      this.displayLoginModal();
      return failCallback();
    }

    render() {
      return [
        <LoginModal
          key="login-modal"
          visible={this.state.displayLoginModal}
          handleLoginModalCancel={this.hideLoginModal}
        />,
        <WrappedComponent
          key="wrapped-component"
          onActionInitiated={this.handleActionInit}
          {...this.props}
        />,
      ];
    }
  }

  Wrapper.displayName = `withAuthActions(${getDisplayName(WrappedComponent)})`;

  return Wrapper;
}
