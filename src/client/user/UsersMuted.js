import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Table } from "antd";
import Avatar from '../components/Avatar';
import { getMutedList, getAuthenticatedUserName } from "../reducers";
import Loading from "../components/Icon/Loading";
import '../styles/modules/account_list.less';

class UsersMuted extends React.Component {

  render() {
    const { mutedList, username } = this.props;
    const ownProfile = username === this.props.match.params.name;

    const columns = [
      {
        title     : '',
        dataIndex : 'account',
        key       : 'avatar',
        width     : 1,
        render    : (text, record) => <Avatar size={32} username={record.account} />
      },
      {
        title     : () => {return ownProfile ? `Members You Muted` : ''},
        dataIndex : 'account',
        key       : 'account',
        className : 'account',
        render    : (text, record) => <Link to={`/@${record.account}`}>{text}</Link>,
      },
    ];


    let data = [];
    if (mutedList && ownProfile) {
      data = this.props.mutedList.map( (item) => {
        return {key:item, account: item };
      });
    }

    return (
      <div className="main-body">
        <div className="main-content AccList UseList">
          <Table 
            key        = 'muted-list'
            pagination = {false}
            columns    = {columns}
            dataSource = {data}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    mutedList : getMutedList(state),
    username  : getAuthenticatedUserName(state),
  };
};

export default connect(
  mapStateToProps,
)(UsersMuted);
