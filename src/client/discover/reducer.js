import produce from 'immer';

export const SET = '@discoverUsers/SET';

const initialState = {
  tag: '',             // filtered by tag, empty means all tags
  start: '',           // to filter by a,b,c,...
  users: false,        // list of user for browsing
  hasMore: false,
  loadingMore: false,
};

const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET:
        draft[action.payload.key] = action.payload.value;
        break;
      default:
        break;
    }
  });

export default reducer;
