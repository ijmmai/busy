import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { take, find } from 'lodash';
import { injectIntl, FormattedNumber, FormattedMessage } from 'react-intl';
import Icon from 'antd/lib/icon';
import { sortTips } from '../../helpers/sortHelpers';
import BTooltip from '../BTooltip';
import ReactionsModal from '../Reactions/ReactionsModal';
import withAuthActions from '../../auth/withAuthActions';
import WLSDisplay from '../Utils/WLSDisplay';
import IconTip from '../../../../assets/images/svgs/IconTips.svg';
import './Buttons.less';

const MAX_TIPS_PREVIEW = 10;

const DisplayTips = (props) => {
  const tips          = props.tips;
  const moreTips      = tips.length - MAX_TIPS_PREVIEW;
  const moreTipsText  = moreTips > 0
                        ? <FormattedMessage id='and_more_amount' defaultMessage='and {amount} more' values={{ amount: moreTips }} />
                        : '';
  const tipPhrase     = tips.length > 1
                        ? <FormattedMessage id="tipPlural" defaultMessage="Tips" />
                        : <FormattedMessage id="tipSingular" defaultMessage="Tip" />

  const tipsPreview   = take(tips, MAX_TIPS_PREVIEW).map(
                          vote => (
                            <p key = {`${vote.tipper}{"-"}${props.id}`}>
                              {vote.tipper}
                              <span style={{ opacity: '0.5' }}>
                                {' '}{vote.amount}
                              </span>
                            </p>
                          )
                        );

  return (
    <>
      <span className="button">
        <span
          className = {classNames('CommentFooter__count', {
                          'CommentFooter__count--clickable': false,
                      })}
          role      = "presentation"
          onClick   = {props.onClick}
        >
          <BTooltip title=<div>{tipsPreview}{moreTipsText}</div>>
            {tips.length}{' '}{tipPhrase}{' '}
          </BTooltip>
        </span>
      </span>
      <DisplayTotalTipAmount amount={props.totalAmount} />
    </>
  );
};

const DisplayTotalTipAmount = (props) => {
  if (props.amount === '0.000 WLS') return null;

  return (
    <span className="button">
    <span className="CommentFooter__payout">
      <BTooltip title={props.amount} >
        <WLSDisplay value={parseFloat(props.amount)} fraction={0}/>
      </BTooltip>
    </span>
    </span>
  );
};

const DisplayReplyButton = (props) => {
  return (
    <span className="button">
      <a
        role                  = "presentation"
        className             = {classNames('CommentFooter__link', {
                                  'CommentFooter__link--active': props.replying,
                                })}
        onClick               = {props.onClick}
      >
        <FormattedMessage id="reply" defaultMessage="Reply" />
      </a>
    </span>
  );
};

@injectIntl
@withAuthActions
class Buttons extends React.Component {
  static propTypes = {
    intl                : PropTypes.shape().isRequired,
    user                : PropTypes.shape().isRequired,
    comment             : PropTypes.shape().isRequired,
    onActionInitiated   : PropTypes.func.isRequired,
    ownPost             : PropTypes.bool,
    editable            : PropTypes.bool,
    editing             : PropTypes.bool,
    replying            : PropTypes.bool,
    pendingVotes        : PropTypes.arrayOf(
                            PropTypes.shape({
                              id      : PropTypes.number,
                              percent : PropTypes.number,
                            }),
                          ),
    onLikeClick         : PropTypes.func,
    onReplyClick        : PropTypes.func,
    onEditClick         : PropTypes.func,
  };

  static defaultProps = {
    editable      : false,
    editing       : false,
    replying      : false,
    ownPost       : false,
    pendingVotes  : [],
    onLikeClick   : () => {},
    onReplyClick  : () => {},
    onEditClick   : () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      reactionsModalVisible : false,
      sortedTips            : this.props.comment.comment_tips.sort(sortTips),
    };

    this.handleCloseReactions = this.handleCloseReactions.bind(this);
    this.handleLikeClick      = this.handleLikeClick.bind(this);
    this.handleShowReactions  = this.handleShowReactions.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.comment.comment_tips !== this.props.comment.comment_tips) {
      let tips = this.props.comment.comment_tips;
  
      if (tips.length > 1) {
        tips = tips.sort(sortTips);
      }

      this.setState({sortedTips:tips});
    }   
  }

  handleLikeClick() {
    this.props.onActionInitiated(this.props.onLikeClick);
  }

  handleShowReactions() {
    this.setState({
      reactionsModalVisible: true,
    });
  }

  handleCloseReactions() {
    this.setState({
      reactionsModalVisible: false,
    });
  }

  render() {
    const { intl, user, comment, pendingVotes, editable, editing, replying, ownPost, isRestricted } = this.props;
    const { sortedTips }  = this.state;

    const pendingLike     = pendingVote && (pendingVote.amount > 0);
    const pendingVote     = find(pendingVotes, { id: comment.id });
    const userVote        = find(comment.comment_tips, { tipper: user.name });
    const userUpVoted     = userVote && userVote.tipper;

    let likeTooltip = <span>{intl.formatMessage({ id: 'like' })}</span>;
    if (userUpVoted) {
      likeTooltip = <span>{intl.formatMessage({ id: 'like_more' })}</span>;
    }

    return (
      <div className="CommentFooter__wrapper">
        <div className="one">

          {!ownPost && (
            <span className="button">
              <BTooltip title={likeTooltip}>
                <a
                  role="presentation"
                  className={classNames('CommentFooter__link', {
                    'CommentFooter__link--active': userUpVoted,
                  })}
                  onClick={this.handleLikeClick}
                >
                  {pendingLike ? (
                    <Icon type="loading" />
                  ) : (
                    <Icon component={IconTip} className="iconfont" />
                  )}{' '}
                </a>
              </BTooltip>
            </span>
          )}

          {sortedTips.length > 0 && (
            <DisplayTips tips={sortedTips} totalAmount={comment.total_payout_value} id={comment.id} onClick={this.handleShowReactions}/>
          )}

          {user.name && !isRestricted && !ownPost && (
            <DisplayReplyButton onClick={this.props.onReplyClick} />
          )}
          
          {user.name && !isRestricted && ownPost && editable && (
            <span className="button">
              <a
                role      = "presentation"
                className = {classNames('CommentFooter__link', {
                              'CommentFooter__link--active': editing,
                            })}
                onClick   = {this.props.onEditClick}
              >
                <FormattedMessage id="edit" defaultMessage="Edit" />
              </a>
            </span>
          )}

          <ReactionsModal
            onClose = {this.handleCloseReactions}
            upVotes = {sortedTips}
            visible = {this.state.reactionsModalVisible}
          />
        </div>
      </div>
    );
  }
}

export default Buttons;
