import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import { FormattedMessage } from 'react-intl';
import './UserMenu.less';

class UserWalletMenu extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    defaultKey: PropTypes.string,
  };

  static defaultProps = {
    onChange: () => {},
    defaultKey: 'wallet',
  };

  constructor(props) {
    super(props);
    this.state = {
      current: props.defaultKey ? props.defaultKey : 'wallet',
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.defaultKey ? nextProps.defaultKey : 'wallet',
    });
  }

  getItemClasses = key =>
    classNames('UserMenu__item', { 'UserMenu__item--active': this.state.current === key });

  handleClick = e => {
    const key = e.currentTarget.dataset.key;
    this.setState({ current: key }, () => this.props.onChange(key));
  };

  render() {
    return (
      <div className="UserMenu UserWalletMenu">
        <div className="container menu-layout">
          <div className="left" />
          <Scrollbars
            universal
            autoHide
            renderView={({ style, ...props }) => (
              <div style={{ ...style, marginBottom: '-20px' }} {...props} />
            )}
            style={{ width: '100%', height: 32 }}
          >
            <ul className="UserMenu__menu">
              <li
                className={this.getItemClasses('wallet')}
                onClick={this.handleClick}
                role="presentation"
                data-key="wallet"
              >
                <FormattedMessage id="all" defaultMessage="All" />
              </li>
              <li
                className={this.getItemClasses('author_reward')}
                onClick={this.handleClick}
                role="presentation"
                data-key="author_reward"
              >
                <FormattedMessage id="author_reward" defaultMessage="Author reward" />
              </li>
              <li
                className={this.getItemClasses('curation_reward')}
                onClick={this.handleClick}
                role="presentation"
                data-key="curation_reward"
              >
                <FormattedMessage id="curation_reward" defaultMessage="Curation reward" />
              </li>
            </ul>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

export default UserWalletMenu;
