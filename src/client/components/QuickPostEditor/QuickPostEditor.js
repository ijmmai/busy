import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import { Input } from 'antd';
import uuidv4 from 'uuid/v4';
import { injectIntl, FormattedMessage } from 'react-intl';
import { getAuthenticatedUser, getIsEditorLoading } from '../../reducers';
import { isValidImage, MAXIMUM_UPLOAD_SIZE } from '../../helpers/image';
import { notify } from '../../app/Notification/notificationActions';
import withEditor from '../Editor/withEditor';
import withAuthActions from '../../auth/withAuthActions';
import { createPost } from '../../post/Write/editorActions';
import Avatar from '../Avatar';
import QuickPostEditorFooter from './QuickPostEditorFooter';
import { POST_AUDIENCE_FRIENDS } from '../../helpers/postHelpers';
import './QuickPostEditor.less';

const version = require('../../../../package.json').version;

const CharCounter = (props) => {
  const { count, focus } = props;
  if (!focus) return null;

  let className         = (count > 200) ? 'CharCounter__half' : 'CharCounter__empty';
  className             = (count == 255) ? 'CharCounter__full' : className;

  return <div className={className}>{count}/255</div>;
}

@withRouter
@injectIntl
@withEditor
@connect(
  state => ({
    user: getAuthenticatedUser(state),
    postCreationLoading: getIsEditorLoading(state),
    userPods: state.user.pods,
  }),
  {
    notify,
    createPost,
  },
)
@withAuthActions
class QuickPostEditor extends React.Component {
  static propTypes = {
    postCreationLoading : PropTypes.bool.isRequired,
    user                : PropTypes.shape().isRequired,
    intl                : PropTypes.shape().isRequired,
    location            : PropTypes.shape().isRequired,
    userPods            : PropTypes.arrayOf(PropTypes.shape()),
    notify              : PropTypes.func.isRequired,
    createPost          : PropTypes.func.isRequired,
    onImageUpload       : PropTypes.func,
    onImageInvalid      : PropTypes.func,
    onActionInitiated   : PropTypes.func,
  };

  static defaultProps = {
    userPods          : [],
    onImageUpload     : () => {},
    onImageInvalid    : () => {},
    onActionInitiated : () => {},
  };

  constructor (props) {
    super(props);

    this.state = {
      noContent       : false,
      imageUploading  : false,
      dropzoneActive  : false,
      currentImages   : [],
      focusedInput    : false,
      inputMinRows    : 1,
      interest        : 'off-topic',
      destination     : '',
      charCount       : 0,
    };

    this.input = React.createRef();
  }

  getQuickPostData = () => {
    //const currentPaths = this.props.location.pathname.split('/');
    const defaultTag  = this.state.interest;
    const tag         = defaultTag;  //currentPaths[2];
    const tags        = [];
    const images      = _.map(this.state.currentImages, image => image.src);
    const postBody    = _.reduce(
                          this.state.currentImages,
                          (str, image) => {
                            const imageText = `![${image.name}](${image.src})\n`;
                            return `${str}${imageText}`;
                          },
                          ' ',
                        );
    const postTitle   = this.input.current.value;
    const data        = {
                          body          : postBody,
                          title         : postTitle,
                          reward        : '100',
                          author        : this.props.user.name,
                          parentAuthor  : '',
                          lastUpdated   : Date.now(),
                          upvote        : false,
                          post_to       : this.state.destination,
                        };

    const metaData    = {
                          app     : `wls/${version}`,
                          format  : 'markdown',
                        };

    if (images.length) {
      metaData.image = images;
    }

    if (!_.isEmpty(tag)) {
      tags.push(tag);
    } else {
      tags.push(defaultTag);
    }

    metaData.tags = tags;

    data.parentPermlink = _.isEmpty(tag) ? defaultTag : tag;
    data.permlink       = _.kebabCase(postTitle);
    data.jsonMetadata   = metaData;

    return data;
  };

  insertImage = (image, imageName = 'image') => {
    if (!this.input) return;
    const newImage = {
                      src   : image,
                      name  : imageName,
                      id    : uuidv4(),
                    };
    this.setState({
      currentImages : _.concat(this.state.currentImages, newImage),
      focusedInput  : true,
    });
  };

  handleDrop = files => {
    if (files.length === 0) {
      this.setState({
        dropzoneActive: false,
      });
      return;
    }

    this.setState({
      dropzoneActive : false,
      imageUploading : true,
    });
    let callbacksCount = 0;
    Array.from(files).forEach(item => {
      this.props.onImageUpload(
        item,
        (image, imageName) => {
          callbacksCount += 1;
          this.insertImage(image, imageName);
          if (callbacksCount === files.length) {
            this.setState({
              imageUploading: false,
            });
          }
        },
        () => {
          this.setState({
            imageUploading: false,
          });
        },
      );
    });
  };

  handleImageChange = e => {
    e.preventDefault();
    e.stopPropagation();

    if (e.target.files && e.target.files[0]) {
      if (!isValidImage(e.target.files[0])) {
        this.props.onImageInvalid();
        return;
      }

      this.setState({
        imageUploading: true,
      });
      this.props.onImageUpload(e.target.files[0], this.disableAndInsertImage, () =>
        this.setState({
          imageUploading: false,
        }),
      );
      // Input reacts on value change, so if user selects the same file nothing will happen.
      // We have to reset its value, so if same image is selected it will emit onChange event.
      e.target.value = '';
    }
  };

  handleDragEnter  = () => this.setState({ dropzoneActive: true });
  handleDragLeave  = () => this.setState({ dropzoneActive: false });

  handleFocusInput = () => this.setState({ focusedInput: true, inputMinRows: 2 });

  disableAndInsertImage = (image, imageName = 'image') => {
    this.setState({
      imageUploading: false,
    });
    this.insertImage(image, imageName);
  };

  handleCreatePost = () => {
    if (_.isEmpty(this.input.current.value)) {
      this.props.notify(
        this.props.intl.formatMessage({
          id: 'quick_post_error_empty_title',
          defaultMessage: 'Post title cannot be empty.',
        }),
        'error',
      );
      return;
    }
    const data = this.getQuickPostData();
    this.props.onActionInitiated(() => this.props.createPost(data));
  };

  handleTextInputChange = (e) => {
    this.input.current = e.target;
    this.setState({charCount : this.input.current.textLength});
  };

  handleRemoveImage = currentImage => {
    const imageIndex    = _.findIndex(this.state.currentImages, image => image.id === currentImage.id);
    const currentImages = [...this.state.currentImages];
    currentImages.splice(imageIndex, 1);
    this.setState({ currentImages });
  };

  handleChange = name => selectedOption => {
    this.setState({ [name] : selectedOption });
  }

  render() {
    const { charCount, imageUploading, focusedInput, currentImages, inputMinRows } = this.state;
    const { user, postCreationLoading, intl, userPods } = this.props;

    const placeholder         = intl.formatMessage({
                                  id: 'write_quick_post',
                                  defaultMessage: 'Write quick post (max 255 chars)',
                                });
    const submittingPostText  = intl.formatMessage({
                                  id: 'post_send_progress',
                                  defaultMessage: 'Submitting',
                                })
    const postText            = intl.formatMessage({
                                  id: 'post_send',
                                  defaultMessage: 'Post',
                                })

    return (
      <div className="QuickPostEditor">
        <div className="QuickPostEditor__contents">
          <div className="QuickPostEditor__avatar">
            <Avatar username={user.name} size={40} />
          </div>
          <div className="QuickPostEditor__dropzone-base">
            <Dropzone
              disableClick
              style           = {{ flex: 1 }}
              accept          = "image/*"
              maxSize         = {MAXIMUM_UPLOAD_SIZE}
              onDropRejected  = {this.props.onImageInvalid}
              onDrop          = {this.handleDrop}
              onDragEnter     = {this.handleDragEnter}
              onDragLeave     = {this.handleDragLeave}
            >
              {this.state.dropzoneActive && (
                <div className="QuickPostEditor__dropzone">
                  <div>
                    <i className="iconfont icon-picture" />
                    <FormattedMessage id="drop_image" defaultMessage="Drop your images here" />
                  </div>
                </div>
              )}
              <Input.TextArea
                autosize    = {{ minRows: inputMinRows, maxRows: 12 }}
                onFocus     = {this.handleFocusInput}
                onChange    = {this.handleTextInputChange}
                ref         = {this.input}
                placeholder = {placeholder}
                maxLength   = "255"
              />
            </Dropzone>
          </div>
          <div className='CharCounter'>
            <CharCounter count={charCount} focus={focusedInput} />
          </div>
        </div>
        {focusedInput && (
          <QuickPostEditorFooter
            imageUploading          = {imageUploading}
            postCreationLoading     = {postCreationLoading}
            handleCreatePost        = {this.handleCreatePost}
            handleImageChange       = {this.handleImageChange}
            postText                = {postText}
            submittingPostText      = {submittingPostText}
            currentImages           = {currentImages}
            onRemoveImage           = {this.handleRemoveImage}
            handleChange            = {this.handleChange}
            interest                = {this.state.interest}
            destination             = {this.state.destination}
            userPods                = {userPods}
          />
        )}
      </div>
    );
  }
}

export default QuickPostEditor;
