require('dotenv').config();
const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const postcssFlexbugs = require('postcss-flexbugs-fixes');
const configUtils = require('./configUtils');

const baseDir = path.resolve(__dirname, '..');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: path.resolve(baseDir, './src/client/index.js'),
  output: {
    filename: 'bundle.js',
    publicPath: '/js/',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        ENABLE_LOGGER: JSON.stringify(process.env.ENABLE_LOGGER),
        WLS_RPC_URL: JSON.stringify(process.env.WLS_RPC_CLIENT_URL || 'https://pubrpc.whaleshares.io'),
        WALLET_URL: JSON.stringify(process.env.WALLET_URL || 'https://wallet.whaleshares.io'),
        EXPLORER_URL: JSON.stringify(process.env.EXPLORER_URL || 'https://explorer.whaleshares.io'),
        IS_BROWSER: JSON.stringify(true),
      },
    }),
  ],
  module: {
    rules: [
      {
        test: configUtils.MATCH_JS_JSX,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?.+)?$/,
        loader: 'url-loader',
      },
      {
        test: configUtils.MATCH_CSS_LESS,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                postcssFlexbugs,
                autoprefixer({
                  browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                  ],
                }),
              ],
            },
          },
          {
            loader: 'less-loader',
            options: {
                javascriptEnabled: true
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      }
    ],
  },
  devServer: {
    port: 3000,
    contentBase: [path.resolve(baseDir, 'templates'), path.resolve(baseDir, 'assets')],
    historyApiFallback: {
      disableDotRule: true,
    },
    // headers: {
    //   'Access-Control-Allow-Origin': '*',
    // },
  },
};
