import React from 'react';
import PropTypes from 'prop-types';
import './Avatar.less';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

export function getAvatarURL(username, size = 100) {
  let wxh = 128;
  if (size <= 32) {
    wxh = 32;
  } else if (size <= 48) {
    wxh = 48;
  } else if (size <= 64) {
    wxh = 64;
  } else if (size <= 96) {
    wxh = 96;
  } else {
    wxh = 128;
  }

  return `${WLS_IMG_PROXY}/profileimage/${username}/${wxh}x${wxh}`;
}

const Avatar = ({ username, size }) => {
  let style = {
    minWidth: `${size}px`,
    width: `${size}px`,
    height: `${size}px`,
  };

  const url = getAvatarURL(username, size);

  if (username) {
    style = {
      ...style,
      backgroundImage: `url(${url})`,
    };
  }

  return <div className="Avatar" style={style} />;
};

Avatar.propTypes = {
  username: PropTypes.string.isRequired,
  size: PropTypes.number,
};

Avatar.defaultProps = {
  size: 100,
};

export default Avatar;
