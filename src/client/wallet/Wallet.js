import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { injectIntl, FormattedMessage } from 'react-intl';
import Affix from '../components/Utils/Affix';
import SidenavMain from '../components/Navigation/SidenavMain';
import UserWallet from '../user/UserWallet';
import requiresLogin from '../auth/requiresLogin';

const Wallet = ({ intl }) => (
  <div className="shifted">
    <Helmet>
      <title>{intl.formatMessage({ id: 'wallet', defaultMessage: 'Rewards' })}</title>
    </Helmet>
    <div className="layout-twocol container">
      <Affix className="leftContainer" stickPosition={77}>
        <div className="left">
          <SidenavMain />
        </div>
      </Affix>
      <div className="center main-body UserWallet">
        <div className="main-title">
          <h1>
            <FormattedMessage id="status" defaultMessage="Status" />
          </h1>
        </div>
        <UserWallet isCurrentUser />
      </div>
    </div>
  </div>
);

Wallet.propTypes = {
  intl: PropTypes.shape().isRequired,
};

export default requiresLogin(injectIntl(Wallet));
