import React from 'react';
import PropTypes from 'prop-types';
import url from 'url';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';
import { withRouter, Redirect } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { ConfigProvider, Layout } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import { getAvailableLocale, getTranslationsByLocale, getLocaleDirection } from './translations';
import { getIsLoaded, getAuthenticatedUser, getAuthenticatedUserName, getLocale, getUsedLocale } from './reducers';
import { login, logout } from './auth/authActions';
import { openWhaletoken } from './wallet/walletActions';
import { getFollowing, getNotifications, getMuted, getUserPods } from './user/userActions';
import { setUsedLocale, busyAPIHandler, setAppUrl } from './app/appActions';
import busyAPI from './busyAPI';
//import Redirect from './components/Utils/Redirect';
import NotificationPopup from './notifications/NotificationPopup';
import Topnav from './components/Navigation/Topnav';
import TopnavLanding from './landing/TopnavLanding';
// import Transfer from './wallet/Transfer';
import LoginModal from './components/LoginModalWithPassword';
import Loading from './components/Icon/Loading';
import WhaleTokenModal from './wallet/WhaleToken';

import { fetchFriends, fetchPendingReceivedRequests } from './friends/actions';

import { WALLET_URL } from '../common/constants/settings';

@withRouter
@connect(
  state => ({
    loaded: getIsLoaded(state),
    user: getAuthenticatedUser(state),
    username: getAuthenticatedUserName(state),
    usedLocale: getUsedLocale(state),
    locale: getLocale(state),
  }),
  {
    login,
    logout,
    getFollowing,
    fetchFriends,
    fetchPendingReceivedRequests,
    getNotifications,
    busyAPIHandler,
    setUsedLocale,
    getMuted,
    openWhaletoken,
    getUserPods,
  },
)
class Wrapper extends React.PureComponent {
  static propTypes = {
    route: PropTypes.shape().isRequired,
    loaded: PropTypes.bool.isRequired,
    user: PropTypes.shape().isRequired,
    locale: PropTypes.string.isRequired,
    location: PropTypes.shape({ pathname: PropTypes.string }).isRequired, // from withRouter
    usedLocale: PropTypes.string.isRequired,
    history: PropTypes.shape().isRequired,
    username: PropTypes.string,
    login: PropTypes.func,
    logout: PropTypes.func,
    getFollowing: PropTypes.func,
    fetchFriends: PropTypes.func,
    fetchPendingReceivedRequests: PropTypes.func,
    getNotifications: PropTypes.func,
    setUsedLocale: PropTypes.func,
    busyAPIHandler: PropTypes.func,
    getMuted: PropTypes.func,
    openWhaletoken: PropTypes.func,
    getUserPods: PropTypes.func,
  };

  static defaultProps = {
    username: '',
    login: () => {},
    logout: () => {},
    getFollowing: () => {},
    fetchFriends: () => {},
    fetchPendingReceivedRequests: () => {},
    getNotifications: () => {},
    setUsedLocale: () => {},
    busyAPIHandler: () => {},
    getMuted: () => {},
    openWhaletoken: () => {},
    getUserPods: () => {},
  };

  state = {
	  ccti: false,
    lastUser: '',
  };

  static async fetchData({ store, req }) {
    await store.dispatch(login());
    const appUrl = (process.env.NODE_ENV !== 'production') ? url.format({
      protocol: req.protocol,
      host: req.get('host'),
    }) : 'https://whaleshares.io';
    store.dispatch(setAppUrl(appUrl));
    const state = store.getState();
    const locale = getLocale(state);
    await Wrapper.loadLocaleData(locale);
    store.dispatch(setUsedLocale(getAvailableLocale(locale)));
  }

  static async loadLocaleData(locale) {
    const availableLocale = getAvailableLocale(locale);
    const translationsLocale = getTranslationsByLocale(locale);
    const localeDataPromise = import(`react-intl/locale-data/${availableLocale}`);
    const translationsPromise = import(`./locales/${translationsLocale}.json`);
    const [localeData, translations] = await Promise.all([localeDataPromise, translationsPromise]);
    addLocaleData(localeData);
    global.translations = translations;
  }

  constructor(props) {
    super(props);
    this.loadLocale = this.loadLocale.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined') window.addEventListener('load', this.load);
  }

  componentWillReceiveProps(nextProps) {
    const { usedLocale } = this.props;

    if (usedLocale !== getAvailableLocale(nextProps.locale) && nextProps.loaded) {
      this.loadLocale(nextProps.locale);
    } else if (nextProps.locale !== this.props.locale) {
      this.loadLocale(nextProps.locale);
    }

  }

  load = () => {
    const { loaded, locale, usedLocale } = this.props;

    this.props.login();  /*.then(() => {
      this.props.getFollowing();
      this.props.fetchFriends();
      this.props.fetchPendingReceivedRequests();
      this.props.getMuted();
      this.props.getNotifications();
      this.props.getUserPods();
    });*/

    if (usedLocale !== getAvailableLocale(locale) && loaded) {
      this.loadLocale(locale);
    }

    busyAPI.subscribe(this.props.busyAPIHandler);
  };

  async loadLocale(locale) {
    await Wrapper.loadLocaleData(locale);
    this.props.setUsedLocale(getAvailableLocale(locale));
  }

  handleMenuItemClick(key) {
    switch (key) {
      case 'logout':
        this.props.logout();
        break;
      case 'witnesses':
        if (typeof window !== 'undefined') {
          window.location = `${WALLET_URL}/#/witnesses`;
        }
        break;
      case 'replies':
        this.props.history.push('/replies');
        break;
      case 'comments':
        this.props.history.push(`/comments`);
        break;
      case 'editor':
        this.props.history.push('/editor');
        break;
      case 'bookmarks':
        this.props.history.push('/bookmarks');
        break;
      case 'drafts':
        this.props.history.push('/drafts');
        break;
      case 'settings':
        if (typeof window !== 'undefined') {
          window.location = `${WALLET_URL}/#/profile_settings`;
        }
        break;
      case 'wallet':
        if (typeof window !== 'undefined') {
          window.location = WALLET_URL;
        }
        break;
      case 'feed':
        this.props.history.push('/myfeed');
        break;
      case 'news':
        this.props.history.push('/created');
        break;
      case 'status':
        this.props.history.push('/status');
        break;
      case 'tips_received':
        this.props.history.push('/tips/received');
        break;
      case 'tips_sent':
        this.props.history.push('/tips/sent');
        break;
      case 'friend_send_request':
        this.props.history.push('/friend_send_request');
        break;
      case 'friend_received_request':
        this.props.history.push('/friend_received_request');
        break;
      case 'my_friends':
        this.props.history.push('/my_friends');
        break;
      case 'pod_list':
        this.props.history.push('/pod_list');
        break;
      case 'discover_people':
        this.props.history.push('/discover_people');
        break;
      case 'my-profile':
        this.props.history.push(`/@${this.props.username}`);
        break;
      case 'whaletokens':
		    this.setState({ ccti: false });
        this.props.openWhaletoken();
        break;
      case 'ccti':
		    this.setState({ ccti: true });
        this.props.openWhaletoken();
        break;
      default:
        break;
    }
  }

  render() {
    const { user, usedLocale, locale, loaded } = this.props;

    // force url redirect if not all lowercase
    const case_regex = /[a-z/]*[A-Z]+[a-z/]*/;
    if (this.props.location.pathname.match(case_regex)) {
      return (
       <Redirect to={`${this.props.location.pathname.toLowerCase()}`} />
      );
    }

    // helps solve missing/stale data issues of logout/login new user
    if (this.props.username != '' && this.state.lastUser != this.props.username) {
      this.setState({ lastUser: this.props.username });
      this.props.getFollowing();
      this.props.fetchFriends();
      this.props.fetchPendingReceivedRequests();
      this.props.getMuted();
      this.props.getNotifications();
      this.props.getUserPods();
    }

    // https://gitlab.com/beyondbitcoin/busy/issues/17
    const regex = /^\/(myfeed|bookmarks|drafts|replies|comments|activity|tags|rewards|editor|settings|notifications)/gi;
    if (this.props.location.pathname.match(regex)) {
      if (!process.env.IS_BROWSER || (process.env.IS_BROWSER && !loaded)) {
        return (
          <div className="main-panel">
            <Loading style={{ paddingTop: '22px' }} />
          </div>
        );
      }
    }

    return (
      <IntlProvider key={usedLocale} locale={usedLocale} messages={global.translations}>
        <ConfigProvider locale={enUS}>
          <Layout data-dir={getLocaleDirection(getAvailableLocale(locale))}>
            <Layout.Header style={{ position: 'fixed', width: '100vw', zIndex: 1050 }}>
              {this.props.location.pathname === '/' ? (
                <TopnavLanding username={user.name} onMenuItemClick={this.handleMenuItemClick} />
              ) : (
                <Topnav username={user.name} onMenuItemClick={this.handleMenuItemClick} />
              )}
            </Layout.Header>
            <div className="content">
              {renderRoutes(this.props.route.routes)}
              {/* <Redirect /> */}
              {/* <Transfer /> */}
              <NotificationPopup />
              <LoginModal />
              <WhaleTokenModal ccti={this.state.ccti} />
            </div>
          </Layout>
        </ConfigProvider>
      </IntlProvider>
    );
  }
}

export default Wrapper;
