import isArray from "lodash/isArray";
import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Tag, Descriptions, Divider } from "antd";
import { FormattedMessage, FormattedRelative } from 'react-intl';
import PodAvatar from '../pod/PodAvatar';
import { jsonParse } from '../helpers/formatter';
import { createPodJoinLink } from '../utils/wlsuri';
import { WALLET_URL, WLS_IMG_PROXY } from '../../common/constants/settings';
import './PodCard.less';

const PodCard = (props) => {
  /**
   * using pod json_metadata and fallback to json_metadata_creator
   */
  const podMetaData = jsonParse(props.json_metadata) || {};
  const { profile = {} } = jsonParse(props.json_metadata_creator) || {};
  const title = podMetaData.name || profile.name || props.name || '';
  const about = podMetaData.about || profile.about || '';

  const cover_image = !!podMetaData.cover_image
    ? `${WLS_IMG_PROXY}/1024x256/${podMetaData.cover_image}`
    : (profile.cover_image
      ? `${WLS_IMG_PROXY}/1024x256/${profile.cover_image}`
      : `/images/user-header.jpg`);

  let tags = [];
  if (podMetaData.tags && isArray(podMetaData.tags)) {
    try {
      tags = podMetaData.tags.map(topic => (
        <Link key={topic} to={`/discover_people/${topic}`}>
          <Tag>{topic}</Tag>
        </Link>
      ));
    } catch(e) {
      // do nothing
    }
  } else if (profile.tags && isArray(profile.tags)) {
    try {
      tags = profile.tags(topic => (
        <Link key={topic} to={`/discover_people/${topic}`}>
          <Tag>{topic}</Tag>
        </Link>
      ));
    } catch(e) {
      // do nothing
    }
  }

  return (
    <Card className="PodCard"
      // title={props.name}
      cover={<img alt="cover" src={cover_image}/>}
    >
      <div style={{float: 'right'}}>
        <a className='ant-btn ant-btn-primary ant-btn-sm'
           target="_blank" rel="noopener noreferrer" href={`${WALLET_URL}${createPodJoinLink(props.name)}`}>
          <FormattedMessage id="join" defaultMessage="Join"/>
        </a>
      </div>

      <Card.Meta
        avatar={<PodAvatar podname={props.name} size={40}/>}
        title={<Link to={`/@${props.name}`}>{title}</Link>}
        description={about}
      />
      {tags.length > 0 && (<div className='interests'>Focuses on: {tags}</div>)}
      <Divider dashed />
      <Descriptions size="small" layout="vertical"
                    style={{fontSize: '14px'}}
      >
        <Descriptions.Item label="Members">{props.member_count}</Descriptions.Item>
        <Descriptions.Item label="Created"><FormattedRelative value={`${props.created}Z`} /></Descriptions.Item>
        <Descriptions.Item label="Fee">{props.fee}</Descriptions.Item>
      </Descriptions>
    </Card>
  );
};

export default PodCard;
