import React from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-image-lightbox';
import { Icon } from "antd";
import faUsers from '../../../assets/images/svgs/faUsers.svg';
import Avatar from './Avatar';
import BTooltip from './BTooltip';

export default class AvatarLightbox extends React.Component {
  static propTypes = {
    username: PropTypes.string,
    size: PropTypes.number,
    isPod: PropTypes.bool,
  };

  static defaultProps = {
    username: undefined,
    size: 100,
    isPod: false,
  };

  state = {
    open: false,
  };

  handleAvatarClick = () => this.setState({ open: true });

  handleCloseRequest = () => this.setState({ open: false });

  render() {
    const { username, size, isPod } = this.props;

    return (
      <div className="AvatarLightbox">
        <a role="presentation" onClick={this.handleAvatarClick}>
          <Avatar username={username} size={size} />
          {isPod && (
            <BTooltip title="Pod Account">
              <Icon component={faUsers}/>
            </BTooltip>
          )}
        </a>
        {this.state.open && (
          <Lightbox
            mainSrc={`https://imgp.whaleshares.io/profileimage/${username}/128x128`}
            onCloseRequest={this.handleCloseRequest}
          />
        )}
      </div>
    );
  }
}
