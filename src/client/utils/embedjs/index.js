const getUrls = require('get-urls');

let SteemEmbed = {
  getUrls: (text) => {
    let urls = [];
    try {
      urls = getUrls(text);
    } catch (e) {
      console.log(e);
    }
    return urls;
  },
  getAll: (text, options) => {
    let embeds = [];

    if (!options) options = {};
    options.width = options.width || '100%';
    options.height = options.height || '400';
    options.autoplay = 'autoplay' in options ? options.autoplay : true;

    const urls = getUrls(text);
    urls.forEach((url) => {
      const embed = SteemEmbed.get(url, options);
      if (embed) {
        embeds.push(SteemEmbed.get(url, options));
      }
    });
    return embeds;
  },
  get: (url, options) => {
    let youtubeId = SteemEmbed.isYoutube(url);
    let twitchChannel = SteemEmbed.isTwitch(url);
    let periscopeId = SteemEmbed.isPeriscope(url);
    let soundcloudId = SteemEmbed.isSoundcloud(url);
    let vimeoId = SteemEmbed.isVimeo(url);
    let bitchuteId = SteemEmbed.isBitchute(url);
    let tiktokId = SteemEmbed.isTiktok(url);
    let musicoinId = SteemEmbed.isMusicoin(url);
    let instagramId = SteemEmbed.isInstagram(url);
    let rumbleId = SteemEmbed.isRumble(url);
    let dtubeId = SteemEmbed.isDtube(url);
    let brighteonId = SteemEmbed.isBrighteon(url);
    let lbryId = SteemEmbed.isLbry(url);

    // periscope shutting down spring 2021
    // musicoin doesn't seem to work anymore

    if (youtubeId) {
      return {
        'type': 'video',
        'url': url,
        'provider_name': 'YouTube',
        'thumbnail': 'https://img.youtube.com/vi/' + youtubeId + '/mqdefault.jpg',
        'id': youtubeId,
        'embed': SteemEmbed.youtube(url, youtubeId, options)
      }
    } else if (twitchChannel) {
      return {
        'type': 'video',
        'url': url,
        'provider_name': 'Twitch',
        'id': twitchChannel,
        'embed': SteemEmbed.twitch(url, twitchChannel, options)
      }
    } else if (periscopeId) {
      return {
        'type': 'video',
        'url': url,
        'provider_name': 'Periscope',
        'id': periscopeId,
        'embed': SteemEmbed.periscope(url, periscopeId, options)
      }
    } else if (soundcloudId) {
      return {
        'type': 'music',
        'url': url,
        'provider_name': 'SoundCloud',
        'id': soundcloudId,
        'embed': SteemEmbed.soundcloud(url, soundcloudId, options)
      }
    } else if (vimeoId) {
      return {
        'type': 'video',
        'url': url,
        'provider_name': 'Vimeo',
        'id': vimeoId,
        'embed': SteemEmbed.vimeo(url, vimeoId, options)
      }
    } else if (bitchuteId) {
      return {
        type: 'video',
        url: url,
        provider_name: 'BitChute',
        id: bitchuteId,
        embed: SteemEmbed.bitchute(url, bitchuteId, options)
      }
    } else if (tiktokId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'TikTok',
        id: tiktokId,
        embed: SteemEmbed.tiktok(url, tiktokId, options)
       }
    } else if (musicoinId) {
       return {
        type: 'music',
        url: url,
        provider_name: 'Musicoin',
        id: musicoinId,
        embed: SteemEmbed.musicoin(url, musicoinId, options)
       }
    } else if (instagramId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'Instagram',
        id: instagramId,
        embed: SteemEmbed.instagram(url, instagramId, options)
       }
    } else if (rumbleId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'Rumble',
        id: rumbleId,
        embed: SteemEmbed.rumble(url, rumbleId, options)
       }
    } else if (dtubeId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'Dtube',
        id: dtubeId,
        embed: SteemEmbed.dtube(url, dtubeId, options)
       }
    } else if (brighteonId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'Brighteon',
        id: brighteonId,
        embed: SteemEmbed.brighteon(url, brighteonId, options)
       } 
    } else if (lbryId) {
       return {
        type: 'video',
        url: url,
        provider_name: 'Lbry',
        id: lbryId,
        embed: SteemEmbed.lbry(url, lbryId, options)
       }
    }

    return null;
  },
  isYoutube: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
  },
  youtube: (url, id, options) => {
    const timerMatches = url.match(/[?&]t=([0-9]+h)*([0-9]+m)*([0-9]+s)+/);
    const autoplayValue = options.autoplay ? 1 : 0;
    let srcUrl = '//www.youtube.com/embed/' + id + '?autoplay=' + autoplayValue;
    if (timerMatches && timerMatches[3]) {
      srcUrl += '&start=' + ((parseInt(timerMatches[1], 10) || 0) * 3600 + (parseInt(timerMatches[2]) || 0) * 60 + (parseInt(timerMatches[3]) || 0));
    }
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="' + srcUrl + '" frameborder="0" scrolling="no" allowfullscreen></iframe>';
  },
  isTwitch: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:twitch.tv\/)(.*)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
  },
  twitch: (url, channel, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="//player.twitch.tv/?channel=' + channel + '&parent=' + window.location.host + '&autoplay=false" frameborder="0" scrolling="no" allowfullscreen></iframe>';
  },
  isPeriscope: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:periscope.tv\/)(.*)?$/;
    const m = (url.match(p)) ? RegExp.$1.split('/') : [];
    const r = (m[1]) ? m[1] : false;
    return r;
  },
  periscope: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="//www.periscope.tv/w/' + id + '" frameborder="0" scrolling="no" allowfullscreen></iframe>';
  },
  isSoundcloud: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:soundcloud.com\/)(.*)?$/;
    return (url.match(p)) ? "url" : false;
  },
  soundcloud: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="//w.soundcloud.com/player/?url=' + encodeURIComponent(url + '?visual=true') + '" frameborder="0" scrolling="no" allowfullscreen></iframe>';
  },
  isVimeo: (url) => {
    let p = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/;
    return (url.match(p)) ? RegExp.$3 : false;
  },
  vimeo: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://player.vimeo.com/video/' + id + '" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
  },
  isBitchute: (url) => {
    let p = /^(?:https?:\/\/)?(?:www\.)?(?:bitchute\.com\/video\/)(\w+)\/?$/;
    return url.match(p) ? RegExp.$1 : false;
  },
  bitchute: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://www.bitchute.com/embed/' + id + '" frameborder="0" scrolling="no" allowfullscreen></iframe>';
  },
  isTiktok: (url) => {
    let p = /^(?:https?:\/\/)?(?:www\.)?(?:tiktok\.com\/embed\/)(\w+)\/?$/;
    return url.match(p) ? RegExp.$1 : false;
  },
  tiktok: (url, id, options) => {
    options.width = '340';
    options.height = '700';
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://www.tiktok.com/embed/' + id + '" frameborder="0" scrolling="no" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
  },
  isMusicoin: (url) => {
    let p = /^(?:https?:\/\/)?(?:www\.)?(?:musicoin\.org\/embedded-player\/)(\w+)\/?$/;
    return url.match(p) ? RegExp.$1 : false;
  },
  musicoin: (url, id, options) => {
    options.width = '500';
    options.height = '120';
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://musicoin.org/embedded-player/' + id + '?embedded=true&autoQueue=true&preview=true" frameborder="0" scrolling="no"></iframe>';
  },
  isInstagram: (url) => {
    let p = /^(?:https?:\/\/)?(?:www\.)?(?:instagram\.com\/p\/)(.*)?$/;
    return url.match(p) ? RegExp.$1.replace(/\/$/,'') : false;
  },
  instagram: (url, id, options) => {
    options.width = '640';
    options.height = '960';
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://www.instagram.com/p/' + id + '/embed/captioned" frameborder="0" scrolling="no"></iframe>';
  },
  isRumble: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:rumble.com\/embed\/)(.*)?$/;
    return (url.match(p)) ? RegExp.$1.replace(/\?.+$/, '').replace(/\/$/,'') : false;
  },
  rumble: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://rumble.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>';
  },
  isDtube: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:emb.d.tube\/#!\/)(.*)?$/;
    return (url.match(p)) ? btoa(RegExp.$1.replace(/\?.+$/, '').replace(/\/$/,'')).replace(/=/g,'_') : false;
  },
  dtube: (url, id, options) => {
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://emb.d.tube/#!/' + atob(id.replace(/_/g,'=')) + '" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
  },
  isBrighteon: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:brighteon.com\/)(.*)?$/;
    return (url.match(p)) ? RegExp.$1.replace(/\?.+$/, '').replace(/\/$/,'') : false;
  },
  brighteon: (url, id, options) => {
    options.width = '560';
    options.height = '315';
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://www.brighteon.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>';
  },
  isLbry: (url) => {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:lbry.tv\/\$\/embed\/)(.*)?$/;
    return (url.match(p)) ? btoa(RegExp.$1.replace(/\?.+$/, '').replace(/\/$/,'')).replace(/=/g,'_') : false;
  },
  lbry: (url, id, options) => {
    options.width = '560';
    options.height = '315';
    return '<iframe width="' + options.width + '" height="' + options.height + '" src="https://lbry.tv/$/embed/' + atob(id.replace(/_/g,'=')) + '" allowfullscreen></iframe>';
  }
};

export default SteemEmbed;
