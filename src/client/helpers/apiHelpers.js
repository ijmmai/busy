// import { intersection } from 'lodash';
// import SteemAPI from '../steemAPI';
import busyAPI from '../busyAPI';
import { jsonParse } from '../helpers/formatter';
import * as accountHistoryConstants from '../../common/constants/accountHistory';
import { fetchOffChainContents, fetchStateOffChainContents } from "../CncApi";
import _ from 'lodash';
import DMCA from '../../common/constants/dmca.json';

/** *
 * Get the path from URL and the API object of steem and return the correct API call based on path
 * @param path - as in URL like 'trending'
 * @param API - the { api } from steem npm package
 * @param query {Object} - the same query sending to Steem API
 * @param steemAPI - The same giving to Steem API
 * @returns {function}
 */
export function getDiscussionsFromAPI(sortBy, query) {
  switch (sortBy) {
    case 'feed':
    case 'podfeed':
    // case 'hot':
    case 'created':
    case 'active':
    // case 'trending':
    case 'blog':
    case 'comments':
    // case 'promoted':
      return busyAPI.sendAsync('database_api', `get_discussions_by_${sortBy}`, [query])
        .then (result => fetchOffChainContents(result));
    case 'news':
      return busyAPI.sendAsync('database_api', `get_discussions_by_blog`, [query])
        .then (result => fetchOffChainContents(result));
    default:
      throw new Error('There is not API endpoint defined for this sorting');
  }
}

export const getAccount = username => {
  let user = null;
  return busyAPI.sendAsync('database_api', 'get_accounts', [[username]]).then(result => {
    if (result.length) {
      user = result[0];
      return user;
    } else throw new Error('User Not Found');
  }).then(result => {
      if (result.is_pod) return getPod(username); else return null;
  }).then(result => {
    if (result) user.json_metadata = {...result.json_metadata, ...jsonParse(user.json_metadata)}; else
      user.json_metadata = jsonParse(user.json_metadata);
    return user;
  });
}

export const getPod = username =>
  busyAPI.sendAsync('database_api', 'get_pods', [[username]]).then(result => {
    if (result.length) {
      const pod = result[0];
      pod.json_metadata = jsonParse(result[0].json_metadata);
      return pod;
    }
    return null;
  });

export const getFollowingCount = username => busyAPI.sendAsync('follow_api', 'get_follow_count', [username]);

export const getFollowing = (username, startForm = '', type = 'blog', limit = 100) =>
  busyAPI
    .sendAsync('follow_api', 'get_following', [username, startForm, type, limit])
    .then(result => result.map(user => user.following));

export const getFollowers = (username, startForm = '', type = 'blog', limit = 100) =>
  busyAPI
    .sendAsync('follow_api', 'get_followers', [username, startForm, type, limit])
    .then(result => result.map(user => user.follower));

export const getFollowingAsync = (username, startForm = '', type = 'blog', limit = 100, lastSet = []) =>
  busyAPI
    .sendAsync('follow_api', 'get_following', [username, startForm, type, limit])
    .then(result => [...lastSet, ...result.map(user => user.following)])
    .then(followings => {
      if (followings.length !== 0 && followings.length % 100 === 0) {
        return getFollowingAsync(username, followings[followings.length - 1], type, limit, followings);
      }
      return followings;
    });

export const getMutedAsync = (username, startForm = '', type = 'ignore', limit = 100, lastSet = []) =>
  busyAPI
    .sendAsync('follow_api', 'get_following', [username, startForm, type, limit])
    .then(result => [...lastSet, ...result.map(user => user.following)])
    .then(followings => {
      if (followings.length !== 0 && followings.length % 100 === 0) {
        return getFollowingAsync(username, followings[followings.length - 1], type, limit, followings);
      }
      const spam_arr  = _.get(DMCA, 'spammers', []);
      return _.union(followings,spam_arr);
    });

export const getFollowersAsync = (username, startForm = '', type = 'blog', limit = 100, lastSet = []) =>
  busyAPI
    .sendAsync('follow_api', 'get_followers', [username, startForm, type, limit])
    .then(result => [...lastSet, ...result.map(user => user.follower)])
    .then(followings => {
      if (followings.length !== 0 && followings.length % 100 === 0) {
        return getFollowersAsync(username, followings[followings.length - 1], type, limit, followings);
      }
      return followings;
    });

export const getAccountWithFollowingCount = username =>
  Promise.all([
    getAccount(username),
    getFollowingCount(username),
    getPod(username), /* getFollowersAsync(username), getFollowingAsync(username) */,
  ]).then(([account, following, pod /* followers, followings, */]) => ({
    ...account,
    following_count: following.following_count,
    follower_count: following.follower_count,
    pod,
    /* friends: intersection(followers, followings), */
  }));

export const getAllFollowing = username =>
  new Promise(async resolve => {
    const following = await getFollowingCount(username);
    const chunkSize = 100;
    const limitArray = Array(Math.ceil(following.following_count / chunkSize)).fill(chunkSize);
    const list = limitArray.reduce(async (currentListP, value) => {
      const currentList = await currentListP;
      const startForm = currentList[currentList.length - 1] || '';
      const followers = await getFollowing(username, startForm, 'blog', value);
      return currentList.slice(0, currentList.length - 1).concat(followers);
    }, []);
    resolve(list);
  });

export const defaultAccountLimit = 30;

export const getAccountHistory = (account, from = -1, limit = defaultAccountLimit) =>
  busyAPI.sendAsync('database_api', 'get_account_history', [account, from, limit]);

export const getDynamicGlobalProperties = () => busyAPI.sendAsync('database_api', 'get_dynamic_global_properties', []);

export const isWalletTransaction = actionType =>
  actionType === accountHistoryConstants.TRANSFER ||
  actionType === accountHistoryConstants.TRANSFER_TO_VESTING ||
  actionType === accountHistoryConstants.CLAIM_REWARD_BALANCE;

export const currentUserFollowersUser = (currentUsername, username) =>
  busyAPI.sendAsync('follow_api', 'get_following', [username, currentUsername, 'blog', 1]);
