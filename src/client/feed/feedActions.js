import { getDiscussionsFromAPI } from '../helpers/apiHelpers';
import { createAsyncActionType, getFeedFromState, getFeedLoadingFromState } from '../helpers/stateHelpers';
import { getAuthenticatedUserName, getFeed, getPosts, getBookmarks as getBookmarksSelector } from '../reducers';
import { fetchOffChainContent, fetchOffChainContents, fetchStateOffChainContents } from "../CncApi";

export const GET_FEED_CONTENT = createAsyncActionType('@feed/GET_FEED_CONTENT');
export const GET_MORE_FEED_CONTENT = createAsyncActionType('@feed/GET_MORE_FEED_CONTENT');
export const GET_USER_COMMENTS = createAsyncActionType('@feed/GET_USER_COMMENTS');
export const GET_MORE_USER_COMMENTS = createAsyncActionType('@feed/GET_MORE_USER_COMMENTS');
export const GET_REPLIES = createAsyncActionType('@user/GET_REPLIES');
export const GET_MORE_REPLIES = createAsyncActionType('@user/GET_MORE_REPLIES');
export const GET_BOOKMARKS = createAsyncActionType('@bookmarks/GET_BOOKMARKS');


export const getFeedContent = ({ sortBy = 'created', category, limit = 20, from = -1 }) => (dispatch, getState, { busyAPI }) => {
  if (sortBy === 'feed') {
    return dispatch({
      type: GET_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_user_feed', [category, from, limit])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category: category || 'all',
        limit,
      },
    });
  } else if (sortBy === 'podfeed') {
    return dispatch({
      type: GET_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_pod_feed', [category, from, limit])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  } else if (sortBy === 'posts') {
    return dispatch({
      type: GET_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_blog_feed', [ category, from, limit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  } else if (sortBy === 'shares') {
    return dispatch({
      type: GET_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_shares_feed', [ category, from, limit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  } else if (sortBy === 'news') {
    return dispatch({
      type: GET_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_pod_announcements', [ category, from, limit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  }

  return dispatch({
    type: GET_FEED_CONTENT.ACTION,
    payload: getDiscussionsFromAPI(sortBy, { tag: category, limit, from }),
    meta: {
      sortBy,
      category: category || 'all',
      limit,
    },
  });
};

export const getMoreFeedContent = ({ sortBy, category, limit = 20 }) => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const feed = getFeed(state);
  const posts = getPosts(state);
  const feedContent = getFeedFromState(sortBy, category, feed);

  if (!feedContent.length) return Promise.resolve(null);

  if (sortBy === 'feed') {
    let from = feed.feed[category].lastIndex - 1;
    if (from < 0) from = 0;
    const newLimit = from >= limit ? limit : from;

    return dispatch({
      type: GET_MORE_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_user_feed', [category, from, newLimit])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category: category || 'all',
        limit,
      },
    });
  } else if (sortBy === 'posts') {
    let from = feed.posts[category].lastIndex - 1;
    if (from < 0) from = 0;
    const newLimit = from >= limit ? limit : from;

    return dispatch({
      type: GET_MORE_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_blog_feed', [ category, from, newLimit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  } else if (sortBy === 'shares') {
    let from = feed.shares[category].lastIndex - 1;
    if (from < 0) from = 0;
    const newLimit = from >= limit ? limit : from;

    return dispatch({
      type: GET_MORE_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_shares_feed', [ category, from, newLimit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  } else if (sortBy === 'news') {
    let from = feed.news[category].lastIndex - 1;
    if (from < 0) from = 0;
    const newLimit = from >= limit ? limit : from;

    return dispatch({
      type: GET_MORE_FEED_CONTENT.ACTION,
      payload: busyAPI.sendAsync('database_api', 'get_pod_announcements', [ category, from, newLimit ])
        .then (result => fetchOffChainContents(result)),
      meta: {
        sortBy,
        category,
        limit,
      },
    });
  }

  const lastPost = posts[feedContent[feedContent.length - 1]];

  const startAuthor = lastPost.author;
  const startPermlink = lastPost.permlink;

  return dispatch({
    type: GET_MORE_FEED_CONTENT.ACTION,
    payload: getDiscussionsFromAPI(sortBy, {
      tag: category,
      limit: limit + 1,
      start_author: startAuthor,
      start_permlink: startPermlink,
    }).then(postsData => postsData.slice(1)),
    meta: {
      sortBy,
      category: category || 'all',
      limit,
    },
  });
};

export const getUserComments = ({ username, limit = 20 }) => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const feed = getFeed(state);

  if (feed.comments[username] && feed.comments[username].isLoaded) {
    return null;
  }

  return dispatch({
    type: GET_USER_COMMENTS.ACTION,
    payload: busyAPI
      .sendAsync('database_api', 'get_discussions_by_comments', [{ start_author: username, limit }])
      .then (result => fetchOffChainContents(result))
      .then(postsData => postsData),
    meta: { sortBy: 'comments', category: username, limit },
  });
};

export const getMoreUserComments = ({ username, limit = 20 }) => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const feed = getFeed(state);
  const posts = getPosts(state);

  const feedContent = getFeedFromState('comments', username, feed);
  const isLoading = getFeedLoadingFromState('comments', username, feed);

  if (!feedContent.length || isLoading) {
    return null;
  }

  const lastPost = posts[feedContent[feedContent.length - 1]];

  const startAuthor = lastPost.author;
  const startPermlink = lastPost.permlink;

  return dispatch({
    type: GET_MORE_USER_COMMENTS.ACTION,
    payload: busyAPI
      .sendAsync('database_api', 'get_discussions_by_comments', [
        {
          start_author: startAuthor,
          start_permlink: startPermlink,
          limit: limit + 1,
        },
      ])
      .then(postsData => postsData.slice(1))
      .then(result => fetchOffChainContents(result)),
    meta: {sortBy: 'comments', category: username, limit},
  });
};

export const getReplies = () => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const category = getAuthenticatedUserName(state);

  dispatch({
    type: GET_REPLIES.ACTION,
    payload: busyAPI
      .sendAsync('database_api', 'get_state', [`/@${category}/recent-replies`])
      .then (result => fetchStateOffChainContents(result))
      .then(apiRes => Object.values(apiRes.content).sort((a, b) => b.id - a.id)),
    meta: { sortBy: 'replies', category, limit: 50 },
  });
};

export const getMoreReplies = () => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const feed = getFeed(state);
  const posts = getPosts(state);
  const category = getAuthenticatedUserName(state);

  const lastFetchedReplyId =
    feed.replies[category] && feed.replies[category].list[feed.replies[category].list.length - 1];

  if (!lastFetchedReplyId) {
    return null;
  }

  const startAuthor = posts.list[lastFetchedReplyId].author;
  const startPermlink = posts.list[lastFetchedReplyId].permlink;
  const limit = 10;

  return dispatch({
    type: GET_MORE_REPLIES.ACTION,
    payload: busyAPI
      .sendAsync('database_api', 'get_replies_by_last_update', [startAuthor, startPermlink, limit + 1])
      .then(postsData => postsData.slice(1))
      .then (result => fetchOffChainContents(result)),
    meta: { sortBy: 'replies', category, limit },
  });
};

/**
 * Use async await to load all the posts of bookmarked from steemAPI and returns a Promise
 *
 * @param bookmarks from localStorage only contain author and permlink
 * @param busyAPI
 * @returns Promise - bookmarksData
 */
async function getBookmarksData(bookmarks, busyAPI) {
  const bookmarksData = [];
  for (let idx = 0; idx < Object.keys(bookmarks).length; idx += 1) {
    const postId = Object.keys(bookmarks)[idx];

    const postData = busyAPI.sendAsync('database_api', 'get_content', [
      bookmarks[postId].author, bookmarks[postId].permlink])
      .then(data => fetchOffChainContent(data));
    bookmarksData.push(postData);
  }
  return Promise.all(bookmarksData.sort((a, b) => a.timestamp - b.timestamp).reverse());
}

export const getBookmarks = () => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const bookmarks = getBookmarksSelector(state);

  dispatch({
    type: GET_BOOKMARKS.ACTION,
    payload: getBookmarksData(bookmarks, busyAPI),
    meta: {
      sortBy: 'bookmarks',
      category: 'all',
      once: true,
    },
  });
};
