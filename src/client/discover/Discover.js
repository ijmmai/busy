import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Select, Input } from "antd";
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import ReduxInfiniteScroll from "../vendor/ReduxInfiniteScroll";
import DiscoverUser from "./DiscoverUser";
import Loading from "../components/Icon/Loading";
import { SET } from "./reducer";
import { FIXED_TAGS } from "../../common/constants/settings";
import './Discover.less';

const { Option } = Select;

class Discover extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
  };

  componentDidMount() {
    const {loadingMore} = this.props;
    if (loadingMore === false) {
      const paramInterest = this.props.match.params.interest;
      if (paramInterest) {
        this.props.changeTag(paramInterest);
      } else {
        this.props.fetchUsers();
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const {match} = nextProps;
    if (match.url !== this.props.match.url) {
      if (window) window.scrollTo(0, 0);

      const paramInterest = nextProps.match.params.interest;
      if (paramInterest) {
        this.props.changeTag(paramInterest);
      } else {
        this.props.fetchUsers();
      }
    }
  }

  render() {
    const {intl, users, hasMore, loadingMore, loadMore, tag} = this.props;
    const paramInterest = tag || "";

    return (
      <div className="shifted">
        <Helmet>
          <title>
            {intl.formatMessage({id: 'discover_more_people', defaultMessage: 'discover_more_people'})}
          </title>
        </Helmet>
        <div className="feed-layout container">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain/>
            </div>
          </Affix>
          <div className="Discover">
            <div className="Discover__title">
              <h1>
                <FormattedMessage id="discover_more_people" defaultMessage="Discover more people"/>
              </h1>
              <FormattedMessage
                id="discover_more_people_info"
                defaultMessage="Discover new friends, by name or interest"
              />
              <div>
                  <Input.Search
                    name          = "start"
                    placeholder   = "Names starting with..."
                    maxLength     = {16}
                    onSearch      = {value => this.props.changeStart(value)}
                    style         = {{width: '250px', marginRight:".5rem"}} 
                    enterButton
                  />
                <Select 
                  showSearch 
                  defaultValue      = "" 
                  value             = {paramInterest} 
                  style             = {{width: '150px'}} 
                  onChange          = {value => this.props.changeTag(value)}
                  optionFilterProp  = 'children'
                  filterOption      = { (input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                >
                  <Option value="">All Interests</Option>
                  {FIXED_TAGS.map(topic => (
                    <Option key={topic} value={topic}>{topic}</Option>
                  ))}
                </Select>
              </div>
            </div>
            <div className="Discover__content">
              {users && (
                <ReduxInfiniteScroll
                  hasMore={hasMore}
                  loadingMore={loadingMore}
                  loader={<Loading/>}
                  loadMore={loadMore}
                  elementIsScrollable={false}
                >
                  {users.map(user => (
                    <DiscoverUser key={user.name} user={user}/>
                  ))}
                </ReduxInfiniteScroll>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state.discoverUsers,
    ...ownProps,
  };
};

const mapDispatchToProps = {
  changeStart: (start) => async (dispatch) => {
    dispatch({ type: SET, payload: { key: 'start', value: start } });
    dispatch(mapDispatchToProps.fetchUsers());
  },
  changeTag: (tag) => async (dispatch) => {
    dispatch({ type: SET, payload: { key: 'tag', value: tag } });
    dispatch(mapDispatchToProps.fetchUsers());
  },
  fetchUsers: () => async (dispatch, getState, {busyAPI}) => {
    try {
      dispatch({type: SET, payload: {key: 'loadingMore', value: true}});

      const {tag, start} = _.get(getState(), 'discoverUsers');
      const data = await busyAPI.sendAsync('database_api', 'get_accounts_by_tag', [tag, start, 20]);
      dispatch({type: SET, payload: {key: 'users', value: data}});
      const hasMore = data.length >= 20;
      dispatch({type: SET, payload: {key: 'hasMore', value: hasMore}});
    } catch (e) {
      // do nothing
    } finally {
      dispatch({type: SET, payload: {key: 'loadingMore', value: false}});
    }
  },
  loadMore: () => async (dispatch, getState, {busyAPI}) => {
    try {
      dispatch({type: SET, payload: {key: 'loadingMore', value: true}});

      const discoverUsersState = _.get(getState(), 'discoverUsers');
      let {tag, users} = discoverUsersState;

      const start = users[users.length - 1].name;
      const data = await busyAPI.sendAsync('database_api', 'get_accounts_by_tag', [tag, start, 20]);

      const hasMore = data.length >= 20;
      dispatch({type: SET, payload: {key: 'hasMore', value: hasMore}});
      data.shift(); // remove the first item of an array to avoid duplication
      users = users.concat(data);

      dispatch({type: SET, payload: {key: 'users', value: users}});
    } catch (e) {
      // do nothing
    } finally {
      dispatch({type: SET, payload: {key: 'loadingMore', value: false}});
    }
  },
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(injectIntl(Discover));
