import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import { getIsAuthenticated, getAuthenticatedUserName, getFeed } from '../reducers';
import {
  getFeedFromState,
  getFeedLoadingFromState,
  getFeedHasMoreFromState,
} from '../helpers/stateHelpers';
import { getUserComments, getMoreUserComments } from '../feed/feedActions';
import Feed from '../feed/Feed';
import Loading from '../components/Icon/Loading';
import Affix from '../components/Utils/Affix';
import SidenavMain from '../components/Navigation/SidenavMain';
import RightSidebar from '../app/Sidebar/RightSidebar';
import requiresLogin from '../auth/requiresLogin';

class MyComments extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    authenticated: PropTypes.bool.isRequired,
    username: PropTypes.string,
    feed: PropTypes.shape(),
    getComments: PropTypes.func,
    getMoreComments: PropTypes.func,
  };

  static defaultProps = {
    authenticated: false,
    username: '',
    feed: {},
    posts: {},
    getComments: () => {},
    getMoreComments: () => {},
  };

  componentDidMount() {
    const { username, feed, authenticated } = this.props;
    const content = getFeedFromState('comments', username, feed);
    if (authenticated && _.isEmpty(content)) {
      this.props.getComments();
    }
  }

  componentWillUpdate(nextProps) {
    if (nextProps.authenticated && !this.props.authenticated) {
      nextProps.getComments();
    }
  }

  render() {
    const { intl, authenticated, username, feed } = this.props;

    if (!authenticated) return <Loading />;

    const content = getFeedFromState('comments', username, feed);
    const fetching = getFeedLoadingFromState('comments', username, feed);
    const hasMore = getFeedHasMoreFromState('comments', username, feed);

    return (
      <div className="shifted">
        <Helmet>
          <title>{intl.formatMessage({ id: 'comments', defaultMessage: 'Comments' })}</title>
        </Helmet>
        <div className="layout-twocol container">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body">
            <div className="right-sidebar-layout">
              <RightSidebar />
              <div className="main-feed">
                <Feed
                  content={content}
                  isFetching={fetching}
                  hasMore={hasMore}
                  loadMoreContent={this.props.getMoreComments}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authenticated: getIsAuthenticated(state),
  username: getAuthenticatedUserName(state),
  feed: getFeed(state),
});
const mapDispatchToProps = {
  getComments: () => async (dispatch, getState) => {
    const username = getAuthenticatedUserName(getState());
    dispatch(getUserComments({username}));
  },
  getMoreComments: () => async (dispatch, getState) => {
    const username = getAuthenticatedUserName(getState());
    dispatch(getMoreUserComments(username));
  },
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  requiresLogin,
  withConnect,
  injectIntl
)(MyComments);
