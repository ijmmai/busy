import React from 'react';
import PropTypes from 'prop-types';
import { find } from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withAuthActions from '../auth/withAuthActions';
import {
  getAuthenticatedUser,
  getComments,
  getCommentsList,
  getCommentsPendingVotes,
  getIsAuthenticated,
  getAuthenticatedUserName,
  getRewriteLinks,
  getMutedList,
} from '../reducers';
import CommentsList from '../components/Comments/Comments';
import * as commentsActions from './commentsActions';
import { notify } from '../app/Notification/notificationActions';
import './Comments.less';

@connect(
  state => ({
    user: getAuthenticatedUser(state),
    comments: getComments(state),
    commentsList: getCommentsList(state),
    pendingVotes: getCommentsPendingVotes(state),
    authenticated: getIsAuthenticated(state),
    username: getAuthenticatedUserName(state),
    rewriteLinks: getRewriteLinks(state),
    muted: getMutedList(state),
  }),
  dispatch =>
    bindActionCreators(
      {
        getComments: commentsActions.getComments,
        voteComment: (id, amount, memo, onSuccess) => commentsActions.likeComment(id, amount, memo, onSuccess),
        sendComment: (parentPost, body, storage, isUpdating, originalPost) =>
          commentsActions.sendComment(parentPost, body, storage, isUpdating, originalPost),
        notify,
      },
      dispatch,
    ),
)
@withAuthActions
class Comments extends React.Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    user: PropTypes.shape().isRequired,
    rewriteLinks: PropTypes.bool.isRequired,
    muted: PropTypes.arrayOf(PropTypes.string).isRequired,
    username: PropTypes.string,
    post: PropTypes.shape(),
    comments: PropTypes.shape(),
    commentsList: PropTypes.shape(),
    pendingVotes: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        percent: PropTypes.number,
      }),
    ),
    show: PropTypes.bool,
    notify: PropTypes.func,
    getComments: PropTypes.func,
    voteComment: PropTypes.func,
    sendComment: PropTypes.func,
    onActionInitiated: PropTypes.func,
  };

  static defaultProps = {
    username: undefined,
    post: {},
    comments: {},
    commentsList: {},
    pendingVotes: [],
    show: false,
    notify: () => {},
    getComments: () => {},
    voteComment: () => {},
    sendComment: () => {},
    onActionInitiated: () => {},
    muted: [],
  };

  state = {
    sortOrder: 'new',
  };

  componentDidMount() {
    if (this.props.show) {
      this.props.getComments(this.props.post.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { post, show } = this.props;

    if (nextProps.show && (nextProps.post.id !== post.id || !show)) {
      this.props.getComments(nextProps.post.id);
    }
  }

  getNestedComments = (commentsObj, commentsIdArray, nestedComments) => {
    const newNestedComments = nestedComments;
    commentsIdArray.forEach(commentId => {
      const nestedCommentArray = commentsObj.childrenById[commentId];
      if (nestedCommentArray.length) {
        newNestedComments[commentId] = nestedCommentArray.map(id => commentsObj.comments[id]);
        this.getNestedComments(commentsObj, nestedCommentArray, newNestedComments);
      }
    });
    return newNestedComments;
  };

  handleLikeClick = (id, amount, memo, onSuccess) => {
    this.props.onActionInitiated(() => this.props.voteComment(id, amount, memo, onSuccess));
  };

  handleSendComment = (parentPost, body, storage, isUpdating, originalPost) =>
    this.props.onActionInitiated(() => this.props.sendComment(parentPost, body, storage, isUpdating, originalPost));

  render() {
    const {
      user,
      post,
      comments,
      pendingVotes,
      show,
      rewriteLinks,
      muted,
    } = this.props;
    const postId = post.id;
    let fetchedCommentsList = [];

    const rootNode = comments.childrenById[postId];

    if (rootNode instanceof Array) {
      fetchedCommentsList = rootNode.map(id => comments.comments[id]).filter(comment => !muted.includes(comment.author));
    }

    let commentsChildren = {};

    if (fetchedCommentsList && fetchedCommentsList.length) {
      commentsChildren = this.getNestedComments(comments, comments.childrenById[postId], {});
    }

    return (
      fetchedCommentsList && (
        <CommentsList
          user={user}
          parentPost={post}
          comments={fetchedCommentsList}
          authenticated={this.props.authenticated}
          username={this.props.username}
          commentsChildren={commentsChildren}
          pendingVotes={pendingVotes}
          loading={comments.isFetching}
          show={show}
          notify={this.props.notify}
          rewriteLinks={rewriteLinks}
          onLikeClick={this.handleLikeClick}
          onSendComment={this.handleSendComment}
        />
      )
    );
  }
}
export default Comments;
