import { createAsyncActionType } from '../helpers/stateHelpers';
import { notify } from '../app/Notification/notificationActions';
import { truncateDecimal } from '../helpers/formatter';
import { fetchOffChainContent } from "../CncApi";

export const GET_CONTENT = createAsyncActionType('@post/GET_CONTENT');

export const LIKE_POST = '@post/LIKE_POST';
export const LIKE_POST_START = '@post/LIKE_POST_START';
export const LIKE_POST_SUCCESS = '@post/LIKE_POST_SUCCESS';
export const LIKE_POST_ERROR = '@post/LIKE_POST_ERROR';

export const getContent = (author, permlink, afterLike) => (dispatch, getState, { busyAPI }) => {
  if (!author || !permlink) return null;

  return dispatch({
    type: GET_CONTENT.ACTION,
    payload: {
      promise: busyAPI.sendAsync('database_api', 'get_content', [author, permlink])
        .then(data => fetchOffChainContent(data))
        .then(res => {
          if (res.author === '') throw new Error('There is no such post');
          return res;
        }),
    },
    meta: {
      author,
      permlink,
      afterLike,
    },
  }).catch(() => {});
};

// TODO: rename to tipPost
export const votePost = (postId, author, permlink, amount, memo, onSuccess) => async (dispatch, getState, { steemAPI, Keys, busyAPI }) => {
  const { auth, posts, users } = getState();
  if (!auth.isAuthenticated) return null;

  const { keys, hasWhaleVault } = Keys.decrypt();
  const post = posts.list[postId];
  const tipper = auth.user.name;
  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}postReward` : keys.postingKey;
  let enc_memo = memo.startsWith('#');
  if (enc_memo) {
    let author_pubkey = users.users && users.users['user-'+author] ? users.users['user-'+author].posting.key_auths[0][0] : "";
    if (author_pubkey == '') {
      let auth_result = await busyAPI.sendAsync('database_api', 'get_accounts', [[author]]);
      author_pubkey = auth_result[0].posting.key_auths[0][0];
    }
    if (hasWhaleVault) {
      let response = await steemAPI.chainLib.config.whalevault.promiseRequestEncryptMemo('wls_busy', 'wls:'+tipper, memo, 'posting', author_pubkey, 'stm', 'tip_memo');
      if (response.success) memo = response.result; else {
        dispatch(notify(response.message, 'error'));
        return;
      }
    } else {
      try {
        memo = steemAPI.chainLib.memo.encode(useKey, author_pubkey, memo);
      } catch (e) {
        dispatch(notify(e.message, 'error'));
        return;
      }
    }
  }
  const operations = [["social_action", {
    account: tipper,
    action: [6, { // social_action_comment_tip
      author: post.author,
      permlink: post.permlink,
      amount: `${truncateDecimal(amount, 3).toFixed(3)} WLS`,
      memo,
    }]
  }]];

  return dispatch({
    type: LIKE_POST,
    payload: {
      promise: steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ]).then(res => {
        if (onSuccess) onSuccess();
        // Delay to make sure you get the latest data (unknown issue with API)
        setTimeout(() => dispatch(getContent(post.author, post.permlink, true)), 1000);
        return res;
      }).catch( err => { 
        dispatch(notify(err.message, 'error'));
      }),
    },
    meta: { postId, tipper, amount },
  });
};
