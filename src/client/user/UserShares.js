import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Alert from 'antd/lib/alert';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Feed from '../feed/Feed';
import { getIsAuthenticated, getAuthenticatedUser, getFeed, getUser } from '../reducers';
import {
  getFeedLoadingFromState,
  getFeedFetchedFromState,
  getFeedHasMoreFromState,
  getFeedFromState,
} from '../helpers/stateHelpers';
import { getFeedContent, getMoreFeedContent } from '../feed/feedActions';
import { showPostModal, hidePostModal } from '../app/appActions';
import EmptyUserProfile from '../statics/EmptyUserProfile';
import EmptyUserOwnProfile from '../statics/EmptyUserOwnProfile';
import PostModal from '../post/PostModalContainer';

@withRouter
@connect(
  (state, ownProps) => ({
    authenticated: getIsAuthenticated(state),
    authenticatedUser: getAuthenticatedUser(state),
    feed: getFeed(state),
    user: getUser(state, ownProps.match.params.name),
  }),
  {
    showPostModal,
    hidePostModal,
    getFeedContent,
    getMoreFeedContent,
  },
)
class UserShares extends React.Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    authenticatedUser: PropTypes.shape().isRequired,
    feed: PropTypes.shape().isRequired,
    match: PropTypes.shape().isRequired,
    showPostModal: PropTypes.func.isRequired,
    hidePostModal: PropTypes.func.isRequired,
    showBy: PropTypes.string,
  };

  static defaultProps = {
    limit: 10,
    location: {},
    sortBy: 'shares',
  };

  componentDidMount() {
    const { feed, match, sortBy } = this.props;
    const username = match.params.name;
    const content = getFeedFromState(sortBy, username, feed);

    if (_.isEmpty(content)) this.props.getFeedContent({ sortBy, category: username });
  }

  componentWillReceiveProps(nextProps) {
    if (typeof window === 'undefined') return;
    const { feed, match, sortBy } = nextProps;
    const username = nextProps.match.params.name;
    const content = getFeedFromState(sortBy, username, feed);

    if (_.isEmpty(content) && (match.url !== this.props.match.url)) {
      if (window) window.scrollTo(0, 0);

      this.props.getFeedContent({ sortBy, category: username });
      this.props.hidePostModal();
    }
  }

  render() {
    const { authenticated, authenticatedUser, feed, match, sortBy } = this.props;
    const username = match.params.name;
    const isOwnProfile = authenticated && username === authenticatedUser.name;
    const content = getFeedFromState(sortBy, username, feed);
    const isFetching = getFeedLoadingFromState(sortBy, username, feed);
    const fetched = getFeedFetchedFromState(sortBy, username, feed);
    const hasMore = getFeedHasMoreFromState(sortBy, username, feed);
    const loadMoreContentAction = () => this.props.getMoreFeedContent({ sortBy, category: username });
    return (
      <div>
        <div className="profile">
          <Alert
            message={`A List of Public Posts, rewarded by @${username}`}
            type='info'
          />
          <br/>
          <Feed
            content={content}
            isFetching={isFetching}
            hasMore={hasMore}
            loadMoreContent={loadMoreContentAction}
            showPostModal={this.props.showPostModal}
          />
          {_.isEmpty(content) && fetched && isOwnProfile && <EmptyUserOwnProfile type="share"/>}
          {_.isEmpty(content) && fetched && !isOwnProfile && <EmptyUserProfile type="share" user={username}/>}
        </div>
        {<PostModal />}
      </div>
    );
  }
}

export default UserShares;
