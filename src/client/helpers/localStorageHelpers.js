import store from 'store';
import _ from 'lodash';

const defaults = { locale: "en-US",
                   votePercent: 2500,
                   showNSFWPosts: false,
                   rewriteLinks: false
                 };

export const getSettings = () => JSON.parse(localStorage.getItem('settings')) || defaults;
export const saveSettingsMetadata = settings => {
  localStorage.setItem('settings', JSON.stringify(settings));
  return Promise.resolve(settings);
};

export const getBookmarks = () => JSON.parse(localStorage.getItem('bookmarks')) || {};
export const toggleBookmarkMeta = (id, author, permlink) => {
  let bookmarks = getBookmarks();
  if(bookmarks && bookmarks[id])
    bookmarks = _.omit(bookmarks, id)
  else bookmarks[id] = {id, author, permlink};
  localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
  return Promise.resolve(bookmarks);
};

export const getDrafts = () => JSON.parse(localStorage.getItem('drafts')) || {};
export const addDraft = draft => {
  const drafts = getDrafts();
  drafts[draft.id] = draft.postData;
  localStorage.setItem('drafts', JSON.stringify(drafts));
  return Promise.resolve(draft.postData);
};
export const removeDraft = draftIds => {
  let drafts = getDrafts();
  draftIds.forEach(id => {
    drafts = _.omit(drafts, id);
  });
  localStorage.setItem('drafts', JSON.stringify(drafts));
  return Promise.resolve(drafts);
};

export const getFavoriteUsers = () => store.get('users') || {};
export const addFavoriteUser = username => {
  const users = store.get('users') || {};
  users[username] = {};
  store.set('users', users);
  return true;
};
export const removeFavoriteUser = username => {
  const users = store.get('users') || {};
  delete users[username];
  store.set('users', users);
  return true;
};
export const toggleFavoriteUser = username => {
  const users = store.get('users') || {};
  return _.has(users, username) ? removeFavoriteUser(username) : addFavoriteUser(username);
};

export const getFavoriteCategories = () => store.get('categories') || {};
export const addFavoriteCategory = category => {
  const categories = store.get('categories') || {};
  categories[category] = {};
  store.set('categories', categories);
  return true;
};
export const removeFavoriteCategory = category => {
  const categories = store.get('categories') || {};
  delete categories[category];
  store.set('categories', categories);
  return true;
};
export const toggleFavoriteCategory = category => {
  const categories = store.get('categories') || {};
  return _.has(categories, category)
    ? removeFavoriteCategory(category)
    : addFavoriteCategory(category);
};

export const getLocale = () => store.get('locale') || 'en';
export const setLocale = locale => {
  store.set('locale', locale);
};

/**
 * for marking last read notification item
 */
export const getNotifications = () => JSON.parse(localStorage.getItem('notifications')) || {user: 0, tm: 0};
export const setNotifications = (user, tm, read_index) => {
  const notifications = getNotifications();
  notifications.user = user;
  if (tm) notifications.tm = tm;
  if (read_index) notifications[user] = {read: read_index};
  localStorage.setItem('notifications', JSON.stringify(notifications));
  return Promise.resolve(notifications);
};

export const getMetadata = () => ({
  drafts: getDrafts(),
  bookmarks: getBookmarks(),
  settings: getSettings(),
  notifications: getNotifications(),
});
