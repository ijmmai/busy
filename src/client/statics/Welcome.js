/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withRouter, Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import './TextPage.less';

const WelcomePage = ({ intl, staticContext }) => {
  if (staticContext) {
    staticContext.status = 404; // eslint-disable-line no-param-reassign
  }
  return (
    <div className="TextPage container">
      <Helmet>
        <title>
          {intl.formatMessage({ id: 'welcome', defaultMessage: 'Welcome' })}
        </title>
      </Helmet>
      <div className="Welcome__banner">
        <img src="/images/welcome-banner.jpg" alt='Welcome Banner' />
      </div>
      <div className="TextPage__body">
        <h2>Welcome to Whaleshares!</h2>
        <div className="alinea">
          We are turning the social media revenue model on its head. The days of large companies controlling 
          the personal data and content created by its users to make huge profits selling out to advertisers 
          is coming to an end. Blockchain technology provides transparency and a new revenue model where the 
          content creators, and token holders, earn the revenue. No big controlling company present.
        </div>

        <div className="alinea">
          <h2>On Whaleshares, your level of engagement determines the level of your success</h2>
        </div>

        <h3>Coming from another blockchain platform?</h3>
        <div className="alinea">
          <ul>
            <li>Long posts, short posts, picture posts, whatever format fits your passion is fine,</li>
            <li>Reward pool distribution is based on the amount of tokens you hold, and daily claimable,</li>
            <li>Rewards (tips) from community members, on your posts and comments, are instantly available</li>
            <li>Downvoting doesn't exist, "Reward what you like, skip what you don't like" is our credo.</li>
          </ul>
        </div>

        <h3>Coming from Facebook?</h3>
        <div className="alinea">
          Posting on Whaleshares works more or less the same as on Facebook. 
          The main difference is the blockchain aspect. 
          <ul>
            <li>This means you have keys instead of a password,</li>
            <li>Your data is stored permanently,</li>
            <li>and there is a reward system in place.</li>
          </ul>
          <br/>  
          Unlike Facebook
          <ul>
            <li>There is no company controlling your account or content</li>
            <li>There is no data sharing with third parties (although blockchain content is public),</li>
            <li>You will be the only one to profit from your activities on the platform,</li>
            <li>Your engagement with other members will only benefit you.</li>
          </ul>
          <br/>  
          Earning rewards in the form of new "digital cryptocurrencies" can be a bit confusing at first. Don't hesitate
          to ask questions. We are an amazing community of friendly people willing to help guide you and show you how 
          to get started.
        </div>

        <h3>Need Help?</h3>
        <div className="alinea">
          <ul>
            <li>Check the <a href="https://whaleshares.io/@tutorials">Tutorials</a> account</li>
            <li>Join our <a href="https://discord.gg/gEAjZpz">Discord Server</a></li>
            <li>Submit your question in a post</li>
          </ul>
        </div>
        
        <div className="alinea">
          We look forward to meeting you,<br/>
          <h4>The Whaleshares Community</h4>
        </div>
      </div>
      <div>
        <Link to='/created#start'>
          Back to posts
        </Link>
      </div>
    </div>
  );
};

WelcomePage.propTypes = {
  intl: PropTypes.shape().isRequired,
  staticContext: PropTypes.shape(),
};

WelcomePage.defaultProps = {
  staticContext: null,
};

export default withRouter(injectIntl(WelcomePage));

/* eslint-enable react/no-unescaped-entities */
