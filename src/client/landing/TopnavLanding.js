import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon } from 'antd';
import faShareAlt from '../../../assets/images/svgs/faShareAlt.svg';
import faUsers from '../../../assets/images/svgs/faUsers.svg';
import faCoins from '../../../assets/images/svgs/faCoins.svg';
import { getIsAuthenticated } from '../reducers';
import './TopnavLanding.less';

@connect(state => ({
  loggedIn: getIsAuthenticated(state),
}))
class TopnavLanding extends React.Component {
  static propTypes = {
    loggedIn: PropTypes.bool.isRequired,
  };

  render() {
    const { loggedIn } = this.props;

    return (
      <div className="TopnavLanding">
        <div className="topnav-layout-landing">
          <div className="onehalf">
            <Link className="brand" to={loggedIn ? '/myfeed' : '/created#start'}>
              <img src="/images/wls-logo-beta.png" className="logo" alt="Whaleshares Logo" />
              <img src="/images/wls-mark-beta.png" className="mark" alt="Whaleshares Logo" />
            </Link>
          </div>
          <div className="otherhalf">
            <Menu className="top-menu" mode="horizontal">
              <Menu.Item key="one">
                <a href="#one">
                  <Icon component={faShareAlt}/>
                  <span>Social Sharing</span>
                </a>
              </Menu.Item>
              <Menu.Item key="two">
                <a href="#two">
                  <Icon component={faUsers}/>
                  <span>Communities</span>
                </a>
              </Menu.Item>
              <Menu.Item key="three">
                <a href="#three">
                  <Icon component={faCoins}/>
                  <span>Daily Rewards</span>
                </a>
              </Menu.Item>
            </Menu>
            <Link className="ant-btn ant-btn-background-ghost" to={loggedIn ? '/myfeed' : '/created#start'}>
              Start
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default TopnavLanding;
