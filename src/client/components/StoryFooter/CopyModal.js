import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ActionButton from '../Button/Action';

const CopyModal = ({ title, copy, visible, source, onCopy }) => (
  <Modal
    visible={visible}
    title={title}
    onCancel={onCopy}
    footer={
      <CopyToClipboard
        text={source}
        onCopy={onCopy}
      >
        <ActionButton
          text={copy}
          primary
         />
      </CopyToClipboard>
    }
  >
    <code>
      {source}
    </code>
  </Modal>
);

CopyModal.propTypes = {
  title: PropTypes.string.isRequired,
  copy: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  source: PropTypes.string.isRequired,
  onCopy: PropTypes.func.isRequired,
};

export default CopyModal;