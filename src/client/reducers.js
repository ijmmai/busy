import { combineReducers } from 'redux';

import { routerReducer } from 'react-router-redux';

import appReducer, * as fromApp from './app/appReducer';
import authReducer, * as fromAuth from './auth/authReducer';
import commentsReducer, * as fromComments from './comments/commentsReducer';
import feedReducer, * as fromFeed from './feed/feedReducer';
import postsReducer, * as fromPosts from './post/postsReducer';
import userReducer, * as fromUser from './user/userReducer';
import usersReducer, * as fromUsers from './user/usersReducer';
import notificationReducer from './app/Notification/notificationReducers';
import bookmarksReducer, * as fromBookmarks from './bookmarks/bookmarksReducer';
import favoritesReducer, * as fromFavorites from './favorites/favoritesReducer';
import editorReducer, * as fromEditor from './post/Write/editorReducer';
import walletReducer, * as fromWallet from './wallet/walletReducer';
import settingsReducer, * as fromSettings from './settings/settingsReducer';
import friendsReducer, * as fromFriends from './friends/reducer';
import podsReducer, * as fromPods from './pods/reducer';
import tipsReducer from './tips/reducer';
import discoverUsersReducer from './discover/reducer';
import { responsiveReducer } from './vendor/responsive';

const reducers = combineReducers({
  app: appReducer,
  auth: authReducer,
  comments: commentsReducer,
  editor: editorReducer,
  posts: postsReducer,
  feed: feedReducer,
  user: userReducer,
  users: usersReducer,
  responsive: responsiveReducer,
  notifications: notificationReducer,
  bookmarks: bookmarksReducer,
  favorites: favoritesReducer,
  router: routerReducer,
  wallet: walletReducer,
  settings: settingsReducer,
  friends: friendsReducer,
  pods: podsReducer,
  tips: tipsReducer,
  discoverUsers: discoverUsersReducer
});

export default reducers;

export const getIsAuthenticated = state => fromAuth.getIsAuthenticated(state.auth);
export const getIsAuthFetching = state => fromAuth.getIsAuthFetching(state.auth);
export const getIsLoaded = state => fromAuth.getIsLoaded(state.auth);
export const getIsReloading = state => fromAuth.getIsReloading(state.auth);
export const getAuthenticatedUser = state => fromAuth.getAuthenticatedUser(state.auth);
export const getAuthenticatedUserName = state => fromAuth.getAuthenticatedUserName(state.auth);
export const getAuthenticatedUserSCMetaData = state => fromAuth.getAuthenticatedUserSCMetaData(state.auth);
export const getIsLocked = state => fromAuth.getIsLocked(state.auth);
export const getShowLoginModal = state => fromAuth.getShowLoginModal(state.auth);
export const getHasPass = state => fromAuth.getHasPass(state.auth);

export const getPosts = state => fromPosts.getPosts(state.posts);
export const getPostContent = (state, author, permlink) => fromPosts.getPostContent(state.posts, author, permlink);
export const getPendingLikes = state => fromPosts.getPendingLikes(state.posts);
export const getIsPostFetching = (state, author, permlink) =>
  fromPosts.getIsPostFetching(state.posts, author, permlink);
export const getIsPostLoaded = (state, author, permlink) => fromPosts.getIsPostLoaded(state.posts, author, permlink);
export const getIsPostFailed = (state, author, permlink) => fromPosts.getIsPostFailed(state.posts, author, permlink);

export const getDraftPosts = state => fromEditor.getDraftPosts(state.editor);
export const getIsEditorLoading = state => fromEditor.getIsEditorLoading(state.editor);
export const getIsEditorSaving = state => fromEditor.getIsEditorSaving(state.editor);
export const getPendingDrafts = state => fromEditor.getPendingDrafts(state.editor);
export const getIsPostEdited = (state, permlink) => fromEditor.getIsPostEdited(state.editor, permlink);

export const getIsFetching = state => fromApp.getIsFetching(state.app);
export const getIsBannerClosed = state => fromApp.getIsBannerClosed(state.app);
export const getAppUrl = state => fromApp.getAppUrl(state.app);
export const getUsedLocale = state => fromApp.getUsedLocale(state.app);
export const getShowPostModal = state => fromApp.getShowPostModal(state.app);
export const getCurrentShownPost = state => fromApp.getCurrentShownPost(state.app);

export const getFeed = state => fromFeed.getFeed(state.feed);

export const getComments = state => fromComments.getComments(state.comments);
export const getCommentsList = state => fromComments.getCommentsList(state.comments);
export const getCommentsPendingVotes = state => fromComments.getCommentsPendingVotes(state.comments);

export const getBookmarks = state => fromBookmarks.getBookmarks(state.bookmarks);
export const getPendingBookmarks = state => fromBookmarks.getPendingBookmarks(state.bookmarks);

export const getFollowingList = state => fromUser.getFollowingList(state.user);
export const getMutedList = state => fromUser.getMutedList(state.user);
export const getPendingMutes = state => fromUser.getPendingMutes(state.user);
export const getPendingFollows = state => fromUser.getPendingFollows(state.user);
export const getIsFetchingFollowingList = state => fromUser.getIsFetchingFollowingList(state.user);
export const getRecommendations = state => fromUser.getRecommendations(state.user);
export const getFollowingFetched = state => fromUser.getFollowingFetched(state.user);
export const getNotifications = state => fromUser.getNotifications(state.user);
export const getIsLoadingNotifications = state => fromUser.getIsLoadingNotifications(state.user);
export const getIsLoadingNotificationsMore = state => fromUser.getIsLoadingNotificationsMore(state.user);
export const getIsHasMoreNotifications = state => fromUser.getIsHasMoreNotifications(state.user);
export const getFetchFollowListError = state => fromUser.getFetchFollowListError(state.user);
export const getLatestNotification = state => fromUser.getLatestNotification(state.user);

export const getUser = (state, username) => fromUsers.getUser(state.users, username);
export const getIsUserFetching = (state, username) => fromUsers.getIsUserFetching(state.users, username);
export const getIsUserLoaded = (state, username) => fromUsers.getIsUserLoaded(state.users, username);
export const getIsUserFailed = (state, username) => fromUsers.getIsUserFailed(state.users, username);

export const getFavoriteCategories = state => fromFavorites.getFavoriteCategories(state.favorites);

export const getIsTransferVisible = state => fromWallet.getIsTransferVisible(state.wallet);
export const getTransferTo = state => fromWallet.getTransferTo(state.wallet);

export const getIsSettingsLoading = state => fromSettings.getIsLoading(state.settings);
export const getLocale = state => fromSettings.getLocale(state.settings);
export const getShowNSFWPosts = state => fromSettings.getShowNSFWPosts(state.settings);
export const getRewriteLinks = state => fromSettings.getRewriteLinks(state.settings);

export const getTotalVestingShares = state => fromWallet.getTotalVestingShares(state.wallet);
export const getTotalVestingFundSteem = state => fromWallet.getTotalVestingFundSteem(state.wallet);
export const getUsersTransactions = state => fromWallet.getUsersTransactions(state.wallet);
export const getUsersAccountHistory = state => fromWallet.getUsersAccountHistory(state.wallet);
export const getUsersAccountHistoryLoading = state => fromWallet.getUsersAccountHistoryLoading(state.wallet);
export const getLoadingGlobalProperties = state => fromWallet.getLoadingGlobalProperties(state.wallet);
export const getAccountHistoryFilter = state => fromWallet.getAccountHistoryFilter(state.wallet);
export const getCurrentDisplayedActions = state => fromWallet.getCurrentDisplayedActions(state.wallet);
export const getCurrentFilteredActions = state => fromWallet.getCurrentFilteredActions(state.wallet);
export const getIsWhaletokensVisible = state => fromWallet.getIsWhaletokensVisible(state.wallet);
export const getScatter = state => fromWallet.getScatter(state.wallet);

// export const getFriends = state => fromFriends.getPosts(state.posts);

// export const getPod = state => fromAuth.getPod(state.auth);
// export const isPod = state => fromAuth.isPod(state.auth);
// export const getPods = state => fromUser.getPods(state.user);
