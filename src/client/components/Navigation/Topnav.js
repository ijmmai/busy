import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Popover, Icon, Select } from 'antd';
import classNames from 'classnames';
import BTooltip from '../BTooltip';
import Avatar from '../Avatar';
import PopoverMenu, { PopoverMenuItem } from '../PopoverMenu/PopoverMenu';
import { WALLET_URL } from '../../../common/constants/settings';
import {
  getNotifications,
  getAuthenticatedUserSCMetaData,
  getIsLoadingNotifications,
} from '../../reducers';
import faWallet from '../../../../assets/images/svgs/faWallet.svg';
import faPencilAlt from '../../../../assets/images/svgs/faPencilAlt.svg';
import faGoogle from '../../../../assets/images/svgs/faGoogle.svg';
import './Topnav.less';
import SignupButton from '../SignupButton';

const { Option, OptGroup } = Select;

@injectIntl
@withRouter
@connect(
  state => ({
    notifications: getNotifications(state),
    userSCMetaData: getAuthenticatedUserSCMetaData(state),
    loadingNotifications: getIsLoadingNotifications(state),
  }),
  {
    // getUpdatedSCUserMetadata,
  },
)
class Topnav extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    location: PropTypes.shape({ pathname: PropTypes.string }).isRequired,
    username: PropTypes.string,
    onMenuItemClick: PropTypes.func,
  };

  static defaultProps = {
    notifications: [],
    username: undefined,
    onMenuItemClick: () => {},
    userSCMetaData: {},
    loadingNotifications: false,
  };

  static handleScrollToTop() {
    if (window) {
      window.scrollTo(0, 0);
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      searchBarActive: false,
      popoverVisible: false,
      selectedOption: 'Type to search...',
      selectedOptions: [],
      searchTerm: "",
      searchStyle: {
        width:"100%",
      }
    };
    this.handleMoreMenuSelect = this.handleMoreMenuSelect.bind(this);
    this.handleMoreMenuVisibleChange = this.handleMoreMenuVisibleChange.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      const cx = '011723639557098137375:0a16gqowrtw';
      const gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = `https://cse.google.com/cse.js?cx=${cx}`;
      const s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);

      // eslint-disable-next-line no-underscore-dangle
      window.__gcse = {
        parsetags: 'explicit',
        callback: this.renderSearch,
      };
    }
  }

  handleMoreMenuSelect(key) {
    this.setState({ popoverVisible: false }, () => {
      this.props.onMenuItemClick(key);
    });
  }

  handleMoreMenuVisibleChange(visible) {
    this.setState({ popoverVisible: visible });
  }

  menuForLoggedOut = () => {
    const { searchBarActive } = this.state;

    return (
      <div
        className={classNames('Topnav__menu-container', 'logged-out', {
          'Topnav__mobile-hidden': searchBarActive,
        })}
      >
        <Menu className="Topnav__menu-container__menu menu-links" mode="horizontal">
          <Menu.Item key="signup">
            <SignupButton text="">
              <FormattedMessage id="signup" defaultMessage="Sign Up" />
            </SignupButton>
          </Menu.Item>
          <Menu.Item key="login" className="login">
            <Link to="/login">
              <FormattedMessage id="login" defaultMessage="Log in" />
            </Link>
          </Menu.Item>
          <Menu.Item key="wallet" className="last wallet">
            <a target="_blank" rel="noopener noreferrer" href={WALLET_URL}>
              <FormattedMessage id="standalone_wallet" defaultMessage="Status" />
            </a>
          </Menu.Item>
          {/* <Menu.Item key="language">
            <LanguageSettings />
          </Menu.Item> */}
        </Menu>
      </div>
    );
  };

  menuForLoggedIn = () => {
    const { intl, username, notifications, userSCMetaData, loadingNotifications } = this.props;
    const { searchBarActive, popoverVisible } = this.state;
    // const lastSeenTimestamp = _.get(userSCMetaData, 'notifications_last_timestamp');
    // const notificationsCount = _.isUndefined(lastSeenTimestamp)
    //   ? _.size(notifications)
    //   : _.size(
    //     _.filter(
    //       notifications,
    //       notification =>
    //         lastSeenTimestamp < notification.timestamp &&
    //         _.includes(PARSED_NOTIFICATIONS, notification.type),
    //     ),
    //   );
    let notificationsCount = 0;
    if (notifications.length > 0) {
      const last_index = notifications[0][0];
      const read_mark = userSCMetaData.notifications[username] && userSCMetaData.notifications[username].read || 0;
      if (last_index > read_mark) notificationsCount = last_index - read_mark; else
        if (read_mark > last_index) notificationsCount = read_mark - last_index;  // in case old notifications drop off??
    }

    const displayBadge = notificationsCount > 0;
    const notificationsCountDisplay = notificationsCount > 99 ? '99+' : notificationsCount;
    return (
      <div
        className={classNames('Topnav__menu-container', 'logged-in', {
          'Topnav__mobile-hidden': searchBarActive,
        })}
      >
        <Menu selectedKeys={[]} className="Topnav__menu-container__menu" mode="horizontal">
          <Menu.Item key="wallet" className="wallet-icon">
            <a target="_blank" rel="noopener noreferrer" href={WALLET_URL}>
              <Icon component={faWallet}/>
            </a>
          </Menu.Item>
          <Menu.Item key="write">
            <BTooltip
              placement="bottom"
              title={intl.formatMessage({ id: 'write_post', defaultMessage: 'Write post' })}
              mouseEnterDelay={1}
            >
              <Link to="/editor" className="Topnav__link Topnav__link--action">
                <Icon component={faPencilAlt}/>
              </Link>
            </BTooltip>
          </Menu.Item>
          <Menu.Item key="notifications" className="Topnav__item--badge">
            <BTooltip
              placement="bottom"
              title={intl.formatMessage({ id: 'notifications', defaultMessage: 'Notifications' })}
              overlayClassName="Topnav__notifications-tooltip"
              mouseEnterDelay={1}
            >
                <Link to='/notifications' className="Topnav__link Topnav__link--light Topnav__link--action">
                  {<i className="iconfont icon-remind" />}
                  {displayBadge && (
                    <span className="notification">{notificationsCountDisplay}</span>
                    )
                  }
                </Link>
            </BTooltip>
          </Menu.Item>
          {/* this.getMenuItemForLock() */}
          <Menu.Item key="more" className="user-menu">
            <Popover
              placement="bottom"
              trigger="click"
              visible={popoverVisible}
              onVisibleChange={this.handleMoreMenuVisibleChange}
              overlayStyle={{ position: 'fixed' }}
              content={
                <PopoverMenu onSelect={this.handleMoreMenuSelect}>
                  <PopoverMenuItem key="my-profile">
                    <FormattedMessage id="my_posts" defaultMessage="My Posts" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="feed">
                    <FormattedMessage id="feeds" defaultMessage="Feeds" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="replies">
                    <FormattedMessage id="replies" defaultMessage="Replies" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="comments">
                    <FormattedMessage id="comments" defaultMessage="Comments" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="editor">
                    <FormattedMessage id="editor" defaultMessage="Compose" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="drafts">
                    <FormattedMessage id="drafts" defaultMessage="Drafts" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="bookmarks">
                    <FormattedMessage id="bookmarks" defaultMessage="Bookmarks" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="status">
                    <FormattedMessage id="status_rewards" defaultMessage="Status/Rewards" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="tips_received">
                    Tips Received
                  </PopoverMenuItem>
                  <PopoverMenuItem key="tips_sent">
                    Tips Sent
                  </PopoverMenuItem>
                  <PopoverMenuItem key="friend_send_request">
                    Send Friend Request
                  </PopoverMenuItem>
                  <PopoverMenuItem key="friend_received_request">
                    Friend Requests
                  </PopoverMenuItem>
                  <PopoverMenuItem key="my_friends">
                    My Friends
                  </PopoverMenuItem>
                  <PopoverMenuItem key="pod_list">
                    <FormattedMessage id="podlist" defaultMessage="Discover Pods" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="discover_people">
                    Discover People
                  </PopoverMenuItem>
                  <PopoverMenuItem key="wallet">
                    <FormattedMessage id="standalone_wallet" defaultMessage="Wallet" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="settings">
                    <FormattedMessage id="settings" defaultMessage="Settings" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="witnesses">
                    <FormattedMessage id="witnesses" defaultMessage="Witnesses" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="whaletokens">
                    <FormattedMessage id="whaletokens" defaultMessage="Use WhaleTokens" />
                  </PopoverMenuItem>
                  <PopoverMenuItem key="ccti">
                    <FormattedMessage id="ccti" defaultMessage="Use CCTI"/>
                  </PopoverMenuItem>
                  <PopoverMenuItem key="logout">
                    <FormattedMessage id="logout" defaultMessage="Logout"/>
                  </PopoverMenuItem>
                </PopoverMenu>
              }
            >
              <a className="Topnav__link Topnav__link--light">
                <Avatar username={username} size={32}/>
              </a>
            </Popover>
          </Menu.Item>
        </Menu>
      </div>
    );
  };

  handleMobileSearchButtonClick = () => {
    const { searchBarActive } = this.state;
    this.setState({ searchBarActive: !searchBarActive }, () => {
      this.searchInputRef.focus();
    });
  };

  handleSearchForInput = event => {
    const value = event.target.value;
    const element = google.search.cse.element.getElement('search'); // eslint-disable-line no-undef
    element.execute(value);
  };

  renderSearch = () => {
    // eslint-disable-next-line no-undef
    google.search.cse.element.render({
      div: 'SearchArea',
      tag: 'searchresults-only',
      gname: 'search',
    });
  };

  handleSearchChange = selectedOption => {
    this.setState({
      // selectedOption,
    }, () => {
      window.location = "/@" + selectedOption
    });
  };

  onSearchKeyDown = e => {
    // catch the code of the key pressed
    const searched = e.target.value.toString() + e.key.toString();

    switch (e.keyCode) {
      case 8:
        if (e.target.value.toString().length < 3) {
          this.setState({
            searchTerm: e.target.value.toString(),
            selectedOptions: [],
          }, () => {
            // console.log("state set onBackspace");
          });
        }
        break;
      case 46:
        // console.log('delete');
        break;
      case 37:
        // console.log('left');
        break;
      case 38:
        // console.log('up');
        break;
      case 39:
        // console.log('right');
        break;
      case 40:
        // console.log('down');
        break;
      case 13:
        // console.log('Enter pressed -- searching for: ' + e.target.value);
        break;

      default:
        this.setState({
          searchTerm: searched.toString(),
        }, () => {
        });

        if (searched.length > 2) this.searchIndex(searched.trim());
    }
  };

  searchIndex = searchterm => {
    fetch("https://whaleshares.io/searchapi/users/suggest/" + searchterm.toString())
      .then(response => response.json())
      .then(data => {
        const test1 = data.suggestions.map((suggestion) => {
          return <Option className="searchResultUser" key={suggestion}>{suggestion}</Option>;
        });

        this.setState({
          searchTerm: searchterm,
          userSuggestions: data.suggestions,
          selectedOptions: test1,
        }, () => {
        });
      });
  };

  onBlur = () => {
    this.setState({
      // searchTerm: "",
    selectedOptions: [],
    selectedOption: 'Type to search...',
    userSuggestions: [],
    }, () => {
    });
  };

  onFocus = () => {
    document.querySelector("#customPlaceholder").innerHTML = ""
  };

  searchGoogleIndex = () => {
    const select = document.querySelector("#searchIndex");
    const element = google.search.cse.element.getElement('search'); // eslint-disable-line no-undef
    element.execute(this.state.searchTerm);
    this.setState({
      searchTerm: "",
    }, () => {
    });
  };

  onSelect = () => {
  };

  render() {
    const content = this.props.username ? this.menuForLoggedIn() : this.menuForLoggedOut();
    const { intl } = this.props;
    const { searchBarActive } = this.state;
    const homeUrl = this.props.username ? '/myfeed' : '/created#start';
    const selectedOption = this.state.selectedOption;
    const children = [].concat(this.state.selectedOptions);

    return (
      <div className="Topnav">
        <div className="topnav-layout">
          <div className={classNames('left', { 'Topnav__mobile-hidden': searchBarActive })}>
            <Link className="Topnav__brand" to={homeUrl}>
              <img src="/images/wls-logo-beta.png" className="logo" alt="Whaleshares Logo" />
              <img src="/images/wls-mark-beta.png" className="mark" alt="Whaleshares Logo" />
            </Link>
          </div>
          <div className={classNames('center', { mobileVisible: searchBarActive })}>
            <div className="Topnav__input-container">
              <Select
                showSearch
                style={this.state.searchStyle}
                onInputKeyDown={this.onSearchKeyDown}
                onBlur={this.onBlur}
                placeholder="Select a person"
                value={selectedOption}
                ref={ref => {
                  this.searchInputRef = ref;
                }}
                onSelect={this.handleSearchChange}
              >
                <OptGroup label="Users">
                  {children}
                </OptGroup>
              </Select>
              <a
                className={"searchButton "}
                onClick={this.searchGoogleIndex}
              >
                <i className="iconfont icon-search" />
              </a>
            </div>
          </div>
          <div className="right">
            <button
              className={classNames('Topnav__mobile-search', {
                'Topnav__mobile-search-close': searchBarActive,
              })}
              onClick={this.handleMobileSearchButtonClick}
            >
            </button>
            {content}
          </div>
          <div id="SearchArea"/>
        </div>
      </div>
    );
  }
}

export default Topnav;
