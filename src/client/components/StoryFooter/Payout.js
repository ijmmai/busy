import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import classNames from 'classnames';
import { calculatePayout } from '../../vendor/steemitHelpers';
import BTooltip from '../BTooltip';
import WLSDisplay from '../Utils/WLSDisplay';
import PayoutDetail from '../PayoutDetail';
import './Payout.less';

const Payout = ({ intl, post }) => {
  const payout = calculatePayout(post);
  const payoutValue = payout.pastPayouts;

  return (
    payoutValue > 0 && (
      <span className="Payout">
        <BTooltip title={<PayoutDetail post={post} />}>
          <span
            className={classNames({
              'Payout--rejected': payout.isPayoutDeclined,
            })}
          >
            <WLSDisplay value={payoutValue} fraction={0}/>
          </span>
        </BTooltip>
        <BTooltip
          title={intl.formatMessage({
            id: 'reward_option_100',
            defaultMessage: '100% WHALESTAKE',
          })}
         />
      </span>
    )
  );
};

Payout.propTypes = {
  intl: PropTypes.shape().isRequired,
  post: PropTypes.shape().isRequired,
};

export default injectIntl(Payout);
