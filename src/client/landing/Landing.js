import React from 'react';
import { Helmet } from 'react-helmet';
import { Row, Col, Button, Icon } from 'antd';
import { WALLET_URL, EXPLORER_URL, WHALEPAPER_URL, DISCORD_URL } from '../../common/constants/settings';
import BannerLanding from './BannerLanding';
import faWallet from '../../../assets/images/svgs/faWallet.svg';
import faCubes from '../../../assets/images/svgs/faCubes.svg';
import faFilePdf from '../../../assets/images/svgs/faFilePdf.svg';
import faUserShield from '../../../assets/images/svgs/faUserShield.svg';
import faDiscord from '../../../assets/images/svgs/faDiscord.svg';
import './Landing.less';
import SignupButton from '../components/SignupButton.js';

const Landing = () => (
  <div className="FrontPage">
    <Helmet>
      <title>Welcome to Whaleshares — the Social Sharing Friends Network!</title>
    </Helmet>
    <BannerLanding />

    <a className="anchor" name="one" />
    <div className="layout-landing container">
      <Row type="flex" className="centered">
        <Col span={24}>
          <h2>What are you interested in?</h2>
        </Col>
      </Row>
      <Row type="flex">
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-01.svg" alt="illustration" />
          <h3>Share Your Interests</h3>
          <p>Post about topics that interest you and interact with others who share them!</p>
        </Col>
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-02.svg" alt="illustration" />
          <h3>Create Unique Content</h3>
          <p>Create and share art, photography, poems, politics, or whatever else is on your mind.</p>
        </Col>
      </Row>
      <Row type="flex">
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-03.svg" alt="illustration" />
          <h3>Meet Like-Minded People</h3>
          <p>Network with communities of like-minded individuals who share your interests.</p>
        </Col>
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-04.svg" alt="illustration" />
          <h3>Reward Others</h3>
          <p>Spread the love, Comment and Tip content shared by others to show your appreciation.</p>
        </Col>
      </Row>
      {/* <Row type="flex">
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-05.svg" alt="illustration" />
          <h3>Grow Your Community</h3>
          <p>Find and network with people just like you and build lasting communities.</p>
        </Col>
        <Col span={12} className="centered drawing">
          <img src="/images/front/drawing-06.svg" alt="illustration" />
          <h3>Connect with Friends</h3>
          <p>Network with communities of like-minded individuals who share your interests.</p>
        </Col>
      </Row> */}
      <Row className="centered cta">
        <Col span={24}>
          <SignupButton />
        </Col>
      </Row>
    </div>

    <a className="anchor" name="two" />
    <div className="layout-landing  full-width inverse pods">
      <Row type="flex" className="container" justify="space-around" align="middle">
        <Col span={12}>
          <img src="/images/front/drawing-clubs.png" alt="illustration" />
        </Col>
        <Col span={12}>
          <h2>Create and Join Pods (Communities)</h2>
          <p>One of the fastest ways to meet new people who share the same interest as you is by joining Pods.</p>
          <p>Pods are created for users, by users. Simply search by interest and get connected.</p>
          <p>Cannot find what you are looking for? No problem, you can create your own Pod in no time.</p>
          <div className="centered">
            <SignupButton />
          </div>
        </Col>
      </Row>
    </div>

    <a className="anchor" name="three" />
    <div className="layout-landing full-width rewards">
      <Row type="flex" className="container" justify="start" align="middle">
        <Col span={12} className="show-on-mobile">
          <img src="/images/front/wls-tree-2.jpg" alt="illustration" />
        </Col>
        <Col span={12}>
          <h2>Earn Daily Rewards</h2>
          <p>
            Whaleshares offer its users the opportunity to earn cryptocurrency rewards for posting and sharing content
            that interests them that others find value in. Additionally, cross-chain WhaleTokens can be earned and used
            to further boost your earning potential.
          </p>
          <p>Your account includes a built-in wallet to manage your earned and staked WLS tokens.</p>
          <SignupButton />
        </Col>
      </Row>
    </div>

    <div className="layout-landing full-width inverse footer">
      <Row type="flex" className="container">
        <Col span={24}>
          <ul className="menu-bottom">
            <li>
              <a href={WALLET_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faWallet}/>
                <span>Wallet</span>
              </a>
            </li>
            <li>
              <a href={EXPLORER_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faCubes}/>
                <span>Block Explorer</span>
              </a>
            </li>
            <li>
              <a href={WHALEPAPER_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faFilePdf}/>
                <span>WhalePaper</span>
              </a>
            </li>
            <li>
              <a href={DISCORD_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faDiscord}/>
                <span>Discord</span>
              </a>
            </li>
            <li>
              <a href="/privacy">
                <Icon component={faUserShield}/>
                <span>Privacy Policy</span>
              </a>
            </li>
          </ul>
        </Col>
      </Row>
    </div>
  </div>
);

export default Landing;
