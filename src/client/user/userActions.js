import { createAction } from 'redux-actions';
import { getAuthenticatedUserName, getIsAuthenticated } from '../reducers';
import { getAllFollowing, getMutedAsync } from '../helpers/apiHelpers';
import { createAsyncActionType } from '../helpers/stateHelpers';
import { notify } from '../app/Notification/notificationActions';
import { truncateDecimal } from '../helpers/formatter';

export const MUTE_USER = createAsyncActionType('@user/MUTE_USER');
export const UNMUTE_USER = createAsyncActionType('@user/UNMUTE_USER');

export const muteUser = (user, type = 'ignore') => (dispatch, getState, { steemAPI, Keys }) => {
  const state = getState();
  const { username, keys } = Keys.decrypt();

  if (!getIsAuthenticated(state)) {
    return Promise.reject('User is not authenticated');
  }

  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}mute` : keys.postingKey;

  return dispatch({
    type: type === 'ignore' ? MUTE_USER.ACTION : UNMUTE_USER.ACTION,
    payload: {
      promise: steemAPI.chainLib.broadcast.customJsonAsync(
        useKey,
        [],
        [username],
        'follow',
        JSON.stringify([
          'follow',
          {
            follower: username,
            following: user,
            what: [type],
          },
        ]),
      ),
    },
    meta: {
      username: user,
    },
  });
};

export const FOLLOW_USER = '@user/FOLLOW_USER';
export const FOLLOW_USER_START = '@user/FOLLOW_USER_START';
export const FOLLOW_USER_SUCCESS = '@user/FOLLOW_USER_SUCCESS';
export const FOLLOW_USER_ERROR = '@user/FOLLOW_USER_ERROR';

export const followUser = user => (dispatch, getState, { steemAPI, Keys }) => {
  const state = getState();
  const { username, keys } = Keys.decrypt();

  if (!getIsAuthenticated(state)) {
    return Promise.reject('User is not authenticated');
  }

  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}follows` : keys.postingKey;

  return dispatch({
    type: FOLLOW_USER,
    payload: {
      promise: steemAPI.chainLib.broadcast.customJsonAsync(
        useKey,
        [],
        [username],
        'follow',
        JSON.stringify([
          'follow',
          {
            follower: username,
            following: user,
            what: ['blog'],
          },
        ]),
      ),
    },
    meta: {
      username: user,
    },
  });
};

export const UNFOLLOW_USER = '@user/UNFOLLOW_USER';
export const UNFOLLOW_USER_START = '@user/UNFOLLOW_USER_START';
export const UNFOLLOW_USER_SUCCESS = '@user/UNFOLLOW_USER_SUCCESS';
export const UNFOLLOW_USER_ERROR = '@user/UNFOLLOW_USER_ERROR';

export const unfollowUser = user => (dispatch, getState, { steemAPI, Keys }) => {
  const state = getState();
  const { username, keys } = Keys.decrypt();

  if (!getIsAuthenticated(state)) {
    return Promise.reject('User is not authenticated');
  }

  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}follows` : keys.postingKey;

  return dispatch({
    type: UNFOLLOW_USER,
    payload: {
      promise: steemAPI.chainLib.broadcast.customJsonAsync(
        useKey,
        [],
        [username],
        'follow',
        JSON.stringify([
          'follow',
          {
            follower: username,
            following: user,
            what: ['null'],
          },
        ]),
      ),
    },
    meta: {
      username: user,
    },
  });
};

export const GET_FOLLOWING = '@user/GET_FOLLOWING';
export const GET_FOLLOWING_START = '@user/GET_FOLLOWING_START';
export const GET_FOLLOWING_SUCCESS = '@user/GET_FOLLOWING_SUCCESS';
export const GET_FOLLOWING_ERROR = '@user/GET_FOLLOWING_ERROR';

export const getFollowing = username => (dispatch, getState) => {
  const state = getState();
  if (!username) username = state.auth.user.name;
  if (!username && !getIsAuthenticated(state)) {
    return dispatch({ type: GET_FOLLOWING_ERROR });
  }

  const targetUsername = username || getAuthenticatedUserName(state);

  return dispatch({
    type: GET_FOLLOWING,
    meta: targetUsername,
    payload: {
      promise: getAllFollowing(targetUsername).catch(() => dispatch({ type: GET_FOLLOWING_ERROR })),
    },
  });
};

export const GET_MUTED = createAsyncActionType('@user/GET_MUTED');

export const getMuted = username => (dispatch, getState) => {
  const state = getState();
  if (!username) username = state.auth.user.name;
  if (!username && !getIsAuthenticated(state)) {
    return dispatch({ type: GET_MUTED.ERROR });
  }

  const targetUsername = username || getAuthenticatedUserName(state);

  return dispatch({
    type: GET_MUTED.ACTION,
    meta: targetUsername,
    payload: {
      promise: getMutedAsync(targetUsername).catch(() => dispatch({ type: GET_MUTED.ERROR })),
    },
  });
};

export const UPDATE_RECOMMENDATIONS = '@user/UPDATE_RECOMMENDATIONS';
export const updateRecommendations = createAction(UPDATE_RECOMMENDATIONS);

const NOTIFICATIONS_LIMIT = 20;
export const GET_NOTIFICATIONS = createAsyncActionType('@user/GET_NOTIFICATIONS');

export const getNotifications = username => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  if (!username) username = state.auth.user.name;
  if (!username && !getIsAuthenticated(state)) {
    return dispatch({ type: GET_NOTIFICATIONS.ERROR });
  }

  const targetUsername = username || getAuthenticatedUserName(state);

  return dispatch({
    type: GET_NOTIFICATIONS.ACTION,
    meta: targetUsername,
    payload: {
      promise: busyAPI.sendAsync('database_api', 'get_account_notification', [targetUsername, -1, NOTIFICATIONS_LIMIT]),
    },
  });
};

export const GET_NOTIFICATIONS_MORE = createAsyncActionType('@user/GET_NOTIFICATIONS_MORE');

export const getNotificationsMore = username => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  if (!username) username = state.auth.user.name;
  if (!username && !getIsAuthenticated(state)) {
    return dispatch({ type: GET_NOTIFICATIONS_MORE.ERROR });
  }
  const targetUsername = username || getAuthenticatedUserName(state);

  let idx = -1;
  try {
    const notifications = state.user.notifications || [];
    if (notifications.length > 0) {
      idx = notifications[notifications.length - 1][0] - 1;
      if (idx < 0) idx = 0;
    }
  } catch (e) {
    // do nothing
  }
  const limit = (idx < NOTIFICATIONS_LIMIT) ? idx : NOTIFICATIONS_LIMIT;

  return dispatch({
    type: GET_NOTIFICATIONS_MORE.ACTION,
    meta: targetUsername,
    payload: {
      promise: busyAPI.sendAsync('database_api', 'get_account_notification', [targetUsername, idx, limit]),
    },
  });
};

export const GET_USER_PODS = createAsyncActionType('@user/GET_USER_PODS');

export const getUserPods = (limit = 100) => (dispatch, getState, { busyAPI }) => {
  const state = getState();
  const username = getAuthenticatedUserName(state);
  const { pods } = state.user;
  return dispatch({
    type: GET_USER_PODS.ACTION,
    payload: busyAPI.sendAsync('database_api', 'get_pod_members_by_user', [
      username,
      pods.length > 0 ? pods[pods.length - 1].account : '',
      limit,
    ]),
  });
};


export const TIP_USER = createAsyncActionType('@user/TIP_USER');

export const tipUser = (user, amount, memo) => async (dispatch, getState, { steemAPI, Keys, busyAPI }) => {
  const state = getState();
  const { username, keys, hasWhaleVault } = Keys.decrypt();

  if (!getIsAuthenticated(state)) return Promise.reject('User is not authenticated');

  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}follows` : keys.postingKey;
  let enc_memo = memo.startsWith('#');
  if (enc_memo) {
    let tipper = username;
    let author = user;

    let author_pubkey = state.users && state.users['user-'+author] ? state.users['user-'+author].posting.key_auths[0][0] : "";
    if (author_pubkey == '') {
      let auth_result = await busyAPI.sendAsync('database_api', 'get_accounts', [[author]]);
      author_pubkey = auth_result[0].posting.key_auths[0][0];
    }
    if (hasWhaleVault) {
      let response = await steemAPI.chainLib.config.whalevault.promiseRequestEncryptMemo('wls_busy', 'wls:'+tipper, memo, 'posting', author_pubkey, 'stm', 'tip_memo');
      if (response.success) memo = response.result; else {
        dispatch(notify(response.message, 'error'));
        return;
      }
    } else {
      try {
        memo = steemAPI.chainLib.memo.encode(useKey, author_pubkey, memo);
      } catch (e) {
        dispatch(notify(e.message, 'error'));
        return;
      }
    }
  }
  const operations = [["social_action", {
    account: username,
    action: [5, { // social_action_user_tip
      to: user,
      amount: `${truncateDecimal(amount, 3).toFixed(3)} WLS`,
      memo,
    }]
  }]];

  return dispatch({
    type: TIP_USER.ACTION,
    payload: {
      promise: steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ]).then(res => {
        dispatch(notify('Tip user OK!', 'success'));
        return res;
      }).catch(err => {
        dispatch(notify(err.message, 'error'));
      }),
    },
    meta: { username: user },
  });
};
