import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import { FormattedMessage } from 'react-intl';
import '../components/UserMenu.less';
import Topic from '../components/Button/Topic';

class FeedMenu extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    onTopicClose: PropTypes.func,
    defaultKey: PropTypes.string,
    authenticated: PropTypes.bool,
  };

  static defaultProps = {
    onChange: () => {},
    onTopicClose: () => {},
    defaultKey: 'myfeed',
    authenticated: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      current: props.defaultKey ? props.defaultKey : 'myfeed',
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.defaultKey ? nextProps.defaultKey : 'myfeed',
    });
  }

  onTopicClose = () => this.props.onTopicClose();

  getItemClasses = key => classNames('UserMenu__item', { 'UserMenu__item--active': this.state.current === key });

  handleClick = e => {
    const key = e.currentTarget.dataset.key;
    this.setState({ current: key }, () => this.props.onChange(key));
  };

  render() {
    return (
      <div className="UserMenu">
        <div className="container menu-layout">
          <div className="left" />
            <ul className="UserMenu__menu__feed">
              {this.props.authenticated && (
                <li
                  className={this.getItemClasses('myfeed')}
                  onClick={this.handleClick}
                  role="presentation"
                  data-key="myfeed"
                >
                  <FormattedMessage id="myfeed" defaultMessage="My Feed" />
                </li>
              )}

              <li
               className={this.getItemClasses('created')}
               onClick={this.handleClick}
               role="presentation"
               data-key="created"
              >
               <FormattedMessage id="sort_created" defaultMessage="Public Feed" />
              </li>

              <div className="FeedMenu__Remove__Filter">
                {this.props.topic && (
                  <Topic key={this.props.topic} name={this.props.topic} onClose={this.onTopicClose} closable />
                )}
              </div>
            </ul>
        </div>
      </div>
    );
  }
}

export default FeedMenu;
