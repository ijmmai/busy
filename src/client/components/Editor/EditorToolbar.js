import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Button, Menu, Dropdown, Icon } from 'antd';
import BTooltip from '../BTooltip';
import './EditorToolbar.less';

const tooltip = (description, shortcut) => (
  <span>
    {description}
    <br />
    <b>{shortcut}</b>
  </span>
);

const FontSizes = (size) => {
  const Key     = 'h' + size;
  const id      = 'heading_' + size;
  const message = 'Heading ' + size;

  return (
    <Menu.Item key={Key}>
      <Key>
        <FormattedMessage id={id} defaultMessage={message}/>
      </Key>
    </Menu.Item>
  )
};

const EditorToolbar = ({ intl, onSelect, onEmoji }) => {
  const menu = (
      <Menu onClick={e => onSelect(e.key)}>
        {[1,2,3,4,5,6].map((size) => (FontSizes(size)))}
      </Menu>
    );
  
  return (
    <Scrollbars
      style={{ width: '100%', height: 40 }}
      universal
      autoHide
      renderView={({ style, ...props }) => (
        <div style={{ ...style }} {...props} className="EditorToolbar__container" />
      )}
    >
      <div className="EditorToolbar">
        <Dropdown overlay={menu}>
          <Button className="EditorToolbar__button">
            <i className="iconfont icon-fontsize" /> <Icon type="down" />
          </Button>
        </Dropdown>
        <BTooltip
          title={tooltip(intl.formatMessage({ id: 'bold', defaultMessage: 'Add bold' }), 'Ctrl+b')}
        >
          <Button className="EditorToolbar__button" onClick={() => onSelect('b')}>
            <i className="iconfont icon-bold" />
          </Button>
        </BTooltip>
        <BTooltip
          title={tooltip(
            intl.formatMessage({ id: 'italic', defaultMessage: 'Add italic' }),
            'Ctrl+i',
          )}
        >
          <Button className="EditorToolbar__button" onClick={() => onSelect('i')}>
            <i className="iconfont icon-italic" />
          </Button>
        </BTooltip>
        <BTooltip
          title={tooltip(
            intl.formatMessage({ id: 'quote', defaultMessage: 'Add quote' }),
            'Ctrl+q',
          )}
        >
          <Button className="EditorToolbar__button" onClick={() => onSelect('q')}>
            <i className="iconfont icon-q1" />
          </Button>
        </BTooltip>
        <BTooltip
          title={tooltip(intl.formatMessage({ id: 'link', defaultMessage: 'Add link' }), 'Ctrl+k')}
        >
          <Button className="EditorToolbar__button" onClick={() => onSelect('link')}>
            <i className="iconfont icon-link" />
          </Button>
        </BTooltip>
        <BTooltip
          title={tooltip(
            intl.formatMessage({ id: 'image', defaultMessage: 'Add image' }),
            'Ctrl+m',
          )}
        >
          <Button className="EditorToolbar__button" onClick={() => onSelect('image')}>
            <i className="iconfont icon-picture" />
          </Button>
        </BTooltip>
        <BTooltip
          title={tooltip(
            intl.formatMessage({ id: 'emoji', defaultMessage: 'Add emoji' }),
          )}
        >
          <Button className="EditorToolbar__button EditorToolbar__emoji" onClick={() => onEmoji()}>
            <span role='img' aria-label='emoji'>😀</span>
          </Button>
        </BTooltip>
      </div>
    </Scrollbars>
  );
};

EditorToolbar.propTypes = {
  intl: PropTypes.shape().isRequired,
  onSelect: PropTypes.func,
};

EditorToolbar.defaultProps = {
  onSelect: () => {},
};

export default injectIntl(EditorToolbar);
