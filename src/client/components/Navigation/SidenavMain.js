import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { NavLink, Link } from 'react-router-dom';
import { Button, Icon } from 'antd';
import faCommentDots from '../../../../assets/images/svgs/faCommentDots.svg';
import faThList from '../../../../assets/images/svgs/faThList.svg';
import faPencilAlt from '../../../../assets/images/svgs/faPencilAlt.svg';
import faToiletPaper from '../../../../assets/images/svgs/faToiletPaper.svg';
import faBookmark from '../../../../assets/images/svgs/faBookmark.svg';
import faComments from '../../../../assets/images/svgs/faComments.svg';
import faGem from '../../../../assets/images/svgs/faGem.svg';
import faUserPlus from '../../../../assets/images/svgs/faUserPlus.svg';
import faUserTag from '../../../../assets/images/svgs/faUserTag.svg';
import faUserFriends from '../../../../assets/images/svgs/faUserFriends.svg';
import faUsers from '../../../../assets/images/svgs/faUsers.svg';
import faUsersClass from '../../../../assets/images/svgs/faUsersClass.svg';
import faWallet from '../../../../assets/images/svgs/faWallet.svg';
import faCog from '../../../../assets/images/svgs/faCog.svg';
import faVoteYea from '../../../../assets/images/svgs/faVoteYea.svg';
import faCoins from '../../../../assets/images/svgs/faCoins.svg';
import faScrollOld from '../../../../assets/images/svgs/faScrollOld.svg';
import faCubes from '../../../../assets/images/svgs/faCubes.svg';
import faRandom from '../../../../assets/images/svgs/faRandom.svg';
import faGift from '../../../../assets/images/svgs/faGift.svg';
import faDiscord from '../../../../assets/images/svgs/faDiscord.svg';
import faGamepad from '../../../../assets/images/svgs/faGamepad.svg';
import faLock from '../../../../assets/images/svgs/faLock.svg';
import { getIsAuthenticated, getAuthenticatedUserName, getIsAuthFetching } from '../../reducers';
import Loading from '../../components/Icon/Loading';
import IconTip from '../../../../assets/images/svgs/IconTips.svg';
import Avatar from '../Avatar';
import SignUp from '../../components/Sidebar/SignUp';
import { WALLET_URL, EXPLORER_URL, WHALEPAPER_URL, DISCORD_URL } from '../../../common/constants/settings';
import { fetchFriends, fetchPendingReceivedRequests } from '../../friends/actions';
import { getWhalevaultLink } from '../../utils/misc';
import './Sidenav.less';

@connect(
  state => ({
    ...state.friends,
    authenticated: getIsAuthenticated(state),
    authenticatedUserName: getAuthenticatedUserName(state),
    isAuthFetching: getIsAuthFetching(state),
  }),
  {
    fetchFriends,
    fetchPendingReceivedRequests,
  },
)
class SidenavMain extends React.Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    authenticatedUserName: PropTypes.string,
    isAuthFetching: PropTypes.bool.isRequired,
    pendingReceivedRequests: PropTypes.oneOfType([PropTypes.bool, PropTypes.arrayOf(PropTypes.shape())]),
    friends: PropTypes.oneOfType([PropTypes.bool, PropTypes.arrayOf(PropTypes.string)]),
    fetchPendingReceivedRequests: PropTypes.func,
    fetchFriends: PropTypes.func,
  };

  static defaultProps = {
    authenticated: false,
    authenticatedUserName: '',
    isAuthFetching: false,
    pendingReceivedRequests: false,
    friends: false,
    fetchPendingReceivedRequests: () => {},
    fetchFriends: () => {},
  };

  componentDidMount() {
    if (this.props.pendingReceivedRequests === false) this.props.fetchPendingReceivedRequests();
    if( this.props.friends === false) this.props.fetchFriends();
  }

  render() {
    const { authenticated, isAuthFetching } = this.props;
    const username = this.props.authenticatedUserName;

    if (isAuthFetching) return <Loading />;

    const isUser = (match, location) =>
      location.pathname.match(/@[a-zA-Z0-9-.]*[/(shares|comments|followers|followed|transfers|activity)]?/);
    const isNews = (match, location) => location.pathname.match(/(myfeed|created)/);
    const isStatus = (match, location) => location.pathname.match(/status/);
    const isReplies = (match, location) => location.pathname.match(/replies/);
    const isComments = (match, location) => location.pathname.match(/comments/);
    const isCompose = (match, location) => location.pathname.match(/editor/);
    const isDrafts = (match, location) => location.pathname.match(/drafts/);
    const isBookmarks = (match, location) => location.pathname.match(/bookmarks/);
    const isPodList = (match, location) => location.pathname.match(/pod_list/);
    const isSendFriendRequest = (match, location) => location.pathname.match(/friend_send_request/);
    const isReceivedFriendRequest = (match, location) => location.pathname.match(/friend_received_request/);
    const isMyFriends = (match, location) => location.pathname.match(/my_friends/);
    const isTips = (match, location) => location.pathname.match(/tips[/(received|sent)]?/);
    const isDiscoverPeople = (match, location) => location.pathname.match(/discover_people/);
    const wvLink = getWhalevaultLink();

    return (
      <div>
        {!authenticated && <SignUp />}
        {authenticated && (
          <ul className="Sidenav">
            <li className={isUser(null, location) ? 'Sidenav__active' : null}>
              <NavLink to={`/@${username}`}>
                <Avatar username={username} size={26} />
                <FormattedMessage id="my_posts" defaultMessage="My Posts" />
              </NavLink>
            </li>
            <li className={isNews(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/myfeed" activeClassName="Sidenav__item--active" isActive={isNews}>
                <Icon component={faThList}/>
                <FormattedMessage id="feed" defaultMessage="Feed" />
              </NavLink>
            </li>
            <li className={isReplies(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/replies" activeClassName="Sidenav__item--active" isActive={isReplies}>
                <Icon component={faCommentDots}/>
                <FormattedMessage id="replies" defaultMessage="Replies" />
              </NavLink>
            </li>
            <li className={isComments(null, location) ? 'Sidenav__active' : null}>
              <NavLink to={`/comments`} activeClassName="Sidenav__item--active" isActive={isComments}>
                <Icon component={faComments}/>
                <FormattedMessage id="my_comments" defaultMessage="My Comments" />
              </NavLink>
            </li>
            <li className={isCompose(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/editor" activeClassName="Sidenav__item--active" isActive={isCompose}>
                <Icon component={faPencilAlt}/>
                <FormattedMessage id="compose" defaultMessage="Compose" />
              </NavLink>
            </li>
            <li className={isDrafts(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/drafts" activeClassName="Sidenav__item--active" isActive={isDrafts}>
                <Icon component={faToiletPaper}/>
                <FormattedMessage id="drafts" defaultMessage="Drafts" />
              </NavLink>
            </li>
            <li className={isBookmarks(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/bookmarks" activeClassName="Sidenav__item--active" isActive={isBookmarks}>
                <Icon component={faBookmark}/>
                <FormattedMessage id="bookmarks" defaultMessage="Bookmarks" />
              </NavLink>
            </li>
            <li className={isTips(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/tips/received" activeClassName="Sidenav__item--active" isActive={isTips}>
                <Icon component={IconTip} />
                <FormattedMessage id="tips" defaultMessage="Tips" />
              </NavLink>
            </li>
            <li className={isStatus(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/status" activeClassName="Sidenav__item--active" isActive={isStatus}>
                <Icon component={faGem}/>
                <FormattedMessage id="status_rewards" defaultMessage="Status/Rewards" />
              </NavLink>
            </li>

            <h4><FormattedMessage id="Friends" defaultMessage="Friends"/></h4>
            {<li className={isSendFriendRequest(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/friend_send_request" activeClassName="Sidenav__item--active" isActive={isSendFriendRequest}>
                <Icon component={faUserPlus}/>
                Send Request
              </NavLink>
            </li>}
            <li className={isReceivedFriendRequest(null, location) ? 'Sidenav__active' : null}>
              <NavLink
                to="/friend_received_request"
                activeClassName="Sidenav__item--active"
                isActive={isReceivedFriendRequest}
              >
                <Icon component={faUserTag}/>
                <FormattedMessage id="received_requests" defaultMessage="Requests" />
                {this.props.pendingReceivedRequests.length > 0 && (
                  <span className="notification">{this.props.pendingReceivedRequests.length}</span>
                )}
              </NavLink>
            </li>
            <li className={isMyFriends(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/my_friends" activeClassName="Sidenav__item--active" isActive={isMyFriends}>
                <Icon component={faUserFriends}/>
                My Friends
              </NavLink>
            </li>

            <h4><FormattedMessage id="discover" defaultMessage="Discover" /></h4>
            <li className={isPodList(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/pod_list" activeClassName="Sidenav__item--active" isActive={isPodList}>
                <Icon component={faUsers}/>
                <FormattedMessage id="podlist" defaultMessage="Discover Pods" />
              </NavLink>
            </li>
            <li className={isDiscoverPeople(null, location) ? 'Sidenav__active' : null}>
              <NavLink to="/discover_people" activeClassName="Sidenav__item--active" isActive={isDiscoverPeople}>
                <Icon type="search" />
                Discover People
              </NavLink>
            </li>
            <h4>
              <FormattedMessage id="Dapps" defaultMessage="Dapps" />
            </h4>
            <li>
              <a href="https://sharebits.io" target="_blank" rel="noopener noreferrer">
              <Icon component={faGift}/>
                <FormattedMessage id="sharebits" defaultMessage="ShareBits" />
              </a>
            </li>
            <li>
              <a href={wvLink} target="_blank" rel="noopener noreferrer">
              <Icon component={faLock}/>
                <FormattedMessage id="whalevault" defaultMessage="WhaleVault" />
              </a>
            </li>

            <h4>
              <FormattedMessage id="Extras" defaultMessage="Extras" />
            </h4>
            <li>
              <a href={WALLET_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faWallet}/>
                <FormattedMessage id="wallet2" defaultMessage="Wallet" />
              </a>
            </li>
            <li>
              <a href={`${WALLET_URL}/#/profile_settings`} target="_blank" rel="noopener noreferrer">
                <Icon component={faCog}/>
                <FormattedMessage id="settings" defaultMessage="Settings" />
              </a>
            </li>
            <li>
              <a href={`${WALLET_URL}/#/witnesses`} target="_blank" rel="noopener noreferrer">
                <Icon component={faVoteYea}/>
                <FormattedMessage id="witnesses" defaultMessage="Vote for Witnesses" />
              </a>
            </li>
            <li>
              <a href={`${WALLET_URL}/#/pod_create`}>
                <Icon component={faUsersClass}/>
                <FormattedMessage id="create_pod" defaultMessage="Create a Pod" />
              </a>
            </li>
            <li>
              <a href={WHALEPAPER_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faScrollOld}/>
                <FormattedMessage id="whalepaper" defaultMessage="WhalePaper" />
              </a>
            </li>
            <li>
              <a href={EXPLORER_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faCubes}/>
                <FormattedMessage id="explorer" defaultMessage="Blockchain Explorer" />
              </a>
            </li>
            <li>
              <a href={DISCORD_URL} target="_blank" rel="noopener noreferrer">
                <Icon component={faDiscord}/>
                <FormattedMessage id="discord" defaultMessage="Discord" />
              </a>
            </li>
            <li className="bottom-section donate">
              <Link to="/donate">
                <Button type="primary" block>
                  <FormattedMessage id="donate" defaultMessage="Donate" />
                </Button>
              </Link>
            </li>
            <li className="bottom-section">
              <Link to="/privacy">
                <FormattedMessage id="privacy" defaultMessage="Privacy Policy" />
              </Link>
            </li>
          </ul>
        )}
      </div>
    );
  }
}

export default SidenavMain;



