import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import { FormattedMessage } from 'react-intl';
import './UserMenu.less';

class UserMenu extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    defaultKey: PropTypes.string,
    followers: PropTypes.number,
    following: PropTypes.number,
  };

  static defaultProps = {
    onChange: () => {
    },
    defaultKey: 'discussions',
    followers: 0,
    following: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      current: props.defaultKey ? props.defaultKey : 'discussions',
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.defaultKey ? nextProps.defaultKey : 'discussions',
    });
  }

  getItemClasses = key => classNames('UserMenu__item', {'UserMenu__item--active': this.state.current === key});

  handleClick = e => {
    const key = e.currentTarget.dataset.key;
    this.setState({current: key}, () => this.props.onChange(key));
  };

  render() {
    return (
      <div className="UserMenu">
        <div className="menu-layout">
          <Scrollbars
            universal
            autoHide
            renderView={({style, ...props}) => <div style={{...style}} {...props} />}
            style={{width: '100%', height: 48}}
          >
            <ul className="UserMenu__menu">
              <li
                className={this.getItemClasses('discussions')}
                onClick={this.handleClick}
                role="presentation"
                data-key="discussions"
              >
                <FormattedMessage id="posts" defaultMessage="Posts"/>
              </li>
              <li
                className={this.getItemClasses('shares')}
                onClick={this.handleClick}
                role="presentation"
                data-key="shares"
              >
                <FormattedMessage id="shares" defaultMessage="Shares"/>
              </li>
              {/*<li*/}
              {/*  className={this.getItemClasses('comments')}*/}
              {/*  onClick={this.handleClick}*/}
              {/*  role="presentation"*/}
              {/*  data-key="comments"*/}
              {/*>*/}
              {/*  <FormattedMessage id="comments" defaultMessage="Comments" />*/}
              {/*</li>*/}
              <li
                className={this.getItemClasses('pods')}
                onClick={this.handleClick}
                role="presentation"
                data-key="pods"
              >
                <FormattedMessage id="pods" defaultMessage="Pods"/>
              </li>
              <li
                className={this.getItemClasses('friends')}
                onClick={this.handleClick}
                role="presentation"
                data-key="friends"
              >
                <FormattedMessage id="friends" defaultMessage="Friends"/>
              </li>
              <li
                className={this.getItemClasses('followers')}
                onClick={this.handleClick}
                role="presentation"
                data-key="followers"
              >
                <FormattedMessage id="followers" defaultMessage="Followers"/>
              </li>
              <li
                className={this.getItemClasses('followed')}
                onClick={this.handleClick}
                role="presentation"
                data-key="followed"
              >
                <FormattedMessage id="followed" defaultMessage="Followed"/>
              </li>
              {this.props.ownProfile && <li
                className = {this.getItemClasses('muted')}
                onClick   = {this.handleClick}
                role      = "presentation"
                data-key  = "muted"
              >
                <FormattedMessage id="muted" defaultMessage="Muted"/>
              </li>}
            </ul>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

export default UserMenu;
