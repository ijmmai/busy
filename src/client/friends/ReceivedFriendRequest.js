import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage, FormattedRelative } from 'react-intl';
import _ from 'lodash';
import { Button, message } from 'antd';
import requiresLogin from '../auth/requiresLogin';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import Avatar from '../components/Avatar';
import '../styles/modules/account_list.less';
import { fetchPendingReceivedRequests, approvalRequest } from './actions';

@requiresLogin
@connect(
  state => ({
    ...state.friends,
  }),
  {
    fetchPendingReceivedRequests,
    approvalRequest,
  },
)
class ReceivedFriendRequest extends React.Component {
  componentDidMount() {
    if (this.props.pendingReceivedRequests === false) this.props.fetchPendingReceivedRequests();
  }

  handleApproval = (toUsername, accepted) => {
    const onSuccess = () => {
      this.props.fetchPendingReceivedRequests();
    };

    this.props.approvalRequest(toUsername, accepted, onSuccess);
  };

  render() {
    const renderReqsIn = this.props.pendingReceivedRequests
      ? this.props.pendingReceivedRequests.map(item => (
        <tr key={item.account}>
          <td className="acc-pic">
            <Avatar username={item.account} size={32}/>
          </td>
          <td className="acc-name">
            <Link to={'/@' + item.account}>@{item.account}</Link>
          </td>
          <td className="date">
            <FormattedRelative value={`${item.created}Z`}/>
          </td>
          <td className="actions">
            <Button
              className="RelAction"
              onClick={() => {
                this.handleApproval(item.account, false);
              }}
            >
              <FormattedMessage id="reject" defaultMessage="Reject"/>
            </Button>{' '}
            <Button
              className="RelAction"
              onClick={() => {
                this.handleApproval(item.account, true);
              }}
            >
              <FormattedMessage id="accept" defaultMessage="Accept"/>
            </Button>
          </td>
        </tr>
      ))
      : null;

    return (
      <div className="shifted">
        <Helmet>
          <title>Received Friend Requests</title>
        </Helmet>
        <div className="layout-twocol container AccList UseTable">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body ReceivedFriendRequests">
            <div className="main-title">
              <h2>
                <FormattedMessage id="incoming_requests" defaultMessage="Received" />
              </h2>
            </div>
            <div className="main-content">
              {!_.isEmpty(this.props.pendingReceivedRequests) ? (
                <table className="AvatarNameDateActions">
                  <thead>
                    <tr>
                      <th className="acc-pic" />
                      <th className="acc-name">
                        <FormattedMessage id="name" defaultMessage="Name" />
                      </th>
                      <th className="date">
                        <FormattedMessage id="date" defaultMessage="Date" />
                      </th>
                      <th className="actions">
                        <FormattedMessage id="actions" defaultMessage="Actions" />
                      </th>
                    </tr>
                  </thead>
                  <tbody>{renderReqsIn}</tbody>
                </table>
              ) : (
                <div className="message">
                  <FormattedMessage id="no_requests_in" defaultMessage="No pending incoming friend requests yet." />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ReceivedFriendRequest;
