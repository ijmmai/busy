import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames';
import { Icon, Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import Action from '../Button/Action';
import { FIXED_TAGS } from '../../../common/constants/settings';
import { POST_AUDIENCE_FRIENDS } from '../../helpers/postHelpers';

const { Option } = Select;

const QuickPostEditorFooter = ({
  currentImages,
  imageUploading,
  postCreationLoading,
  handleCreatePost,
  handleImageChange,
  postText,
  submittingPostText,
  onRemoveImage,
  handleChange,
  interest,
  destination,
  userPods,
}) => (
  // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
  <div className="QuickPostEditor__footer" tabIndex="0">
    <div className="QuickPostEditor__imagebox">
      {_.map(currentImages, image => (
        <div className="QuickPostEditor__imagebox__preview__image" key={image.id}>
          <div
            className = "QuickPostEditor__imagebox__remove"
            onClick   = {() => onRemoveImage(image)}
            role      = "presentation"
          >
            <i className="iconfont icon-delete_fill QuickPostEditor__imagebox__remove__icon" />
          </div>
          <img src={image.src} width="38" height="38" alt={image.src} />
        </div>
      ))}
      <input
        id        = "inputfile"
        className = "QuickPostEditor__footer__file"
        type      = "file"
        accept    = "image/*;capture=camera"
        onChange  = {handleImageChange}
      />
      <label htmlFor="inputfile">
        {imageUploading ? (
          <div className="QuickPostEditor__imagebox__loading">
            <Icon type="loading" />
          </div>
        ) : (
          <div
            className={classNames({
              QuickPostEditor__imagebox__upload: !_.isEmpty(currentImages),
            })}
          >
            <i
              className={classNames('iconfont QuickPostEditor__imagebox__upload__icon', {
                'icon-picture': _.isEmpty(currentImages),
                'icon-add'    : !_.isEmpty(currentImages),
              })}
            />
          </div>
        )}
      </label>
    </div>
    <div className="QuickPostEditor__interests">
      <p className="QuickPostEditor__interests__text">Interest</p>
      <Select
        value         = {interest}
        style         = {{ width: 150 }}
        onChange      = {handleChange('interest')}
      >
        {FIXED_TAGS.map(i => {
          return <Option key={i} value={i}>{i}</Option>
        })}
      </Select>
    </div>
    <div className="QuickPostEditor__audience">
      <p className="QuickPostEditor__audience__text">Audience</p>
      <Select 
        onChange      = {handleChange('destination')}
        style         = {{ width: 150 }} value={destination}>
        <Option key="public" value={''}>Public</Option>
        <Option key="friends" value={POST_AUDIENCE_FRIENDS}>Friends</Option>
        {userPods.length > 0 &&
          userPods.map(userPod => (
            <Option key={userPod.id} value={userPod.pod}>
              <FormattedMessage
                id="post_to_pod"
                defaultMessage="Pod: {pod}"
                values={{
                  pod: userPod.pod,
                }}
              />
            </Option>
          ))}
        </Select>
    </div>
    <div className="QuickPostEditor__submit">
      <p className="QuickPostEditor__submit__text">&nbsp;</p>
      <Action
        primary
        small
        loading   = {postCreationLoading}
        disabled  = {postCreationLoading}
        text      = {postCreationLoading ? submittingPostText : postText}
        onClick   = {handleCreatePost}
      />
    </div>
  </div>
);

QuickPostEditorFooter.propTypes = {
  currentImages       : PropTypes.arrayOf(PropTypes.shape()),
  imageUploading      : PropTypes.bool,
  postCreationLoading : PropTypes.bool,
  postText            : PropTypes.string,
  submittingPostText  : PropTypes.string,
  interest            : PropTypes.string,
  destination         : PropTypes.string,
  userPods            : PropTypes.arrayOf(PropTypes.shape()),
  handleCreatePost    : PropTypes.func,
  handleImageChange   : PropTypes.func,
  onRemoveImage       : PropTypes.func,
  handleChange        : PropTypes.func,
};

QuickPostEditorFooter.defaultProps = {
  currentImages       : [],
  imageUploading      : false,
  postCreationLoading : false,
  postText            : 'Post',
  submittingPostText  : 'Submitting',
  interest            : 'off-topic',
  destination         : '',
  userPods            : [],
  handleCreatePost    : () => {},
  handleImageChange   : () => {},
  onRemoveImage       : () => {},
  handleChange        : () => {},
};

export default QuickPostEditorFooter;
