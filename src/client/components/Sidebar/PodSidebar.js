import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { Divider } from 'antd';
import { FormattedMessage } from 'react-intl';
import { usernameURLRegex } from '../../helpers/regexHelpers';
import { DEFAULT_POD_RANKS } from '../../../common/constants/settings';
import PodBadge from '../PodBadge';
import SidebarBlock from '../../app/Sidebar/SidebarBlock';
import './SidebarContentBlock.less';
import './PodSidebar.less';
import './PostRecommendation.less';

/**
 * Return the level index, value from 0 to 4
 * @param count number of posts or comments
 * @param rank values, format [10, 100, 1000, 10000],
 */
const getRankLevel = (count, ranks) => {
  let rank = 0;
  try {
    for (let i = 0; i < ranks.length; i++) {
      if (count >= ranks[i]) {
        rank = i;
      }
    }
  } catch (e) {
    // do nothing
  }

  return rank;
};

class PodSidebar extends Component {
  render() {
    const { podMember, ranks, postRankLevel, commentRankLevel} = this.props;

    let renderContent = null;
    try {
      renderContent = (
        <>
          <PodBadge
            type        = 'post'
            name        = {ranks.names[postRankLevel] || DEFAULT_POD_RANKS.names[postRankLevel] || ''}
            level       = {postRankLevel}
            badgeImage  = {ranks.badges[postRankLevel] || DEFAULT_POD_RANKS.badges[postRankLevel] || 'https://whaleshares.io/images/icons/icon-128x128.png'}
            count       = {podMember.total_posts}
          />
          < Divider dashed style={{margin: 0}}/>
          <PodBadge
            type        = 'comment'
            name        = {ranks.names[commentRankLevel] || DEFAULT_POD_RANKS.names[commentRankLevel] || ''}
            level       = {commentRankLevel}
            badgeImage  = {ranks.badges[commentRankLevel] || DEFAULT_POD_RANKS.badges[commentRankLevel] || 'https://whaleshares.io/images/icons/icon-128x128.png'}
            count       = {podMember.total_comments}
          />
        </>);
    } catch (e) {
      renderContent = <p style={{margin: 16}}>Something wrong with pod ranks settings.</p>
    }

    if (!podMember) return null;

    return (
      <SidebarBlock 
        title   = 'My Pod Member Ranks' 
        icon    = 'safety'
        content = {renderContent}
      />
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  const podname = ownProps.location.pathname.match(usernameURLRegex)[1];
  let ranks = DEFAULT_POD_RANKS;
  {
    const pod = _.get(state, `users.users.user-${podname}.pod`, null);
    if (pod) {
      ranks = _.get(pod, 'json_metadata.ranks', DEFAULT_POD_RANKS);
    }
  }

  let podMember         = null;
  let postRankLevel     = 0;
  let commentRankLevel  = 0;
  {
    const userPods      = _.get(state, 'user.pods', []);
    podMember           = userPods.filter(item => item.pod === podname)[0];

    if (podMember) {
      postRankLevel     = getRankLevel(podMember.total_posts, _.get(ranks, 'posts', DEFAULT_POD_RANKS.posts));
      commentRankLevel  = getRankLevel(podMember.total_comments, _.get(ranks, 'comments', DEFAULT_POD_RANKS.comments));
    }
  }

  return {
    ...ownProps,
    podMember,
    ranks,
    postRankLevel,
    commentRankLevel
  };
};
const mapDispatchToProps = {
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  withRouter,
  withConnect
)(PodSidebar);
