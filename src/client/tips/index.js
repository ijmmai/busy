import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Menu } from 'antd';
import requiresLogin from '../auth/requiresLogin';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import { renderRoutes } from "react-router-config";
import '../styles/modules/account_list.less';

class Tips extends React.Component {
  render() {
    const current = this.props.location.pathname.split('/')[2];
    const currentKey = current || 'received';

    return (
      <div className="shifted">
        <Helmet>
          <title>Tips</title>
        </Helmet>
        <div className="layout-twocol container AccList UseTable">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center main-body Tips">
            <div className="main-title">
              <h1>Tips</h1>
            </div>
            <div className="main-content">

              <Menu selectedKeys={[currentKey]} mode="horizontal">
                <Menu.Item key="received"><Link to='/tips/received'>Received</Link></Menu.Item>
                <Menu.Item key="sent"><Link to='/tips/sent'>Sent</Link></Menu.Item>
              </Menu>

              {renderRoutes(this.props.route.routes)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    // ...state.tips,
  };
};
const mapDispatchToProps = dispatch => ({
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  requiresLogin,
  withRouter,
  withConnect
)(Tips);
