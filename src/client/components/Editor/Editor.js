import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { injectIntl, FormattedMessage } from 'react-intl';
import _ from 'lodash';
import readingTime from 'reading-time';
import { Form, Input, Select, Button } from 'antd';
import improve from '../../helpers/improve';
import Action from '../Button/Action';
import requiresLogin from '../../auth/requiresLogin';
import withEditor from './withEditor';
import EditorInput from './EditorInput';
import Body, { remarkable } from '../Story/Body';
import { FIXED_TAGS, MOODS } from '../../../common/constants/settings';
import { POST_AUDIENCE_FRIENDS } from '../../helpers/postHelpers';
import './Editor.less';

const { Option } = Select;

const moodOptions = Object.keys(MOODS).map(function (key) {
  return <Option key={key} value={key}><img className="moodlist" src={"https://whaleshares.io/emojis/" + MOODS[key] + ".png"}/> {key}</Option>
});

@injectIntl
@requiresLogin
@Form.create()
@withEditor
class Editor extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    form: PropTypes.shape().isRequired,
    title: PropTypes.string,
    topics: PropTypes.arrayOf(PropTypes.string),
    userPods: PropTypes.arrayOf(PropTypes.string),
    post_to: PropTypes.string, // could be public, pod, or friends
    body: PropTypes.string,
    storage: PropTypes.string,
    mood: PropTypes.string,
    loading: PropTypes.bool,
    isUpdating: PropTypes.bool,
    saving: PropTypes.bool,
    draftId: PropTypes.string,
    onUpdate: PropTypes.func,
    onDelete: PropTypes.func,
    onSubmit: PropTypes.func,
    onError: PropTypes.func,
    onImageUpload: PropTypes.func,
    onImageInvalid: PropTypes.func,
  };

  static defaultProps = {
    title: '',
    topics: [],
    body: '',
    storage: '',
    mood: '',
    userPods: [],
    post_to: null,
    recentTopics: [],
    popularTopics: [],
    loading: false,
    isUpdating: false,
    saving: false,
    draftId: null,
    onUpdate: () => {},
    onDelete: () => {},
    onSubmit: () => {},
    onError: () => {},
    onImageUpload: () => {},
    onImageInvalid: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      bodyHTML: '',
      showPredefined: true,
      predefinedTopics: FIXED_TAGS,
    };

    this.onUpdate = this.onUpdate.bind(this);
    this.setValues = this.setValues.bind(this);
    this.setBodyAndRender = this.setBodyAndRender.bind(this);
    this.throttledUpdate = this.throttledUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddTopic = this.handleAddTopic.bind(this);
  }

  componentDidMount() {
    this.setValues(this.props);

    // eslint-disable-next-line react/no-find-dom-node
    const select = ReactDOM.findDOMNode(this.select);
    if (select) {
      const selectInput = select.querySelector('input,textarea,div[contentEditable]');
      if (selectInput) {
        selectInput.setAttribute('autocorrect', 'off');
        selectInput.setAttribute('autocapitalize', 'none');
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { title, topics, body, storage, mood, draftId, post_to } = this.props;
    if (
      title !== nextProps.title ||
      !_.isEqual(topics, nextProps.topics) ||
      body !== nextProps.body ||
      storage !== nextProps.storage ||
      (draftId && nextProps.draftId === null) ||
      post_to !== nextProps.post_to || 
      mood !== nextProps.mood
    ) {
      this.setValues(nextProps);
    }
  }

  onUpdate() {
    _.throttle(this.throttledUpdate, 200, { leading: false, trailing: true })();
  }

  setValues(post) {
    this.props.form.setFieldsValue({
      title: post.title,
      topics: post.topics,
      body: post.body,
      storage: post.storage,
      post_to: post.post_to,
    });

    this.setBodyAndRender(post.body);
  }

  setBodyAndRender(body) {
    this.setState({
      bodyHTML: remarkable.render(improve(body)),
    });
  }

  checkTopics = intl => (rule, value, callback) => {
    if (!value || value.length < 1 || value.length > 5) {
      callback(
        rule.message,
      );
    }

    value
      .map(topic => topic.toLowerCase())
      .map(topic => ({ topic, valid: /^[a-z0-9]+(-[a-z0-9]+)*$/.test(topic) }))
      .filter(topic => !topic.valid)
      .map(topic =>
        callback(
          intl.formatMessage(
            {
              id: 'topics_error_invalid_topic',
              defaultMessage: 'Interest {topic} is invalid.',
            },
            {
              topic: topic.topic,
            },
          ),
        ),
      );
    this.setState({showPredefined: value.length < 5});
    this.setState({predefinedTopics: FIXED_TAGS.filter( topic => !value.includes(topic))});
    callback();
  };

  throttledUpdate() {
    const { form } = this.props;

    const values = form.getFieldsValue();
    this.setBodyAndRender(values.body);

    if (Object.values(form.getFieldsError()).filter(e => e).length > 0) return;

    this.props.onUpdate(values);
  }

  handleSubmit(e) {
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) this.props.onError();
      else this.props.onSubmit(values);
    });
  }

  handleDelete(e) {
    e.stopPropagation();
    e.preventDefault();
    this.props.onDelete();
  }

  handleAddTopic  = e => {
    if ( "none" != e ) {
      const topic   = e;
      let topics  = this.props.form.getFieldValue('topics');
      if (!topics.includes(topic) && topics.length < 5) {
        topics = [...topics, topic];
        this.props.form.setFieldsValue({topics: topics });
        this.setState({showPredefined: (topics.length + 1) < 5});
        this.setState({predefinedTopics: FIXED_TAGS.filter(topic => !topics.includes(topic))});      
      }
    }
  };

  render() {
    const { intl, form, loading, isUpdating, saving, draftId, userPods, storage, mood, user } = this.props;
    const { getFieldDecorator } = form;
    const { bodyHTML, showPredefined, predefinedTopics } = this.state;

    const { words, minutes }    = readingTime(bodyHTML);

    let submitButtonTextId      = '';
    let submitButtonTextDefault = '';

    switch(this.props.form.getFieldValue('post_to')) {
      case 'Choose Audience:':  submitButtonTextId      = 'submit_to_not_set';
                                submitButtonTextDefault = 'Audience not Set';
                                break;
      case '':                  submitButtonTextId      = 'submit_to_public';
                                submitButtonTextDefault = 'Submit to Public';
                                break;
      case '(friends)':         submitButtonTextId      = 'submit_to_friends';
                                submitButtonTextDefault = 'Submit to Friends';  
                                break;
      default:                  submitButtonTextId      = 'submit_to_podmembers';
                                submitButtonTextDefault = 'Submit to Podmembers';
    }

    const stake = Number(user.vesting_shares.replace(' VESTS',''));
    let def_storage = storage;
    if ((def_storage == '') && (stake < 25)) def_storage = 'cnc';

    const ItemLabel = (props) =>{
      return(
        <span className="Editor__label">
          <FormattedMessage id={props.id} defaultMessage={props.defaultMessage} />
        </span>
      );
    }

    return (
      <Form className="Editor" layout="vertical">
        <Helmet>
          <title>{intl.formatMessage({ id: 'write_post', defaultMessage: 'Write post' })}</title>
        </Helmet>
        <Form.Item
          label={<ItemLabel id="title" defaultMessage="Title"/>}
        >
          {getFieldDecorator('title', {
            initialValue: '',
            rules: [
              {
                required: true,
                message: intl.formatMessage({
                  id: 'title_error_empty',
                  defaultMessage: 'Please enter a title.',
                }),
              },
              {
                max: 255,
                message: intl.formatMessage({
                  id: 'title_error_too_long',
                  defaultMessage: "Title can't be longer than 255 characters.",
                }),
              },
            ],
          })(
            <Input
              ref={title => {
                this.title = title;
              }}
              onChange={this.onUpdate}
              className="Editor__title"
              placeholder={intl.formatMessage({
                id: 'title_placeholder',
                defaultMessage: 'Add title',
              })}
            />,
          )}
        </Form.Item>
        <Form.Item
          label={<ItemLabel id="post_content" defaultMessage="Post Content"/>}
        >
          {getFieldDecorator('body', {
            rules: [
              {
                required: true,
                message: intl.formatMessage({
                  id: 'story_error_empty',
                  defaultMessage: "Post content can't be empty.",
                }),
              },
            ],
          })(
            <EditorInput
              minRows={12}
              addon={
                <FormattedMessage
                  id="reading_time"
                  defaultMessage={'{words} words / {min} min read'}
                  values={{
                    words,
                    min: Math.ceil(minutes),
                  }}
                />
              }
              onChange={this.onUpdate}
              onImageUpload={this.props.onImageUpload}
              onImageInvalid={this.props.onImageInvalid}
              inputId={'editor-inputfile'}
            />,
          )}
        </Form.Item>
             <div className="Editor__meta group">
        {!isUpdating && (
          <Form.Item
            label={<ItemLabel id="content_storage" defaultMessage="Post Content Storage"/>}
            extra={
              <span>
                <b>Onchain</b>: default storage on Whaleshares chain.<br/>
                <b>Content Network</b>: on CNC chain.
              </span>
            }
          >
            {getFieldDecorator('storage', {
              initialValue: def_storage
            })(
              <Select onChange={this.onUpdate}>
                <Option value={''}>Onchain</Option>
                <Option value='cnc'>Content Network</Option>
              </Select>,
            )}
          </Form.Item>)
        }
        <Form.Item
          className={classNames('Editor__post_to_wrapper', { Editor__hidden: isUpdating })}
          label={<ItemLabel id="post_to" defaultMessage="Audience"/>}
          extra={
            <span>
              <b>Public</b>: Anyone can comment on this post.<br />
              <b>Friends</b>: Only your friends can comment on this post.<br />
              <b>Pod</b>: Only pod members can comment on this post.
            </span>
          }
        >
          {getFieldDecorator('post_to')(
            <Select onChange={this.onUpdate} disabled={isUpdating}>
              <Option value={''}>Public</Option>
              <Option value={POST_AUDIENCE_FRIENDS}>Friends</Option>
                {userPods.length > 0 &&
                  userPods.map(c => (
                    <Option key={`post_to_pod_${c}`} value={c}>
                          <FormattedMessage
                            id="post_to_pod"
                            defaultMessage="Pod: {pod}"
                            values={{
                              pod: c,
                            }}
                          />
                    </Option>
                ))}
            </Select>,
          )}
          </Form.Item>
          </div>
          {showPredefined && (<div className="Editor__meta group">
            <Form.Item
              className="Editor__topics_wrapper"
              label={<ItemLabel id="predefined_topics_picker" defaultMessage="Predefined Interests Picker"/>}
              extra={intl.formatMessage({
                id: 'predefined_topics_extra',
                defaultMessage:
                  'Easily add RELEVANT Predefined Interests to the Interests Field',
              })}>
              <div>
                <Select
                  showSearch
                  className = "Editor__topics"
                  onChange  = {this.handleAddTopic}
                  value = "none"
                  optionFilterProp = 'children'
                  filterOption = { (input, option) =>
                    option.props.children.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option key="none" value="none">--</Option>
                  {predefinedTopics.map(item => (
                    <Option key={item} value={item}> {item} </Option>
                  ))}
                </Select>
              </div>
            </Form.Item>
          </div>)}          
          <div className="Editor__meta group">
            <Form.Item
              className="Editor__topics_wrapper"
              label={<ItemLabel id="topics" defaultMessage="Add Relevant Interests (max 5)"/>}
              extra={intl.formatMessage({
                id: 'topics_extra',
                defaultMessage:
                  "Separate interests with commas or spaces. Valid characters a..z, 0..9, -",
              })}
            >
              {getFieldDecorator('topics', {
                initialValue: [],
                rules: [
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'topics_error_count',
                      defaultMessage: 'Add 1 to 5 Interests',
                    }),
                    type: 'array',
                  },
                  { validator: this.checkTopics(intl) },
                ],
              })(
                <Select
                  ref={ref => {
                    this.select = ref;
                  }}
                  onChange={this.onUpdate}
                  className="Editor__topics"
                  mode="tags"
                  placeholder={intl.formatMessage({
                    id: 'topics_placeholder',
                    defaultMessage: 'Add Relevant Interest(s)',
                  })}
                  dropdownStyle={{ display: 'none' }}
                  tokenSeparators={[' ', ',']}
                />,
              )}
            </Form.Item>
          </div>
        {!isUpdating && (<div className="Editor__meta">
          <Form.Item
            label={<ItemLabel id="mood" defaultMessage="How are you feeling today?"/>}
            extra={
              <span>
                Express your current mood. (Will display as emoji) 
              </span>
            }
          >
            {getFieldDecorator('mood', {
              initialValue: ''
            })(
              <Select onChange={this.onUpdate}>
                <Option value=''>Not in the mood</Option>
                {moodOptions}
              </Select>,
            )}
          </Form.Item></div>)
        }         
        {bodyHTML && (
          <Form.Item
            label={<ItemLabel id="preview" defaultMessage="Preview"/>}
          >
            <Body full body={bodyHTML} enable_proxify_images={false} />
          </Form.Item>
        )}
        <div className="Editor__bottom">
          <div className="Editor__bottom__right">
            {saving && (
              <span className="Editor__bottom__right__saving">
                <FormattedMessage id="saving" defaultMessage="Saving..." />
              </span>
            )}
            <Form.Item className="Editor__bottom__cancel">
              {draftId && (
                <Button type="danger" size="large" disabled={loading} onClick={this.handleDelete}>
                  <FormattedMessage id="draft_delete" defaultMessage="Discard Draft" />
                </Button>
              )}
            </Form.Item>
            <Form.Item className="Editor__bottom__submit">
              {isUpdating ? (
                <Action
                  primary
                  loading={loading}
                  disabled={loading}
                  text={intl.formatMessage({
                    id: loading ? 'post_send_progress' : 'post_update_send',
                    defaultMessage: loading ? 'Submitting' : 'Update Post',
                  })}
                  onClick={this.handleSubmit}
                />
              ) : (
                <Action
                  primary
                  loading={loading}
                  disabled={loading}
                  text={intl.formatMessage({
                    id: loading ? 'post_send_progress' : submitButtonTextId,
                    defaultMessage: loading ? 'Submitting' : submitButtonTextDefault,
                  })}
                  onClick={this.handleSubmit}
                />
              )}
            </Form.Item>
          </div>
        </div>
      </Form>
    );
  }
}

export default Editor;
