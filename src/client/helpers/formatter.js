export const jsonParse = str => {
  try {
    return jsonParse(JSON.parse(str));
  } catch (e) {
    return str;
  }
};

export const epochToUTC = epochTimestamp => new Date(0).setUTCSeconds(epochTimestamp);


export const truncateDecimal = (num, decimal) => {
  const decimals = 10**decimal;
  let res = num*decimals;
  res = ~~res;
  res /= decimals;
  return res;
};

export default jsonParse;
