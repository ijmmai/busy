import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Avatar from '../Avatar';
import FollowButton from '../../widgets/FollowButton';
import { getPod } from '../../helpers/apiHelpers';
import './User.less';

const UserWithName = ({ username, followBtn = false /* divider = true */ }) => {
  const [podDetails, setPod] = useState({});
  useEffect(() => {
    getPod(username).then(res => {
      setPod(res);
    });
  }, []);
  const isPod = podDetails !== null;
  return (
    <div key={username} className="User">
      <div className="User__row">
        <div className="User__links">
          <Link to={`/@${username}`}>
            <Avatar username={username} size={32} />
          </Link>
          <Link to={`/@${username}`} title={username} className="User__name">
            <span className="username">{username}</span>
          </Link>
        </div>
        {followBtn && (
          <div className="User__follow">
            <FollowButton username={username} secondary />
          </div>
        )}
        {isPod && (
          <React.Fragment>
            <div className="pod-head">
              <div className="pod-item pod-members">
                <FormattedMessage id="members" defaultMessage="Members" />
              </div>
              <div className="pod-item pod-posts">
                <FormattedMessage id="posts" defaultMessage="Posts" />
              </div>
              <div className="pod-item pod-fee">
                <FormattedMessage id="fee" defaultMessage="Fee, WLS" />
              </div>
              <div className="pod-item pod-lastpostedto">
                <FormattedMessage id="lastpostedto" defaultMessage="Last Post" />
              </div>
            </div>
            <div className="pod-details">
              <div className="pod-item pod-members">{podDetails.members}</div>
              <div className="pod-item pod-posts">{podDetails.posts}</div>
              <div className="pod-item pod-fee">{podDetails.fee}</div>
              <div className="pod-item pod-lastpostedto">{podDetails.lastpostedto}</div>
            </div>
          </React.Fragment>
        )}
      </div>
    </div>
  );
}

UserWithName.propTypes = {
  username: PropTypes.string.isRequired,
  followBtn: PropTypes.bool.isRequired,
  /* divider: PropTypes.bool.isRequired, */
};

export default UserWithName;
