import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl';
import { truncateDecimal } from '../../helpers/formatter';
import { Input, InputNumber, Tooltip } from 'antd';
import WLSDisplay from '../Utils/WLSDisplay';
import RawSlider from './RawSlider';
import './Slider.less';


@injectIntl
class Slider extends React.Component {
  static propTypes = {
    maxValue    : PropTypes.number,
    onChange    : PropTypes.func,
    sliderValue : PropTypes.number,
  };

  static defaultProps = {
    maxValue    : 1,
    onChange    : () => {},
    sliderValue : 1,
  };

  state = {
    value: 1,
  };


  onChange = (value) => {
    this.setState({ value }, () => {
      this.props.onChange(value);
    });
  }
  

  componentWillMount() {
    if (this.props.sliderValue) {
      this.setState({
        value: this.props.sliderValue,
      });
    }
  }

  componentDidMount() {
    this.props.onChange(this.state.value);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.sliderValue) {
      this.setState({
        value: nextProps.value,
      });
    }
  }

  handleChange = value => {
    if (typeof value !== 'string' && value <= this.props.maxValue) {
      this.setState({ value }, () => {
       this.props.onChange(value);
      });
    }
  };

  formatTip = value => (
    <WLSDisplay value={value}/>
  );

  render() {
    const { value } = this.state;
    const { maxValue, intl, sliderValue } = this.props;

    return (
      <div className="Slider">
       
        <RawSlider
          initialValue  = {value}
          maxValue      = {maxValue}
          onChange      = {this.handleChange}
          tipFormatter  = {this.formatTip}
        />
        <div className='SliderInfoContainer'>
          <div className="Slider__inputNumber">
            <InputNumber 
              autoFocus     = {true}
              defaultValue  = {sliderValue} 
              min           = {0.001} 
              onChange      = {this.handleChange}
              onFocus       = {(e) => e.target.select()}
              step          = {1}
              value         = {value}
            />
          </div>
          <div className="Slider__info">
            <FormattedMessage
              id              = "like_slider_info"
              defaultMessage  = "Your tip will be worth {amount} ({maxValue})."
              values          = {{
                                  amount: (
                                    <span className="Slider__info__amount">{`${truncateDecimal(value, 3).toFixed(3)} WLS`}</span>
                                  ),
                                  maxValue: (
                                    <span className="Slider__info__maxAmount">{`max ${truncateDecimal(this.props.maxValue, 3).toFixed(3)} WLS`}</span>
                                  ),                
                                }}
            />
          </div>
        </div>
        <Input.TextArea
          autosize    = {{minRows: 2, maxRows: 6}}
          maxLength   = {140}
          onChange    = {this.props.handleMemoChange}
          placeholder = {intl.formatMessage({
                          id: 'memo_placeholder',
                          defaultMessage: 'Feel free to add a Memo (memos are public).\nStart with # for Encrypted Memo',
                        })}
        />
      </div>
    );
  }
}

export default Slider;
