import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { injectIntl } from 'react-intl';
import { Icon } from 'antd';
import './Follow.less';

export class FollowPure extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    isFollowed: PropTypes.bool,
    pending: PropTypes.bool,
    onClick: PropTypes.func,
    secondary: PropTypes.bool,
    isPod: PropTypes.bool,
    isJoined: PropTypes.bool,
  };

  static defaultProps = {
    isFollowed: false,
    pending: false,
    secondary: false,
    isPod: false,
    isJoined: false,
    onClick: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isHovered: false,
    };
  }

  onMouseOver = () => this.setState({ isHovered: true });

  onMouseOut = () => this.setState({ isHovered: false });

  handleClick = e => {
    e.preventDefault();
    if (this.props.pending) return;
    this.props.onClick(e);
  };

  render() {
    const { intl, isFollowed, pending, secondary, isPod, isJoined } = this.props;
    const { isHovered } = this.state;
    const isDangerStyles = isFollowed && (isHovered || pending);

    let followingText = intl.formatMessage({ id: 'follow', defaultMessage: 'Follow' });
    if (isFollowed && !(isHovered || pending)) {
      followingText = intl.formatMessage({ id: 'followed', defaultMessage: 'Following' });
    } else if (isFollowed && isHovered && !pending) {
      followingText = intl.formatMessage({ id: 'unfollow', defaultMessage: 'Unfollow' });
    } else if (isFollowed && pending) {
      followingText = intl.formatMessage({ id: 'unfollowing', defaultMessage: 'Unfollowing' });
    } else if (!isFollowed && isHovered && !pending) {
      followingText = intl.formatMessage({ id: 'follow', defaultMessage: 'Follow' });
    } else if (!isFollowed && pending) {
      followingText = intl.formatMessage({ id: 'followed', defaultMessage: 'Following' });
    }

    let joiningText = intl.formatMessage({ id: 'join', defaultMessage: 'Join' });

    if (isJoined && !(isHovered || pending)) {
      joiningText = intl.formatMessage({ id: 'joined', defaultMessage: 'Joined' });
    } else if (isJoined && isHovered && !pending) {
      joiningText = intl.formatMessage({ id: 'leave', defaultMessage: 'Leave' });
    } else if (isJoined && pending) {
      joiningText = intl.formatMessage({ id: 'leavin', defaultMessage: 'Leaving' });
    } else if (!isJoined && isHovered && !pending) {
      joiningText = intl.formatMessage({ id: 'join', defaultMessage: 'Join' });
    } else if (!isJoined && pending) {
      joiningText = intl.formatMessage({ id: 'joined', defaultMessage: 'Joined' });
    }

    return (
      <button
        className={classNames('Follow', {
          'Follow--danger': isDangerStyles,
          'Follow--danger--secondary': isDangerStyles && secondary,
          'Follow--secondary': secondary,
        })}
        onClick={this.handleClick}
        onMouseOver={this.onMouseOver}
        onMouseOut={this.onMouseOut}
      >
        {pending && <Icon type="loading" />}
        {isPod ? joiningText : followingText}
      </button>
    );
  }
}

export default injectIntl(FollowPure);
