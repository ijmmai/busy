import React from 'react';

const SvgIconPosting = props => (
  <svg id="icon-posting_svg__Layer_12" data-name="Layer 12" viewBox="0 0 960 959.94" {...props}>
    <defs>
      <style>{'.icon-posting_svg__cls-1{fill:#b9bff2}'}</style>
    </defs>
    <path
      className="icon-posting_svg__cls-1"
      d="M407.24 990.648H289.7c0-140.4-114.24-254.64-254.7-254.64v-117.54c205.26-.06 372.24 166.92 372.24 372.18zM995 990.648H877.52c0-464.46-377.94-842.4-842.52-842.4V30.708c529.32 0 960 430.62 960 959.94z"
      transform="translate(-35 -30.708)"
    />
    <path
      className="icon-posting_svg__cls-1"
      d="M701.12 990.648H583.58c0-302.4-246.06-548.46-548.52-548.46v-117.54c367.26 0 666.06 298.74 666.06 666zM35 990.648v-140.04a140.042 140.042 0 0 1 140.04 140.04z"
      transform="translate(-35 -30.708)"
    />
  </svg>
);

export default SvgIconPosting;
