import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { getFeedContent } from './feedActions';
import { getIsAuthenticated } from '../reducers';
import SubFeed from './SubFeed';
import FeedMenu from './FeedMenu';
import HeroBannerContainer from './HeroBannerContainer';
import SidenavMain from '../components/Navigation/SidenavMain';
import RightSidebar from '../app/Sidebar/RightSidebar';
import Affix from '../components/Utils/Affix';
import ScrollToTop from '../components/Utils/ScrollToTop';
import ScrollToTopOnMount from '../components/Utils/ScrollToTopOnMount';
import QuickPostEditor from '../components/QuickPostEditor/QuickPostEditor';
import Promo from '../components/Promo/Promo';

@connect(state => ({
  authenticated: getIsAuthenticated(state),
}))
class Page extends React.Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    history: PropTypes.shape().isRequired,
    location: PropTypes.shape().isRequired,
    match: PropTypes.shape().isRequired,
  };

  static fetchData({ store, match }) {
    const { sortBy, category } = match.params;
    return store.dispatch(getFeedContent({ sortBy, category, limit: 10 }));
  }

  componentDidMount() {
    try {
      const auth = JSON.parse(localStorage.getItem('auth'));
      const { location } = this.props;
      if (location.pathname === '/') {
        if (auth) this.props.history.push('/myfeed');
        else this.props.history.push('/created');
      }
    } catch (e) {
      this.props.history.push('/created');
    }
  }

  handleSortChange = key => {
    const { category } = this.props.match.params;
    if (category && key !== 'myfeed') {
      this.props.history.push(`/${key}/${category}`);
    } else {
      this.props.history.push(`/${key}`);
    }
  };

  handleTopicClose = () => this.props.history.push('/created');

  render() {
    const { authenticated, location, match } = this.props;
    const { category, sortBy } = match.params;

    const title = 'Whaleshares';
    const desc = 'Whaleshares is a decentralized social network.';
    const image = 'https://whaleshares.io/images/wls-share.png';
    const robots = 'index,follow';

    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta property="description" content={desc} />
          <meta property="og:title" content={title} />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://whaleshares.io" />
          <meta property="og:image" content={image} />
          <meta property="og:description" content={desc} />
          <meta property="og:site_name" content={title} />
          <meta property="twitter:card" content={image} />
          <meta property="twitter:site" content={'@Whaleshares_io'} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={desc} />
          <meta property="twitter:image" content={image} />
          <meta name="robots" content={robots} />
        </Helmet>
        <ScrollToTop />
        <ScrollToTopOnMount />
        <div className="shifted">
          <div className="layout-twocol container">
            <Affix className="leftContainer" stickPosition={77}>
              <div className="left">
                <SidenavMain />
              </div>
            </Affix>
            {/*<Affix className="rightContainer" stickPosition={77}>*/}
              {/*<div className="right">*/}
                {/*<RightSidebar />*/}
              {/*</div>*/}
            {/*</Affix>*/}
            <div className="center main-body">
              <div className="right-sidebar-layout">
                <RightSidebar />
                <div className="main-feed">
                  <FeedMenu
                    authenticated={authenticated}
                    onChange={this.handleSortChange}
                    defaultKey={sortBy}
                    topic={category}
                    onTopicClose={this.handleTopicClose}
                  />
                  {authenticated && <QuickPostEditor />}
                  {/*location.pathname === '/created' && */<Promo type="feed" />}
                  <SubFeed />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Page;
