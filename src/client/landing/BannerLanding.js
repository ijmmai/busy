import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, Carousel } from 'antd';
import './BannerLanding.less';
import SignupButton from '../components/SignupButton.js';

const BannerLanding = () => (
  <div className="CarouselLanding">
    <div className="container">
      <div className="slider-cta">
      <h1>Welcome to Whaleshares, a Decentralized Social Society</h1>
        <SignupButton text="SIGN UP NOW!" />
      </div>
    </div>
    <Carousel className="slider" autoplay>
      <div className="slide one" />
      <div className="slide two" />
      <div className="slide three" />
      <div className="slide four" />
    </Carousel>
  </div>
);

export default BannerLanding;
