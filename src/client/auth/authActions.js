/* eslint-disable no-lonely-if no-undef */

import { createAction } from 'redux-actions';
import { push } from 'react-router-redux';
import { getIsAuthenticated } from '../reducers';
import { createAsyncActionType } from '../helpers/stateHelpers';
import { getMetadata, setNotifications } from '../helpers/localStorageHelpers';
import { getFollowing } from '../user/userActions';
import { notify } from '../app/Notification/notificationActions';

export const LOCK = '@auth/LOCK';
export const UNLOCK = createAsyncActionType('@auth/UNLOCK');
export const OPEN_MODAL = '@auth/OPEN_MODAL';
export const CLOSE_MODAL = '@auth/CLOSE_MODAL';

export const LOGIN = '@auth/LOGIN';
export const LOGIN_START = '@auth/LOGIN_START';
export const LOGIN_SUCCESS = '@auth/LOGIN_SUCCESS';
export const LOGIN_ERROR = '@auth/LOGIN_ERROR';

export const RELOAD = '@auth/RELOAD';
export const RELOAD_START = '@auth/RELOAD_START';
export const RELOAD_SUCCESS = '@auth/RELOAD_SUCCESS';
export const RELOAD_ERROR = '@auth/RELOAD_ERROR';

export const LOGOUT = '@auth/LOGOUT';

export const BUSY_LOGIN = createAsyncActionType('@auth/BUSY_LOGIN');

export const SAVE_NOTIFICATION_MARK = createAsyncActionType('@auth/SAVE_NOTIFICATION_MARK');

const loginError = createAction(LOGIN_ERROR);

export const openLoginModal = () => dispatch => {
  dispatch({
    type: OPEN_MODAL,
  });
};

export const closeLoginModal = () => dispatch => {
  dispatch({
    type: CLOSE_MODAL,
  });
};

export const unlock = password => (dispatch, getState, { Keys }) => {
  const works = Keys.setPassword(password).testPassword();
  if (works) {
    dispatch({
      type: UNLOCK.ACTION,
      payload: {
        promise: Promise.resolve(),
      },
    });
    return Promise.resolve();
  }
  Keys.lock();
  dispatch({
    type: UNLOCK.ACTION,
    payload: {
      promise: Promise.reject(),
    },
  });
  return Promise.reject();
};

export const lock = () => (dispatch, getState, { Keys }) => {
  Keys.lock();
  dispatch({
    type: LOCK,
  });
};

export const login = () => (dispatch, getState, { steemAPI, busyAPI, Keys }) => {
  let promise = Promise.resolve(null);

  if (typeof window !== 'undefined') {
    if (window.whalevault != null) steemAPI.chainLib.config.set('wv_obj', window.whalevault);
  }

  if (getIsAuthenticated(getState())) {
    promise = Promise.resolve(null);
  } else {
    try {
      if (Keys.decrypt() === false) throw new Error();
      const { username, hasPass, hasWhaleVault } = Keys.decrypt();

      if (hasWhaleVault && steemAPI.chainLib.config.get('wv_obj'))
        steemAPI.chainLib.config.set('whalevault', steemAPI.chainLib.config.get('wv_obj'));

      promise = Promise.all([
        busyAPI
          .sendAsync('database_api', 'get_accounts', [[username]])
          .then(apiRes => ({
            account: apiRes[0],
            user_metadata: getMetadata(),
            hasPass,
          }))
          .catch(() => dispatch(loginError())),
        busyAPI
          .sendAsync('database_api', 'get_pods', [[username]])
          .then(apiRes => ({
            pod: apiRes[0]
          }))
      ]).then(([a, b]) => ({...a, ...b}));
    } catch (e) {
      promise = Promise.reject(new Error('There is no loggedin present'));
    }
  }

  return dispatch({
    type: LOGIN,
    payload: {
      promise,
    },
    meta: {
      refresh: false,
      locked: true,
    },
  }).catch(() => dispatch(loginError()));
};

export const loginWithPostingKey = (username, postingkey, password = '') => async (
  dispatch,
  getState,
  { steemAPI, busyAPI, Keys },
) => {
  let account = null; //
  let postingKey = postingkey;
  let pod = null;

  // validating if key is correct
  try {
    account = await busyAPI.sendAsync('database_api', 'get_accounts', [[username]]).then(apiRes => apiRes[0]);
    if (!account || !account.posting) throw new Error('Unrecognized Account! ['+username+']');
    pod = await busyAPI.sendAsync('database_api', 'get_pods', [[username]]).then(apiRes => apiRes[0]);
    steemAPI.chainLib.config.set('whalevault', null);
    if (typeof window !== 'undefined') {
      if (window.whalevault != null) steemAPI.chainLib.config.set('wv_obj', window.whalevault);
    }

    const pubWif = account.posting.key_auths[0][0];
    if (postingKey === '' && steemAPI.chainLib.config.get('wv_obj')) {
      const wvPubkeys = await steemAPI.chainLib.config
        .get('wv_obj')
        .promiseRequestPubKeys('wls_busy', `wls:${username}`);
      try {
        if (wvPubkeys.result[`wls:${username}`].postingPubkey !== pubWif) throw new Error('WhaleVault key mismatch');
        postingKey = `${username}:`;
        steemAPI.chainLib.config.set('whalevault', steemAPI.chainLib.config.get('wv_obj'));
      } catch (e) {
        throw new Error('WhaleVault key not found');
      }
    } else {
      // eslint-disable-next-line no-lonely-if
      if (!steemAPI.chainLib.auth.isWif(postingKey) || !steemAPI.chainLib.auth.wifIsValid(postingKey, pubWif)) {
        throw new Error('Invalid key. Please use your Social Key.');
      }
    }
  } catch (e) {
    dispatch(notify(e.message, 'error'));
    return;
  }

  // dispatch login OK
  dispatch({
    type: LOGIN_SUCCESS,
    meta: {
      refresh: false,
      locked: false,
    },
    payload: {
      account,
      user_metadata: getMetadata(),
      hasPass: password !== '',
      pod,
    },
  });

  Keys.setPassword(password).encrypt(username, { postingKey });

  // redirect to feed page
  dispatch(push('/myfeed'));
};

export const getCurrentUserFollowing = () => dispatch => dispatch(getFollowing());

export const reload = () => async (dispatch, getState, { busyAPI }) => {
  const { auth } = getState();
  const account = await busyAPI.sendAsync('database_api', 'get_accounts', [[auth.user.name]]).then(apiRes => apiRes[0]);
  const pod = await busyAPI.sendAsync('database_api', 'get_pods', [[auth.user.name]]).then(apiRes => apiRes[0]);
  dispatch({
    type: RELOAD,
    payload: {
      promise: Promise.resolve({
        account,
        user_metadata: getMetadata(),
        pod,
      }),
    },
  });
};

export const logout = () => (dispatch, getState, { Keys }) => {
  Keys.remove();
  dispatch({type: LOGOUT,});
  dispatch(push('/login'));
  location.reload();
};

export const saveNotificationsMark = (user, tm, last_index) => dispatch => {
  dispatch({
    type: SAVE_NOTIFICATION_MARK.ACTION,
    payload: {
      promise: setNotifications(user, tm, last_index),
    },
  });
};
