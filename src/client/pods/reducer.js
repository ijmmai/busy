import produce from 'immer';
import * as actions from './actions';

const initialState = {
  tag: '',            // filtered by tag, empty means all tags
  pods: false,        // list of pod for podlist browsing
  hasMore: false,
  loadingMore: false,
};

const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.SET:
        draft[action.payload.key] = action.payload.value;
        break;
      default:
        break;
    }
  });

export default reducer;
