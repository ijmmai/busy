import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Input, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';
import { closeLoginModal, unlock } from '../auth/authActions';
import {
  getShowLoginModal
} from '../reducers';
import './LoginModal.less';

@connect(
  state => ({
    visible: getShowLoginModal(state),
  }),
  {
    handleLoginModalCancel: closeLoginModal,
    unlock
  }
)
class LoginModalWithPassword extends React.Component {
  static propTypes = {
    handleLoginModalCancel: PropTypes.func.isRequired,
    unlock: PropTypes.func.isRequired,
    visible: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    handleLoginModalCancel: () => {},
    unlock: () => {},
    visible: false,
  };

  state = {
    pin: '',
    error: false,
  };

  updatePin = (e) => {
    e.preventDefault();
    this.setState({
      pin: e.target.value
    });
  }

  handleSubmit = () => {
    this.props.unlock(this.state.pin)
      .then(() => {
        this.setState({
          pin: '',
          error: false,
        });
        this.props.handleLoginModalCancel();
      })
      .catch(() => {
        this.setState({
          error: true,
        })
      });
  }

  render() {
    const { handleLoginModalCancel, visible } = this.props;
    return (
      <Modal
        title=""
        visible={visible}
        onCancel={handleLoginModalCancel}
        onOk={this.handleSubmit}
      >
        <div className="LoginModal__body">
          <Icon type="key" className="iconfont icon-busy LoginModal__icon" />
          <span className="LoginModal__login-title">
            <FormattedMessage id="pin" defaultMessage="Enter Your Pin" />
          </span>
          {
            this.state.error && <span className="LoginModal__login-error">
              <FormattedMessage id="error_wrong_pin" defaultMessage="Wrong Pin" />
            </span>
          }
          <Input
            placeholder="Pin"
            onChange={this.updatePin}
            value={this.state.pin}
          />
        </div>
      </Modal>
    );
  }
}

export default LoginModalWithPassword;
