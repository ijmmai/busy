const appsList = {
  wls: {
    name: 'Whaleshares',
    homepage: 'https://whaleshares.io',
    url_scheme: 'https://whaleshares.io/{category}/@{username}/{permlink}',
  },
  'wls-v1': {
    name: 'Whaleshares v1',
    homepage: 'https://v1.whaleshares.io',
    url_scheme: 'https://v1.whaleshares.io/{category}/@{username}/{permlink}',
  },
  'wls-v2': {
    name: 'Whaleshares v2',
    homepage: 'https://whaleshares.io',
    url_scheme: 'https://whaleshares.io/{category}/@{username}/{permlink}',
  },
};

const apps = {};

Object.keys(appsList).forEach(key => {
  apps[key] = appsList[key].name;
});

export default apps;
