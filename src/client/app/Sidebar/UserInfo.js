import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl';
import _ from 'lodash';
import { getUser } from '../../reducers';
import SocialLinks from '../../components/SocialLinks';
import { urlWithoutWWW } from '../../helpers/regexHelpers';

@injectIntl
@connect((state, ownProps) => ({
  user: getUser(state, ownProps.match.params.name),
}))
class UserInfo extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
  };

  render() {
    const { intl, user } = this.props;
    const location = user && _.get(user.json_metadata, 'profile.location');
    const profile = (user && _.get(user.json_metadata, 'profile')) || {};
    const website = user && _.get(user.json_metadata, 'profile.website');
    const host = urlWithoutWWW(website);

    return (
      <div>
        {user.name && (
          <div style={{ wordBreak: 'break-word' }}>
            <div style={{ fontSize: '18px' }}>
              {_.get(user && user.json_metadata, 'profile.about')}
            </div>
            <div style={{ marginTop: 16, marginBottom: 16 }}>
              {location && (
                <div>
                  <i className="iconfont icon-coordinates text-icon" />
                  {location}
                </div>
              )}
              {website && (
                <div>
                  <i className="iconfont icon-link text-icon" />
                  <a target="_blank" rel="noopener noreferrer" href={website}>
                    {host}
                  </a>
                </div>
              )}
              <div>
                <i className="iconfont icon-time text-icon" />
                <FormattedMessage
                  id="joined_date"
                  defaultMessage="Joined {date}"
                  values={{
                    date: intl.formatDate(user.created, {
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric',
                    }),
                  }}
                />
              </div>
              <SocialLinks profile={profile} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default UserInfo;
