import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { Icon, Button } from 'antd';
import faCoins from '../../../assets/images/svgs/faCoins.svg';
import formatter from '../helpers/steemitFormatter';
import Loading from '../components/Icon/Loading';
import IconWlsCoin from '../../../assets/images/svgs/IconWlsCoin';
import Promo from '../components/Promo/Promo';
import { Link } from 'react-router-dom';
import copy from 'copy-to-clipboard';
import BTooltip from '../components/BTooltip'
import './UserWalletSummary.less';

const UserWalletSummary = ({
  user,
  loading,
  totalVestingShares,
  totalVestingFundSteem,
  loadingGlobalProperties,
  currentDailyBandwith,
  dailyBandwidthLimit,
  dailyBandwidthPercent,
  referralLink = 'https://sharebits.io/wls_signup.html?r='+user.name,
}) => (

<div className="UserWalletSummary">
  <Promo type="top" />
  <div className="main-content">
    <div className="UserWalletSummary__item">
      <div className="UserWalletSummary__item_top">
        <Icon
          type="thunderbolt"
          theme="twoTone"
          twoToneColor="#2b78d6"
          className="iconfont"
          style={{ marginRight: 10 }}
        />
        <div className="UserWalletSummary__label">
          <FormattedMessage id="whalestake" defaultMessage="Whalestake" />
        </div>
        <div className="UserWalletSummary__value">
          {loading || loadingGlobalProperties ? (
            <Loading />
          ) : (
            <span>
              <FormattedNumber
                value={parseFloat(
                  formatter.vestToSteem(user.vesting_shares, totalVestingShares, totalVestingFundSteem),
                )}
              />{' '}
              {/* WS */}
            </span>
          )}
        </div>
      </div>
      <div className="UserWalletSummary__item_desc">
        Influence tokens which allow you to earn more daily rewards and give you more bandwidth.
      </div>
    </div>
    <div className="UserWalletSummary__item">
      <div className="UserWalletSummary__item_top">
        <Icon component={IconWlsCoin} className="iconfont" style={{ marginRight: 10 }} />
        <div className="UserWalletSummary__label">
          <FormattedMessage id="wls" defaultMessage="WLS" />
        </div>
        <div className="UserWalletSummary__value">
          {loading ? (
            <Loading />
          ) : (
            <span>
              <FormattedNumber value={parseFloat(user.balance)} /> {/* WLS */}
            </span>
          )}
        </div>
      </div>
      <div className="UserWalletSummary__item_desc">
        Tradeable tokens that may be transferred anywhere at anytime. WLS can be converted to WHALESTAKE in a process
        called powering up.
      </div>
    </div>
    <div className="UserWalletSummary__item">
      <div className="UserWalletSummary__item_top">
        <Icon component={faCoins} className="iconfont" style={{ marginRight: 10, color:"#2b78d6" }}/>
        <div className="UserWalletSummary__label">
          Tip Balance
        </div>
        <div className="UserWalletSummary__value">
          <FormattedNumber value={parseFloat(user.reward_steem_balance)} />
        </div>
      </div>
      <div className="UserWalletSummary__item_desc">The amount of Whalestake you have put aside to tip others.</div>
    </div>
    <div className="UserWalletSummary__item">
      <div className="UserWalletSummary__item_top">
        <Icon
          type="experiment"
          theme="twoTone"
          twoToneColor="#2b78d6"
          className="iconfont"
          style={{ marginRight: 10 }}
        />
        <div className="UserWalletSummary__label">
          <FormattedMessage id="DaBa" defaultMessage="DaBa" />
        </div>
        <div className="UserWalletSummary__value">
          {dailyBandwidthPercent}% <span className="secondary-text">({currentDailyBandwith} / {dailyBandwidthLimit})</span>
        </div>
      </div>
      <div className="UserWalletSummary__item_desc">Daily Bandwidth (in bytes).</div>
    </div>
  </div>
  <div className="main-content">
    <div className="UserWalletSummary__item">
      <div className="UserWalletSummary__item_top">
        <Icon
          type="usergroup-add"
          className="iconfont"
          style={{ marginRight: 10, color: "#2b78d6"}}
        />
        <div className="UserWalletSummary__label">
          <FormattedMessage id="your_referral_link" defaultMessage="Referral Link" />
        </div>
        <div className="UserWalletSummary__value">
          <span className="secondary-text">
          <BTooltip title={referralLink}>
            <Button type='primary' value={referralLink} onClick={onClick}>
              Copy Referral Link to Clipboard
            </Button>
          </BTooltip>
          </span>
        </div>
      </div>
      <div className="UserWalletSummary__item_desc">
        <p>Use your referral link to invite your friends to join Whaleshares.io, instantly.</p>
        <p>Read <Link to='/@whaleshares/3z5wde-instant-signup-now-available-for-whaleshares-via-twitter'>this post</Link> to learn about the details and how you can get rewarded yourself.</p>
      </div>
    </div>
  </div>
</div>
);

UserWalletSummary.propTypes = {
  loadingGlobalProperties: PropTypes.bool.isRequired,
  user: PropTypes.shape().isRequired,
  totalVestingShares: PropTypes.string.isRequired,
  totalVestingFundSteem: PropTypes.string.isRequired,
  currentDailyBandwith: PropTypes.number.isRequired,
  dailyBandwidthLimit: PropTypes.number.isRequired,
  dailyBandwidthPercent: PropTypes.number.isRequired,
  loading: PropTypes.bool,
};

UserWalletSummary.defaultProps = {
  loading: false, 
};

const onClick = (e) => {
  copy(e.target.value);
}

export default UserWalletSummary;
