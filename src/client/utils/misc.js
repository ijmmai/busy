// eslint-disable-next-line import/prefer-default-export
export const sleep = (millis) => {
  return new Promise(resolve => setTimeout(resolve, millis));
};

export const getWhalevaultLink = () => {
  let wvLink = 'https://chrome.google.com/webstore/detail/hcoigoaekhfajcoingnngmfjdidhmdon/';
  if (typeof window !== 'undefined') {
    const isFireFox = window.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (isFireFox) wvLink = 'https://addons.mozilla.org/en-US/firefox/addon/whalevault/';
  }

  return wvLink;
};

export const ALPHABET = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
