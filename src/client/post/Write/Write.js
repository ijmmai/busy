import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
import _ from 'lodash';
import 'url-search-params-polyfill';
import { injectIntl, FormattedMessage } from 'react-intl';
import uuidv4 from 'uuid/v4';
import { getHtml } from '../../components/Story/Body';
import improve from '../../helpers/improve';
import Feed from '../../feed/Feed';
import { extractImageTags, extractLinks } from '../../helpers/parser';
import LastDraftsContainer from './LastDraftsContainer';
import DeleteDraftModal from './DeleteDraftModal';
import {
  getAuthenticatedUser,
  getDraftPosts,
  getIsEditorLoading,
  getIsEditorSaving,
  getComments as getCommentsReducer,
  // getPods,
} from '../../reducers';
import { getComments } from '../../comments/commentsActions';
import { createPost, saveDraft, newPost } from './editorActions';
import { getContent } from '../postActions';
import Editor from '../../components/Editor/Editor';
import Affix from '../../components/Utils/Affix';
import SidenavMain from '../../components/Navigation/SidenavMain';
import withAuthActions from '../../auth/withAuthActions';
import requiresLogin from '../../auth/requiresLogin';
import { POST_AUDIENCE_FRIENDS } from '../../helpers/postHelpers';
import { jsonParse } from '../../helpers/formatter';
import './Write.less';

const version = require('../../../../package.json').version;

@requiresLogin
@injectIntl
@withRouter
@connect(
  (state, props) => ({
    user: getAuthenticatedUser(state),
    draftPosts: getDraftPosts(state),
    loading: getIsEditorLoading(state),
    saving: getIsEditorSaving(state),
    draftId: new URLSearchParams(props.location.search).get('draft'),
    comments: getCommentsReducer(state),
    userPods: state.user.pods,
  }),
  {
    createPost,
    saveDraft,
    newPost,
    replace,
    getComments,
    getContent,
  },
)
@withAuthActions
class Write extends React.Component {
  static propTypes = {
    user: PropTypes.shape().isRequired,
    draftPosts: PropTypes.shape().isRequired,
    loading: PropTypes.bool.isRequired,
    intl: PropTypes.shape().isRequired,
    comments: PropTypes.shape(),
    userPods: PropTypes.arrayOf(PropTypes.shape()),
    saving: PropTypes.bool,
    draftId: PropTypes.string,
    newPost: PropTypes.func,
    createPost: PropTypes.func,
    saveDraft: PropTypes.func,
    replace: PropTypes.func,
    onActionInitiated: PropTypes.func,
    getComments: PropTypes.func,
    getContent: PropTypes.func,
  };

  static defaultProps = {
    saving: false,
    draftId: null,
    comments: {},
    userPods: [],
    newPost: () => {},
    createPost: () => {},
    saveDraft: () => {},
    notify: () => {},
    replace: () => {},
    onActionInitiated: () => {},
    getComments: () => {},
    getContent: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      initialTitle: '',
      initialTopics: [],
      initialBody: '',
      initialStorage: '',
      initialUpdatedDate: Date.now(),
      isUpdating: false,
      showModalDelete: false,
      loadingComments: true,
      post_to: 'Choose Audience:',
    };
  }

  componentDidMount() {
    this.props.newPost();
    const { draftPosts, draftId } = this.props;
    const draftPost = draftPosts[draftId];

    if (draftPost) {
      this.setDraft(draftPost);
    }

    if (draftId) {
      this.draftId = draftId;
    } else {
      this.draftId = uuidv4();
    }
  }

  componentWillReceiveProps(nextProps) {
    const newDraft = nextProps.draftId === null;
    const differentDraft = this.props.draftId !== nextProps.draftId;
    const moreDrafts = this.props.draftPosts !== nextProps.draftPosts;
    if (differentDraft && newDraft) {
      this.draftId = uuidv4();
      this.setState({
        initialTitle: '',
        initialTopics: [],
        initialBody: '',
        initialStorage: '',
        initialUpdatedDate: Date.now(),
        isUpdating: false,
        showModalDelete: false,
        post_to: POST_AUDIENCE_FRIENDS,
      });
    } else if (differentDraft) {
      const { draftPosts, draftId } = nextProps;
      const draftPost = _.get(draftPosts, draftId, {});
      const initialTitle = _.get(draftPost, 'title', '');
      const initialBody = _.get(draftPost, 'body', '');
      const initialStorage = _.get(draftPost, 'jsonMetadata.storage', '');
      const initialTopics = _.get(draftPost, 'jsonMetadata.tags', []);
      this.draftId = draftId;
      this.setState({
        initialTitle,
        initialBody,
        initialStorage,
        initialTopics,
      });
    } else if (moreDrafts) {
      const { draftPosts, draftId } = nextProps;
      const draftPost = draftPosts[draftId];
      if (draftPost) {
        this.setDraft(draftPost);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (_.get(this.props, 'location.search') !== _.get(prevProps, 'location.search')) {
      this.saveDraft.cancel();
    }
  }

  onDeleteDraft = () => this.props.replace('/editor');

  onDelete = () => this.setState({ showModalDelete: true });

  onSubmit = form => {
    const data = this.getNewPostData(form);
    data.body = improve(data.body);

    if (this.props.draftId) {
      data.draftId = this.props.draftId;
    }

    this.props.onActionInitiated(() => {
      this.props.createPost(data, {
        '10': this.props.intl.formatMessage({
          id: 'error_posting_too_often',
          defaultMessage: 'Error: You are either posting too often, or have not selected an audience.',
        }),
      });
    });
  };

  setDraft = draftPost => {
    let tags = [];
    if (_.isObject(draftPost.jsonMetadata) && _.isArray(draftPost.jsonMetadata.tags)) {
      tags = draftPost.jsonMetadata.tags;
    }

    if (draftPost.id) {
      this.permlink = draftPost.permlink;
      this.props
        .getContent(draftPost.author, draftPost.permlink, false)
        .then(() => this.props.getComments(draftPost.id));
    }

    if (draftPost.originalBody) {
      this.originalBody = draftPost.originalBody;
    }

    const storage = _.get(draftPost, 'jsonMetadata.storage', '');

    // eslint-disable-next-line
    this.setState({
      initialTitle: draftPost.title || '',
      initialTopics: tags || [],
      initialBody: draftPost.body || '',
      initialStorage: storage || '',
      initialUpdatedDate: draftPost.lastUpdated || Date.now(),
      isUpdating: draftPost.isUpdating || false,
      post_to: draftPost.post_to || '',
    });
  };

  getNewPostData = form => {
    const data = {
      body: form.body,
      storage: form.storage,
      title: form.title,
      upvote: false,
      lastUpdated: Date.now(),
      post_to: form.post_to,
      mood: form.mood,
    };

    data.parentAuthor = '';
    data.author = this.props.user.name || '';

    const tags = form.topics;
    const users = [];
    const userRegex = /@([a-zA-Z.0-9-]+)/g;
    let matches;

    const postBody = data.body;

    // eslint-disable-next-line
    while ((matches = userRegex.exec(postBody))) {
      if (users.indexOf(matches[1]) === -1) {
        if (matches[1] !== data.author) users.push(matches[1]);
      }
    }

    const parsedBody = getHtml(false,null, null, postBody, {}, 'text');

    const images = _.map(extractImageTags(parsedBody), tag => tag.src);
    const links = extractLinks(parsedBody).filter(link => !link.startsWith('/@'));

    if (data.title && !this.permlink) {
      data.permlink = _.kebabCase(data.title);
    } else {
      data.permlink = this.permlink;
    }

    if (this.state.isUpdating) data.isUpdating = this.state.isUpdating;

    let metaData = {
      app: `wls/${version}`,
      format: 'markdown',
    };

    // Merging jsonMetadata makes sure that users don't lose any metadata when they edit post using
    // Busy (like video data from DTube)
    if (this.props.draftPosts[this.draftId] && this.props.draftPosts[this.draftId].jsonMetadata) {
      metaData = {
        ...this.props.draftPosts[this.draftId].jsonMetadata,
        ...metaData,
      };
    }

    if (tags.length) {
      metaData.tags = tags;
    }
    if (users.length) {
      metaData.users = users;
    } else delete metaData.users;
    if (links.length) {
      metaData.links = links.slice(0, 10);
    } else delete metaData.links;
    if (images.length) {
      metaData.image = images;
    } else delete metaData.image;
    if (data.storage) {
      metaData.storage = data.storage;
    } else delete metaData.storage;
    if (data.mood) {
      metaData.mood = data.mood;
    } else delete metaData.mood;

    data.parentPermlink = tags.length ? tags[0] : 'general';
    data.jsonMetadata = metaData;

    if (this.originalBody) {
      data.originalBody = this.originalBody;
    }

    return data;
  };

  handleCancelDeleteDraft = () => this.setState({ showModalDelete: false });

  saveDraft = _.debounce(form => {
    if (this.props.saving) return;

    const data = this.getNewPostData(form);
    const postBody = data.body;
    const id = this.props.draftId;
    // Remove zero width space
    const isBodyEmpty = postBody.replace(/[\u200B-\u200D\uFEFF]/g, '').trim().length === 0;

    if (isBodyEmpty) return;

    const redirect = id !== this.draftId;

    this.props.saveDraft({ postData: data, id: this.draftId }, redirect, this.props.intl);
  }, 2000);

  render() {
    const { initialTitle, initialTopics, initialBody, initialStorage, post_to } = this.state;
    const { loading, saving, draftId, comments, draftPosts, userPods } = this.props;
    let commentList = comments.childrenById[draftId];

    const pods = userPods.map(c => c.pod);

    if (!commentList) {
      commentList = [];
    }

    return (
      <div className="shifted">
        <div className="feed-layout container">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center_full">
            <Editor
              ref={this.setForm}
              saving={saving}
              title={initialTitle}
              post_to={post_to}
              topics={initialTopics}
              body={initialBody}
              storage={initialStorage}
              draftId={draftId}
              loading={loading}
              isUpdating={this.state.isUpdating}
              onUpdate={this.saveDraft}
              onSubmit={this.onSubmit}
              onDelete={this.onDelete}
              userPods={pods}
            />
            <div className="Write__bottom">
              <div
                className={`Write__left ${
                  Object.keys(draftPosts).length > 0 ? 'Write__left_half' : 'Write__left_full'
                }`}
              >
                <div className="Write__markdown">
                  <h4 className="SidebarContentBlock__title">
                    <i className="iconfont icon-write SidebarContentBlock__icon" />{' '}
                    <FormattedMessage id="markown" defaultMessage="Markdown" />
                  </h4>
                  <div className="Write__markdown_body">
                    <FormattedMessage id="markdown_supported" defaultMessage="Styling with Markdown supported" />
                    <a href="https://www.markdownguide.org/cheat-sheet/" target="_blank" rel="noopener noreferrer">
                      {' '}
                      Cheat Sheet
                    </a>
                  </div>
                </div>
                {commentList.length > 0 && (
                  <div>
                    <h3>
                      {this.props.intl.formatMessage({
                        id: 'pastComments',
                        defaultMessage: 'Past Comments',
                      })}
                    </h3>
                    <Feed content={commentList} feedType="comments" />
                  </div>
                )}
              </div>
              {Object.keys(draftPosts).length > 0 && (
                <div className="Write__right">
                  <LastDraftsContainer />
                </div>
              )}
            </div>
          </div>
          {this.state.showModalDelete && (
            <DeleteDraftModal
              draftIds={[draftId]}
              onDelete={this.onDeleteDraft}
              onCancel={this.handleCancelDeleteDraft}
            />
          )}
        </div>
      </div>
    );
  }
}

export default Write;
