import React from 'react';
import { shallow } from 'enzyme';
import UserWalletSummary from '../UserWalletSummary';

describe('(Component) UserWalletSummary', () => {
  describe('with default prop values', () => {
    it('renders and matches snapshot', () => {
      const props = {
        user: {
          balance: '100',
          vesting_shares: '0',
        },
        totalVestingShares: '100 WLS',
        totalVestingFundSteem: '100 WLS',
        loading: false,
        loadingGlobalProperties: false,
        voteWorth: 0,
        votePercent: 100,
        currentDailyBandwith: 0,
        dailyBandwidthLimit: 0,
        dailyBandwidthPercent: 0
      };
      const wrapper = shallow(<UserWalletSummary {...props} />);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
