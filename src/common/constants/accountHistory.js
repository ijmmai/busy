export const ACCOUNT_CREATE = 'account_create';
export const VOTE = 'vote';
export const ACCOUNT_UPDATE = 'account_update';
export const COMMENT = 'comment';
export const DELETE_COMMENT = 'delete_comment';
export const CUSTOM_JSON = 'custom_json';
export const FOLLOW = 'follow';
export const REBLOG = 'reblog';
export const CURATION_REWARD = 'curation_reward';
export const AUTHOR_REWARD = 'author_reward';
export const ACCOUNT_WITNESS_VOTE = 'account_witness_vote';
export const FILL_VESTING_WITHDRAW = 'fill_vesting_withdraw';

// Wallet Action Types
export const TRANSFER = 'transfer';
export const TRANSFER_TO_VESTING = 'transfer_to_vesting';
export const CLAIM_REWARD_BALANCE = 'claim_reward_balance';

// Filter Types - General
export const DOWNVOTED = 'downvoted';
export const UPVOTED = 'upvoted';
export const UNVOTED = 'unvoted';
export const FOLLOWED = 'followed';
export const UNFOLLOWED = 'unfollowed';
export const MUTED = 'muted';
export const REPLIED = 'replied';
export const REBLOGGED = 'reblogged';

// Filter Types - Finance
export const POWERED_UP = 'powered_up';
export const RECEIVED = 'received';
export const TRANSFERRED = 'transferred';
export const CLAIM_REWARDS = 'claim_rewards';

export const ID_FOLLOW = 'follow';

export const PARSED_PROPERTIES = [
  ACCOUNT_CREATE,
  VOTE,
  COMMENT,
  CUSTOM_JSON,
  CURATION_REWARD,
  AUTHOR_REWARD,
  TRANSFER,
  TRANSFER_TO_VESTING,
  CLAIM_REWARD_BALANCE,
  ACCOUNT_WITNESS_VOTE,
  FILL_VESTING_WITHDRAW,
];

export const PARSED_CUSTOM_JSON_IDS = [ID_FOLLOW];
