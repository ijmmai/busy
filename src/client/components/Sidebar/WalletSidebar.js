import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { openTransfer } from '../../wallet/walletActions';
import { getAuthenticatedUser } from '../../reducers';
import ClaimRewardsBlock from '../../wallet/ClaimRewardsBlock';
import DailyVestingReward from '../../wallet/DailyVestingReward';
import Promo from '../Promo/Promo';
import './WalletSidebar.less';

@withRouter
@injectIntl
@connect(
  state => ({
    user: getAuthenticatedUser(state),
  }),
  {
    openTransfer,
  },
)
class WalletSidebar extends React.Component {
  static propTypes = {
    user: PropTypes.shape(),
    isCurrentUser: PropTypes.bool,
    match: PropTypes.shape().isRequired,
    openTransfer: PropTypes.func.isRequired,
  };

  static defaultProps = {
    user: {},
    isCurrentUser: false,
  };

  handleOpenTransfer = () => {
    const { match, user, isCurrentUser } = this.props;
    const username = match.params.name === user.name || isCurrentUser ? '' : match.params.name;
    this.props.openTransfer(username);
  };

  render() {
    const { match, user, isCurrentUser } = this.props;
    const displayClaimRewards = match.params.name === user.name || match.path === '/status' || isCurrentUser;
    // const cryptos = [WLS.symbol];

    return (
      <div className="WalletSidebar">
        {displayClaimRewards && <ClaimRewardsBlock />}
        <DailyVestingReward />
        <Promo type="aside" />
      </div>
    );
  }
}

export default WalletSidebar;
