import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { Icon } from 'antd';
import { getUser } from '../reducers';
import faGift from '../../../assets/images/svgs/faGift.svg';
import './PodStyles.less';

const comingSoon = (
  <div className="coming-soon">
    <div className="ico">
      <Icon component={faGift}/>
    </div>
    <p>Coming Soon!</p>
  </div>
);

const PodToken = ({ pod, intl }) => (
  <div className="PodContainer">
    {comingSoon}
    {/*
    <p>
      {_.get(
        pod.json_metadata,
        'pod.token',
        intl.formatMessage({ id: 'no_token', defaultMessage: 'There is no token description.' }),
      )}
    </p>
    */}
  </div>
);

PodToken.propTypes = {
  intl: PropTypes.shape().isRequired,
  pod: PropTypes.shape().isRequired,
};

export default connect((state, ownProps) => ({
  pod: getUser(state, ownProps.match.params.name),
}))(injectIntl(PodToken));
