import React from 'react';
import { FormattedMessage } from 'react-intl';
import Topic from '../Button/Topic';
import './Topics.less';
import { FIXED_TAGS } from '../../../common/constants/settings';
import SidebarBlock from '../../app/Sidebar/SidebarBlock';

class Topics extends React.Component {

  render() {
    const content = <ul className="Interests__list">
                      {FIXED_TAGS.map(topic => (
                        <li key={topic}>
                          <Topic name={topic} favorite={false}/>
                        </li>
                      ))}
                    </ul>;
    const title   = <FormattedMessage id='browse_by_interest' defaultMessage='Browse by Interest'/>;

    return (
      <SidebarBlock
        content = {content}
        icon    = 'tag'
        title   = {title}
      />
    );
  }
}

export default Topics;
