import _ from 'lodash';
import { createAction } from 'redux-actions';
import { createAsyncActionType } from '../helpers/stateHelpers';
import { getAccountHistory, getDynamicGlobalProperties, isWalletTransaction } from '../helpers/apiHelpers';

export const CLAIM_REWARDS = createAsyncActionType('@wallet/CLAIM_REWARDS');
export const CLAIM_DAILY_REWARD = createAsyncActionType('@wallet/CLAIM_DAILY_REWARD');
export const OPEN_TRANSFER = '@wallet/OPEN_TRANSFER';
export const CLOSE_TRANSFER = '@wallet/CLOSE_TRANSFER';
export const GET_GLOBAL_PROPERTIES = createAsyncActionType('@wallet/GET_GLOBAL_PROPERTIES');
export const GET_VESTING_PAYOUT_FUND = createAsyncActionType('@wallet/GET_VESTING_PAYOUT_FUND');
export const GET_USER_ACCOUNT_HISTORY = createAsyncActionType('@users/GET_USER_ACCOUNT_HISTORY');
export const GET_MORE_USER_ACCOUNT_HISTORY = createAsyncActionType('@users/GET_MORE_USER_ACCOUNT_HISTORY');
export const UPDATE_ACCOUNT_HISTORY_FILTER = '@users/UPDATE_ACCOUNT_HISTORY_FILTER';
export const SET_INITIAL_CURRENT_DISPLAYED_ACTIONS = '@users/SET_INITIAL_CURRENT_DISPLAYED_ACTIONS';
export const ADD_MORE_ACTIONS_TO_CURRENT_DISPLAYED_ACTIONS = '@users/ADD_MORE_ACTIONS_TO_CURRENT_DISPLAYED_ACTIONS';
export const UPDATE_FILTERED_ACTIONS = '@users/UPDATE_FILTERED_ACTIONS';
export const LOADING_MORE_USERS_ACCOUNT_HISTORY = '@users/LOADING_MORE_USERS_ACCOUNT_HISTORY';
export const OPEN_WHALETOKEN = '@wallet/OPEN_WHALETOKEN';
export const CLOSE_WHALETOKEN = '@wallet/CLOSE_WHALETOKEN';

export const openTransfer = createAction(OPEN_TRANSFER);
export const closeTransfer = createAction(CLOSE_TRANSFER);
export const openWhaletoken = createAction(OPEN_WHALETOKEN);
export const closeWhaletoken = createAction(CLOSE_WHALETOKEN);

export const SET_SCATTER = createAsyncActionType('@wallet/SET_SCATTER');

export const setScatter = scatter => dispatch =>
  dispatch({
    type: SET_SCATTER.ACTION,
    payload: {
      promise: Promise.resolve(scatter),
    },
  });

const getParsedUserActions = userActions => {
  const userWalletTransactions = [];
  const userAccountHistory = [];

  if (userActions === undefined || userActions.length === 0) {
    return null;
  }

  _.each(userActions.reverse(), action => {
    const actionCount = action[0];
    const actionDetails = {
      ...action[1],
      actionCount,
    };
    const actionType = actionDetails.op[0];

    if (isWalletTransaction(actionType)) {
      userWalletTransactions.push(actionDetails);
    }

    let isReblog;
    try {
      if (JSON.parse(actionDetails.op[1].json)[0] === 'reblog') isReblog = true;
      else isReblog = false;
    } catch (e) {
      isReblog = false;
    }
    if (!isReblog) userAccountHistory.push(actionDetails);
  });

  return {
    userWalletTransactions,
    userAccountHistory,
  };
};

export const claimRewards = (whalestakeBalance) => (dispatch, getState, { steemAPI, Keys }) => {
  try {
    const { username, keys } = Keys.decrypt();
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}claimRewards` : keys.postingKey;
    return dispatch({
      type: CLAIM_REWARDS.ACTION,
      payload: {
        promise: steemAPI.chainLib.broadcast.claimRewardBalanceAsync(useKey, username, '0.000 WLS', whalestakeBalance),
      },
    });
  } catch (e) {
    return Promise.reject();
  }
};

export const claimDailyReward = (amount, to, memo) => (dispatch, getState, { steemAPI, Keys }) => {
  try {
    const { username, keys } = Keys.decrypt();
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}claimVestingReward` : keys.postingKey;

    const social_action = {
      amount: `${amount.toFixed(3)} WLS`,
      to,
      memo
    };
    if (_.isNil(to)) delete social_action.to;
    if (_.isNil(memo)) delete social_action.memo;

    const opt = [
      "social_action",
      {
        account: username,
        action: [ 3, social_action ]
      }
    ];
    const operations = [opt];

    return dispatch({
      type: CLAIM_DAILY_REWARD.ACTION,
      payload: {
        promise: steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ])
      },
    });
  } catch (e) {
    return Promise.reject();
  }
};

export const claimDailyRewardToTipBalance = (amount) => (dispatch, getState, { steemAPI, Keys }) => {
  try {
    const { username, keys } = Keys.decrypt();
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}claimVestingReward` : keys.postingKey;

    const opt = [
      "social_action",
      {
        account: username,
        action: [ 4, { amount: `${amount.toFixed(3)} WLS` } ]
      }
    ];
    const operations = [opt];

    return dispatch({
      type: CLAIM_DAILY_REWARD.ACTION,
      payload: {
        promise: steemAPI.chainLib.broadcast
          .sendAsync({ operations, extensions: [] }, [ useKey ])
      },
    });
  } catch (e) {
    return Promise.reject();
  }
};

export const getGlobalProperties = () => dispatch =>
  dispatch({
    type: GET_GLOBAL_PROPERTIES.ACTION,
    payload: {
      promise: getDynamicGlobalProperties(),
    },
  });

export const getUserAccountHistory = username => dispatch =>
  dispatch({
    type: GET_USER_ACCOUNT_HISTORY.ACTION,
    payload: {
      promise: getAccountHistory(username).then(userActions => {
        const parsedUserActions = getParsedUserActions(userActions);

        return {
          username,
          userWalletTransactions: parsedUserActions.userWalletTransactions,
          userAccountHistory: parsedUserActions.userAccountHistory,
        };
      }),
    },
  });

export const updateAccountHistoryFilter = createAction(UPDATE_ACCOUNT_HISTORY_FILTER);

export const setInitialCurrentDisplayedActions = createAction(SET_INITIAL_CURRENT_DISPLAYED_ACTIONS);

export const addMoreActionsToCurrentDisplayedActions = createAction(ADD_MORE_ACTIONS_TO_CURRENT_DISPLAYED_ACTIONS);
