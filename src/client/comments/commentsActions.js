import _ from "lodash";
import { Signature } from '@whaleshares/wlsjs/lib/auth/ecc';
import { createAction } from 'redux-actions';
import { createCommentPermlink, getBodyPatchIfSmaller } from '../vendor/steemitHelpers';
import { truncateDecimal } from '../helpers/formatter';
import { notify } from '../app/Notification/notificationActions';
import { getReplies } from '../feed/feedActions';
import { getBase64JsonContent, storeCNCContent, fetchOffChainContent, fetchStateOffChainContents } from '../CncApi';
import { sleep } from "../utils/misc";

const version = require('../../../package.json').version;

export const GET_COMMENTS = 'GET_COMMENTS';
export const GET_COMMENTS_START = 'GET_COMMENTS_START';
export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const GET_COMMENTS_ERROR = 'GET_COMMENTS_ERROR';

export const SEND_COMMENT = 'SEND_COMMENT';
export const SEND_COMMENT_START = 'SEND_COMMENT_START';
export const SEND_COMMENT_SUCCESS = 'SEND_COMMENT_SUCCESS';
export const SEND_COMMENT_ERROR = 'SEND_COMMENT_ERROR';

export const LIKE_COMMENT = '@comments/LIKE_COMMENT';
export const LIKE_COMMENT_START = '@comments/LIKE_COMMENT_START';
export const LIKE_COMMENT_SUCCESS = '@comments/LIKE_COMMENT_SUCCESS';
export const LIKE_COMMENT_ERROR = '@comments/LIKE_COMMENT_ERROR';

export const RELOAD_EXISTING_COMMENT = '@comments/RELOAD_EXISTING_COMMENT';

export const reloadExistingComment = createAction(RELOAD_EXISTING_COMMENT, undefined, data => ({
  commentId: data.id,
}));

const getRootCommentsList = apiRes =>
  Object.keys(apiRes.content)
    .filter(commentKey => apiRes.content[commentKey].depth === 1)
    .map(commentKey => apiRes.content[commentKey].id);

const getCommentsChildrenLists = apiRes => {
  const listsById = {};
  Object.keys(apiRes.content).forEach(commentKey => {
    listsById[apiRes.content[commentKey].id] = apiRes.content[commentKey].replies.map(
      childKey => apiRes.content[childKey].id,
    );
  });

  return listsById;
};

/**
 * Fetches comments from blockchain.
 * @param {number} postId Id of post to fetch comments from
 * @param {boolean} reload If set to true isFetching won't be set to true
 * preventing loading icon to be dispalyed
 * @param {object} focusedComment Object with author and permlink to which focus after loading
 */
export const getComments = (postId, reload = false, focusedComment = undefined) => (
  dispatch,
  getState,
  { busyAPI },
) => {
  const { posts, comments } = getState();
  const content = posts.list[postId] || comments.comments[postId];
  const { category, author, permlink } = content;

  dispatch({
    type: GET_COMMENTS,
    payload: {
      promise: busyAPI.sendAsync('database_api', 'get_state', [`/${category}/@${author}/${permlink}`])
        .then (result => fetchStateOffChainContents(result))
        .then(apiRes => ({
        rootCommentsList: getRootCommentsList(apiRes),
        commentsChildrenList: getCommentsChildrenLists(apiRes),
        content: apiRes.content,
      })),
    },
    meta: {
      id: postId,
      reload,
      focusedComment,
    },
  });
};

export const sendComment = (parentPost, body, storage, isUpdating = false, originalComment) =>
  async (dispatch, getState, {steemAPI, Keys}) => {
  try {
    const { category, id, permlink: parentPermlink, author: parentAuthor } = parentPost;
    const { auth } = getState();

    if (!auth.isAuthenticated) return dispatch(notify('You have to be logged in to comment', 'error'));
    if (!body || !body.length) return dispatch(notify("Message can't be empty", 'error'));

    dispatch({
      type: SEND_COMMENT_START,
      meta: {
        parentId: parentPost.id,
        isEditing: false,
        isReplyToComment: parentPost.id !== id
      }
    });
    await sleep(100);

    const { username, keys } = Keys.decrypt();
    const author = auth.user.name;
    const permlink = isUpdating ? originalComment.permlink : createCommentPermlink(parentAuthor, parentPermlink);
    const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}sendComment` : keys.postingKey;
    const is_cnc_storage = storage === 'cnc';
    let newBody = body;
    const jsonMetadata = { app: `wls/${version}`, tags: [category] }; // format: 'markdown'

    if (is_cnc_storage) {
      /**
       * 1. Upload the body content to CNC first
       * 2. Then put the hash to post body
       */

      jsonMetadata.storage = storage;
      const content_base64 = getBase64JsonContent(body);
      const buffer = Buffer.from(content_base64, 'utf8');
      let sig;
      if (steemAPI.chainLib.config.whalevault != null) {
        const response = await steemAPI.chainLib.config.whalevault.promiseRequestSignBuffer('wls_busy', `wls:${username}`,
          buffer.toString('utf8'), 'Posting', 'content_upload', 'raw');
        if (response.error != null) throw response.error;
        sig = Signature.fromHex(response.result).toBuffer().toString('base64');
      } else {
        sig = Signature.signBuffer(buffer, keys.postingKey).toBuffer().toString('base64');
      }

      newBody = await storeCNCContent(username, sig, content_base64);
    } else { // onchain
      newBody = isUpdating ? getBodyPatchIfSmaller(originalComment.body, body) : body;
    }

    const action_id = isUpdating ? 1 : 0;
    const social_action = [
      "social_action",
      {
        account: author,
        action: [
          action_id,
          {
            permlink,
            parent_author: parentAuthor,
            parent_permlink: parentPermlink,
            title: '',
            body: newBody,
            json_metadata: JSON.stringify(jsonMetadata),
          }
        ]
      }
    ];

    const operations = [social_action];
    const resp = await steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ]);

    const focusedComment = { author: resp.operations[0][1].author, permlink: resp.operations[0][1].permlink};
    dispatch(getComments(id, true, focusedComment));
    dispatch(getReplies());

    dispatch({ type: SEND_COMMENT_SUCCESS});
  } catch (err) {
    console.log(err);
    dispatch({type: SEND_COMMENT_ERROR, payload: {result: err.message}});
    dispatch(notify(err.message, 'error'));
    throw new Error("comment_operation_failed");
  }
};

export const likeComment = (commentId, amount, memo, onSuccess, retryCount = 0) => async (dispatch, getState, { steemAPI, Keys, busyAPI }) => {
  const { auth, comments, users } = getState();
  if (!auth.isAuthenticated) return;

  const { keys, hasWhaleVault } = Keys.decrypt();
  const tipper = auth.user.name;
  const { author, permlink } = comments.comments[commentId];
  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.postingKey}commentReward` : keys.postingKey;
  let enc_memo = memo.startsWith('#');
  if (enc_memo) {
    let author_pubkey = users.users && users.users['user-'+author] ? users.users['user-'+author].posting.key_auths[0][0] : "";
    if (author_pubkey == '') {
      let auth_result = await busyAPI.sendAsync('database_api', 'get_accounts', [[author]]);
      author_pubkey = auth_result[0].posting.key_auths[0][0];
    }
    if (hasWhaleVault) {
      let response = await steemAPI.chainLib.config.whalevault.promiseRequestEncryptMemo('wls_busy', 'wls:'+tipper, memo, 'posting', author_pubkey, 'stm', 'tip_memo');
      if (response.success) memo = response.result; else {
        dispatch(notify(response.message, 'error'));
        return;
      }
    } else {
      try {
        memo = steemAPI.chainLib.memo.encode(useKey, author_pubkey, memo);
      } catch (e) {
        dispatch(notify(e.message, 'error'));
        return;
      }
    }
  } 
  const operations = [["social_action", {
    account: tipper,
    action: [6, { // social_action_comment_tip
      author,
      permlink,
      amount: `${truncateDecimal(amount, 3).toFixed(3)} WLS`,
      memo
    }]
  }]];

  dispatch({
    type: LIKE_COMMENT,
    payload: {
      promise: steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ]).then(res => {
        if (onSuccess) onSuccess();
        steemAPI.sendAsync('database_api.get_content', [author, permlink])
          .then(data => fetchOffChainContent(data))
          .then(data => {
            dispatch(reloadExistingComment(data));
            dispatch(getReplies());
            return data;
          })
          .catch(err => {
            dispatch(notify(err.message, 'error'));
          });
        return res;
      }),
    },
    meta: { commentId, tipper, amount, isRetry: retryCount > 0 },
  }).catch(err => {
    if (err.res && err.res.status === 500 && retryCount <= 3) {
      dispatch(likeComment(commentId, amount, memo, onSuccess, retryCount + 1));
    } else {
      dispatch(notify(err.message, 'error'));
    }
  });
};
