import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';
import { Tabs, Modal, Icon } from 'antd';
import ReactionsList from './ReactionsList';
import faCoins from '../../../../assets/images/svgs/faCoins.svg';

class ReactionsModal extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    upVotes: PropTypes.arrayOf(PropTypes.shape()),
    onClose: PropTypes.func,
  };

  static defaultProps = {
    visible: false,
    upVotes: [],
    onOpen: () => {},
    onClose: () => {},
  };

  state = {
    visible: false,
  };

  render() {
    const { upVotes } = this.props;

    const tabs = [];

    if (upVotes.length > 0) {
      tabs.push(
        <Tabs.TabPane
          tab={
            <span>
              <Icon component={faCoins}/>
              <span style={{paddingLeft: '10px'}}>
                <FormattedNumber value={upVotes.length} />
              </span>
            </span>
          }
          key="1"
        >
          <ReactionsList votes={upVotes} />
        </Tabs.TabPane>,
      );
    }

    return (
      <Modal
        visible={this.props.visible && (upVotes.length > 0)}
        footer={null}
        onCancel={this.props.onClose}
      >
        <Tabs>{tabs}</Tabs>
      </Modal>
    );
  }
}

export default ReactionsModal;
