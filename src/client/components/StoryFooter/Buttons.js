import React from 'react';
import PropTypes from 'prop-types';
import take from 'lodash/take';
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl';
import { scroller } from 'react-scroll';
import { Link } from 'react-router-dom';
import { Icon, Popover } from 'antd';
import classNames from 'classnames';
import withAuthActions from '../../auth/withAuthActions';
import { sortTips } from '../../helpers/sortHelpers';
import BTooltip from '../BTooltip';
import PopoverMenu, { PopoverMenuItem } from '../PopoverMenu/PopoverMenu';
import ReactionsModal from '../Reactions/ReactionsModal';
import WLSDisplay from '../Utils/WLSDisplay';
import IconTip from '../../../../assets/images/svgs/IconTips.svg';
import { MOODS } from '../../../common/constants/settings';
import CopyModal from './CopyModal';
import './Buttons.less';

const Mood = (props) => {
 if (MOODS[props.mood]) {
  return (
    <span className="Buttons__mood" role="img" title={props.mood}>Mood <img src={"https://whaleshares.io/emojis/"+MOODS[props.mood]+".png"} alt={props.mood}/></span>
    );
 }
 return null;
};

@injectIntl
@withAuthActions
class Buttons extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    post: PropTypes.shape().isRequired,
    postState: PropTypes.shape().isRequired,
    onActionInitiated: PropTypes.func.isRequired,
    ownPost: PropTypes.bool,
    pendingLike: PropTypes.bool,
    pendingFollow: PropTypes.bool,
    pendingBookmark: PropTypes.bool,
    saving: PropTypes.bool,
    full: PropTypes.bool,
    onLikeClick: PropTypes.func,
    handlePostPopoverMenuClick: PropTypes.func,
  };

  static defaultProps = {
    ownPost: false,
    pendingLike: false,
    pendingFollow: false,
    pendingBookmark: false,
    saving: false,
    full: false,
    onLikeClick: () => {},
    handlePostPopoverMenuClick: () => {},
  };

  static handleCommentClick() {
    const form = document.getElementById('commentFormInput');
    if (form) {
      scroller.scrollTo('commentFormInputScrollerElement', {
        offset: 50,
      });
      form.focus();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      loadingEdit           : false,
      reactionsModalVisible : false,
      sortedTips            : this.props.post.comment_tips.sort(sortTips),
      sourceVisible         : false,
    };

    this.handleCloseReactions = this.handleCloseReactions.bind(this);
    this.handleLikeClick      = this.handleLikeClick.bind(this);
    this.handleShowReactions  = this.handleShowReactions.bind(this);
    this.toggleSource         = this.toggleSource.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.post.comment_tips !== this.props.post.comment_tips) {
      let tips = this.props.post.comment_tips;
  
      if (tips.length > 1) {
        tips = tips.sort(sortTips);
        this.setState({sortedTips:tips});
      }

    }   
  }

  handleLikeClick() {
    if (!this.props.hasTipBalance) return false;
    this.props.onActionInitiated(this.props.onLikeClick);
  }

  handleShowReactions() {
    this.setState({
      reactionsModalVisible: true,
    });
  }

  handleCloseReactions() {
    this.setState({
      reactionsModalVisible: false,
    });
  }

  toggleSource() {
    this.setState({
      sourceVisible: !this.state.sourceVisible,
    });
  }

  renderPostPopoverMenu() {
    const {
      full,
      handlePostPopoverMenuClick,
      intl,
      ownPost,
      pendingBookmark,
      pendingFollow,
      post,
      postState,
      saving,
    } = this.props;
    const { sourceVisible, sortedTips } = this.state;

    let followText = '';

    if (postState.userFollowed && !pendingFollow) {
      followText = intl.formatMessage(
        { id: 'unfollow_username', defaultMessage: 'Unfollow {username}' },
        { username: post.author },
      );
    } else if (postState.userFollowed && pendingFollow) {
      followText = intl.formatMessage(
        { id: 'unfollow_username', defaultMessage: 'Unfollow {username}' },
        { username: post.author },
      );
    } else if (!postState.userFollowed && !pendingFollow) {
      followText = intl.formatMessage(
        { id: 'follow_username', defaultMessage: 'Follow {username}' },
        { username: post.author },
      );
    } else if (!postState.userFollowed && pendingFollow) {
      followText = intl.formatMessage(
        { id: 'follow_username', defaultMessage: 'Follow {username}' },
        { username: post.author },
      );
    }

    let BTooltipText = [];
    let popoverMenu = [];

    if (ownPost && post.cashout_time !== '1969-12-31T23:59:59') {
      popoverMenu = [
        ...popoverMenu,
        <PopoverMenuItem key="edit">
          {saving ? <Icon type="loading" /> : <i className="iconfont icon-write" />}
          <FormattedMessage id="edit_post" defaultMessage="Edit post" />
        </PopoverMenuItem>,
      ];
      BTooltipText.push('Edit');
    }

    if (!ownPost) {
      popoverMenu = [
        ...popoverMenu,
        <PopoverMenuItem key="follow" disabled={pendingFollow}>
          {pendingFollow ? <Icon type="loading" /> : <i className="iconfont icon-people" />}
          {followText}
        </PopoverMenuItem>,
      ];
      BTooltipText.push(followText);
    }

    popoverMenu = [
      ...popoverMenu,
      <PopoverMenuItem key="save">
        {pendingBookmark ? <Icon type="loading" /> : <i className="iconfont icon-collection" />}
        <FormattedMessage
          id={postState.isSaved ? 'unsave_post' : 'save_post'}
          defaultMessage={postState.isSaved ? 'Unsave post' : 'Save post'}
        />
      </PopoverMenuItem>,
    ];
    BTooltipText.push('Bookmark');

    if (ownPost) {
      popoverMenu.push(
        <PopoverMenuItem key="copy">
          <span onClick={this.toggleSource} role="presentation">
            <i className="iconfont icon-document" />
            <FormattedMessage id="see_source" defaultMessage="See source" />
          </span>
          <CopyModal
            visible={sourceVisible}
            source={post.body}
            title={intl.formatMessage({ id: 'see_source', defaultMessage: 'See source' })}
            copy={intl.formatMessage({ id: 'copy', defaultMessage: 'Copy source' })}
            onCopy={this.toggleSource}
          />
        </PopoverMenuItem>,
      );
      BTooltipText.push('See Source');
    }

    return (
      <BTooltip title={BTooltipText.join(', ')}>
        <Popover
          placement="bottomRight"
          trigger="click"
          content={
            <PopoverMenu onSelect={handlePostPopoverMenuClick} bold={false}>
              {popoverMenu}
            </PopoverMenu>
          }
        >
          <i className="Buttons__post-menu iconfont icon-more" />
        </Popover>
      </BTooltip>
    );
  }

  render() {
    const { intl, post, postState, pendingLike, full, ownPost, mood, hasTipBalance } = this.props;
    const { sortedTips } = this.state;

    const upVotes         = sortedTips;
    const totalPayout     = parseFloat(post.total_payout_value);
    const emoji           = MOODS.mood ? url+MOODS.mood: '';
    const upVotesPreview  = take(upVotes, 10).map(vote => (
                              <p key={vote.tipper}>
                                <Link to={`/@${vote.tipper}`}>{vote.tipper}</Link>

                                {parseFloat(vote.amount) > 0 && (
                                  <span style={{ opacity: '0.5' }}>
                                    {' '}
                                    <WLSDisplay value={parseFloat(vote.amount)} />{' '}
                                  </span>
                                )}
                              </p>
                            ));
    const upVotesDiff     = upVotes.length - upVotesPreview.length;
    const upVotesMore     = upVotesDiff > 0 && (
                              <p>
                                <a role="presentation" onClick={this.handleShowReactions}>
                                  <FormattedMessage id="and_more_amount" defaultMessage="and {amount} more" values={{ amount: upVotesDiff }} />
                                </a>
                              </p>
                            );

    const likeClass       = classNames({ active: postState.isLiked, Buttons__link: true });

    const commentsLink    = post.url.indexOf('#') !== -1 ? post.url : { pathname: post.url, hash: '#comments-anchor' };

    let likeTooltip       = <span>{intl.formatMessage({ id: 'like' })}</span>;

    if (postState.isLiked) {
      likeTooltip = <span>{intl.formatMessage({ id: 'like_more' })}</span>;
    }

    if (!hasTipBalance) likeTooltip = <span>Top up your Tip Balance first</span>

    return (
      <div className="Buttons">
        {full && !ownPost && (
          <BTooltip title={likeTooltip}>
            <a role="presentation" className={likeClass} onClick={this.handleLikeClick}>
              {pendingLike ? (
                <Icon type="loading" />
              ) : (
                <Icon
                  component={IconTip}
                  className={`iconfont icon-${this.state.sliderVisible ? 'right' : 'tip'}`}
                />
              )}
            </a>
          </BTooltip>
        )}
        {upVotes.length > 0 && (
          <span
            className={classNames('Buttons__number', {
              'Buttons__reactions-count': false,
              Buttons__shared: postState.isLiked,
            })}
            role="presentation"
            onClick={this.handleShowReactions}
          >
            <BTooltip
              title={
                <div>
                  {upVotesPreview}
                  {upVotesMore}
                </div>
              }
            >
              <FormattedNumber value={upVotes.length} />{' '}
              {upVotes.length === 1 ? (
                <FormattedMessage id="sharesSingular" defaultMessage="Share" />
              ) : (
                <FormattedMessage id="sharesPlural" defaultMessage="Shares" />
              )}
              <span />
            </BTooltip>
          </span>
        )}
        <BTooltip title={intl.formatMessage({ id: 'comment', defaultMessage: 'Comment' })}>
          <Link className="Buttons__link" to={commentsLink} onClick={this.handleCommentClick}>
            <i className="iconfont icon-message" />
          </Link>
        </BTooltip>
        <span className="Buttons__number">{post.children > 0 && <FormattedNumber value={post.children} />}</span>
        {this.renderPostPopoverMenu()}
        <ReactionsModal
          visible={this.state.reactionsModalVisible}
          upVotes={upVotes}
          onClose={this.handleCloseReactions}
        />
        {mood && (< Mood mood={mood} />)}
      </div>
    );
  }
}
export default Buttons;
