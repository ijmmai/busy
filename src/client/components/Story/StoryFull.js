import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import readingTime from 'reading-time';
import {
  injectIntl,
  FormattedMessage,
  FormattedRelative,
  FormattedDate,
  FormattedTime,
} from 'react-intl';
import { Link } from 'react-router-dom';
import Lightbox from 'react-image-lightbox';
import { Scrollbars } from 'react-custom-scrollbars';
import { getFromMetadata, extractImageTags } from '../../helpers/parser';
import { isPostDeleted, dropCategory } from '../../helpers/postHelpers';
import withAuthActions from '../../auth/withAuthActions';
import { getProxyImageURL } from '../../helpers/image';
import { Tooltip } from 'antd';
import Body, { getHtml } from './Body';
import StoryDeleted from './StoryDeleted';
import StoryFooter from '../StoryFooter/StoryFooter';
import Avatar from '../Avatar';
import Topic from '../Button/Topic';
import PostFeedEmbed from './PostFeedEmbed';
import PodAvatar from '../../pod/PodAvatar';
import './StoryFull.less';
import SignupButton from '../SignupButton.js';

@injectIntl
@withAuthActions
class StoryFull extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    post: PropTypes.shape().isRequired,
    postState: PropTypes.shape().isRequired,
    onActionInitiated: PropTypes.func.isRequired,
    signature: PropTypes.string,
    rewriteLinks: PropTypes.bool,
    pendingLike: PropTypes.bool,
    pendingFollow: PropTypes.bool,
    pendingBookmark: PropTypes.bool,
    commentCount: PropTypes.number,
    saving: PropTypes.bool,
    ownPost: PropTypes.bool,
    onFollowClick: PropTypes.func,
    onSaveClick: PropTypes.func,
    onLikeClick: PropTypes.func,
    onEditClick: PropTypes.func,
  };

  static defaultProps = {
    signature: null,
    rewriteLinks: false,
    pendingLike: false,
    pendingFollow: false,
    pendingBookmark: false,
    commentCount: 0,
    saving: false,
    ownPost: false,
    onFollowClick: () => {},
    onSaveClick: () => {},
    onLikeClick: () => {},
    onEditClick: () => {},
    postState: {},
  };

  constructor(props) {
    super(props);

    this.state = {
      lightbox: {
        open: false,
        index: 0,
      },
    };

    this.images = [];
    this.imagesAlts = [];

    this.handleClick = this.handleClick.bind(this);
    this.handleContentClick = this.handleContentClick.bind(this);
  }

  componentDidMount() {
    document.body.classList.add('white-bg');
  }

  componentWillUnmount() {
    const { post } = this.props;
    const hideWhiteBG = document && document.location.pathname !== dropCategory(post.url);
    if (hideWhiteBG) {
      document.body.classList.remove('white-bg');
    }
  }

  clickMenuItem(key) {
    const { post } = this.props;

    switch (key) {
      case 'follow':
        this.props.onFollowClick(post);
        break;
      case 'save':
        this.props.onSaveClick(post);
        break;
      case 'edit':
        this.props.onEditClick(post);
        break;
      default:
    }
  }

  handleClick(key) {
    this.props.onActionInitiated(this.clickMenuItem.bind(this, key));
  }

  handleContentClick(e) {
    if (e.target.tagName === 'IMG' && this.images) {
      const tags = this.contentDiv.getElementsByTagName('img');
      for (let i = 0; i < tags.length; i += 1) {
        if (tags[i] === e.target && this.images.length > i) {
          if (e.target.parentNode && e.target.parentNode.tagName === 'A') return;
          this.setState({
            lightbox: {
              open: true,
              index: i,
            },
          });
        }
      }
    }
  }

  renderDtubeEmbedPlayer() {
    const { post } = this.props;
    const parsedJsonMetaData = _.attempt(JSON.parse, post.json_metadata);

    if (_.isError(parsedJsonMetaData)) {
      return null;
    }

    const video = getFromMetadata(post.json_metadata, 'video');
    const isDtubeVideo = _.has(video, 'content.videohash') && _.has(video, 'info.snaphash');

    if (isDtubeVideo) {
      const videoTitle = _.get(video, 'info.title', '');
      const author = _.get(video, 'info.author', '');
      const permlink = _.get(video, 'info.permlink', '');
      const dTubeEmbedUrl = `https://emb.d.tube/#!/${author}/${permlink}/true`;
      const dTubeIFrame = `<iframe width="100%" height="340" src="${dTubeEmbedUrl}" title="${videoTitle}" allowFullScreen></iframe>`;
      const embed = {
        type: 'video',
        provider_name: 'DTube',
        embed: dTubeIFrame,
        thumbnail: getProxyImageURL(true, post.author, post.permlink, `https://ipfs.io/ipfs/${video.info.snaphash}`, 'preview'),
      };
      return <PostFeedEmbed embed={embed} />;
    }

    return null;
  }

  render() {
    const {
      // intl,
      user,
      post,
      postState,
      signature,
      rewriteLinks,
      pendingLike,
      pendingFollow,
      pendingBookmark,
      // commentCount,
      saving,
      ownPost,
      onLikeClick,
      onEditClick,
    } = this.props;

    const { open, index } = this.state.lightbox;

    let signedBody = post.body;
    if (signature) {
      signedBody = `${post.body}<hr>${signature}`;
    }

    const parsedBody = getHtml(true, post.author, post.permlink, signedBody, {}, 'text');

    this.images = extractImageTags(parsedBody);

    const tags = _.union([post.category], getFromMetadata(post.json_metadata, 'tags')).splice(0,5);
    const mood = getFromMetadata(post.json_metadata, 'mood');

    // let followText = '';

    // if (postState.userFollowed && !pendingFollow) {
    //   followText = intl.formatMessage(
    //     { id: 'unfollow_username', defaultMessage: 'Unfollow {username}' },
    //     { username: post.author },
    //   );
    // } else if (postState.userFollowed && pendingFollow) {
    //   followText = intl.formatMessage(
    //     { id: 'unfollow_username', defaultMessage: 'Unfollow {username}' },
    //     { username: post.author },
    //   );
    // } else if (!postState.userFollowed && !pendingFollow) {
    //   followText = intl.formatMessage(
    //     { id: 'follow_username', defaultMessage: 'Follow {username}' },
    //     { username: post.author },
    //   );
    // } else if (!postState.userFollowed && pendingFollow) {
    //   followText = intl.formatMessage(
    //     { id: 'follow_username', defaultMessage: 'Follow {username}' },
    //     { username: post.author },
    //   );
    // }

    let replyUI = null;

    if (post.depth !== 0) {
      replyUI = (
        <div className="StoryFull__reply">
          <h3 className="StoryFull__reply__title">
            <FormattedMessage
              id="post_reply_title"
              defaultMessage="This is a reply to: {title}"
              values={{ title: post.root_title }}
            />
          </h3>
          <h4>
            <Link to={dropCategory(post.url)}>
              <FormattedMessage id="post_reply_show_original_post" defaultMessage="Show original post" />
            </Link>
          </h4>
          {post.depth > 1 && (
            <h4>
              <Link to={`/@${post.parent_author}/${post.parent_permlink}`}>
                <FormattedMessage id="post_reply_show_parent_discussion" defaultMessage="Show parent discussion" />
              </Link>
            </h4>
          )}
        </div>
      );
    }

    let content = null;
    if (isPostDeleted(post)) {
      content = <StoryDeleted />;
    } else {
      content = (
        <div
          role="presentation"
          ref={div => {
            this.contentDiv = div;
          }}
          onClick={this.handleContentClick}
        >
          {this.renderDtubeEmbedPlayer()}
          <Body is_post_proxy author={post.author} permlink={post.permlink} full rewriteLinks={rewriteLinks} body={signedBody} json_metadata={post.json_metadata} />
        </div>
      );
    }

    return (
      <div className="StoryFull">
        {replyUI}
        <h1 className="StoryFull__title">{post.title}</h1>
        <div className="StoryFull__header">
          <Link to={`/@${post.author}`}>
            <Avatar username={post.author} size={60} />
          </Link>
          <div className="StoryFull__header__text">
            <Link to={`/@${post.author}`}>
              <span className="username">{post.author}</span>
            </Link>
            <span className="StoryFull__header__text__date">
              <FormattedMessage id="posted" defaultMessage="Posted" />{' for '}

                {post.pod && (<Link to={`/@${post.pod}`}><span className='audience-podpost'>{post.pod} Pod Members</span></Link>)}
                {post.allow_friends && (<span className="audience-friends">Friends</span>)} 
                {!post.pod && !post.allow_friends && (<span className="audience-public">Everyone</span>)}
                {' to comment on, '}
              <Tooltip
                title={
                  <React.Fragment>
                    <FormattedDate value={`${post.created}Z`} /> <FormattedTime value={`${post.created}Z`} />
                  </React.Fragment>
                }
              >
                <span>
                  <FormattedRelative value={`${post.created}Z`} />
                </span>
              </Tooltip>
            </span>

            {Math.ceil(readingTime(post.body).minutes) > 1 && (
              <span>
                <span className="StoryFull__bullet" />
                <Tooltip
                  title={
                    <span>
                      <FormattedMessage
                        id="words_tooltip"
                        defaultMessage={'{words} words'}
                        values={{ words: readingTime(post.body).words }}
                      />
                    </span>
                  }
                >
                  <span className="StoryFull__header__reading__time">
                    <FormattedMessage
                      id="reading_time_post"
                      defaultMessage={'{min} min read'}
                      values={{ min: Math.ceil(readingTime(post.body).minutes) }}
                    />
                  </span>
                </Tooltip>
              </span>
            )}
          </div>
        </div>
        <div className="StoryFull__content">{content}</div>
        {open && (
          <Lightbox
            imageTitle={this.images[index].alt}
            mainSrc={getProxyImageURL(true, post.author, post.permlink, this.images[index].src)}
            nextSrc={getProxyImageURL(true, post.author, post.permlink, this.images[(index + 1) % this.images.length].src)}
            prevSrc={getProxyImageURL(true, post.author, post.permlink, this.images[(index + (this.images.length - 1)) % this.images.length].src)}
            onCloseRequest={() => {
              this.setState({
                lightbox: {
                  ...this.state.lightbox,
                  open: false,
                },
              });
            }}
            onMovePrevRequest={() =>
              this.setState({
                lightbox: {
                  ...this.state.lightbox,
                  index: (index + (this.images.length - 1)) % this.images.length,
                },
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                lightbox: {
                  ...this.state.lightbox,
                  index: (index + (this.images.length + 1)) % this.images.length,
                },
              })
            }
          />
        )}
        <div className="StoryFull__topics">
          <Scrollbars
            universal
            autoHide
            renderView={({ style, ...props }) => <div style={{ ...style, marginBottom: '-20px' }} {...props} />}
            style={{ width: '100%', height: 46 }}
          >
            <div className="StoryFull__topics__content">
              {_.uniq(tags)
                .filter(_.isString)
                .map(tag => (
                  <Topic key={tag} name={tag} />
                ))}
              <div style={{ flex: '0 0 20px' }} />
            </div>
          </Scrollbars>
        </div>
        <StoryFooter
          full
          user={user}
          post={post}
          postState={postState}
          pendingLike={pendingLike}
          pendingFollow={pendingFollow}
          pendingBookmark={pendingBookmark}
          ownPost={ownPost}
          saving={saving}
          onLikeClick={onLikeClick}
          handlePostPopoverMenuClick={this.handleClick}
          onEditClick={onEditClick}
          mood={mood}
        />
        {!user.name && (
          <div>
            <center>
              <i><b>
                Sign Up to join this conversation, or to start a topic of your own.<br/>
                Your opinion is celebrated and welcomed, not banned or censored!
              </b></i>
              <br/><br/>
              <SignupButton text="SIGN UP NOW!" />
            </center>
          </div>
        )}
        <a id='comments-anchor'></a>
      </div>
    );
  }
}

export default StoryFull;
