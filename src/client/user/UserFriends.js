import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Table } from "antd";
import Avatar from '../components/Avatar';
import { fetchUserFriends } from '../friends/actions';
import ReduxInfiniteScroll from "../vendor/ReduxInfiniteScroll";
import Loading from "../components/Icon/Loading";
import '../styles/modules/account_list.less';

class UserFriends extends React.Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);

    this.state = {
      username    : false,
      friends     : false,
      hasMore     : false,
      loadingMore : false,
    };
  }

  componentDidMount() {
    const { match } = this.props;
    const username  = match.params.name;

    const onSuccess = (data) => {
      this.setState({
        friends     : data,
        hasMore     : (data.length >= 20),
        loadingMore : false,
      }, () => {if(this.state.hasMore){this.handleLoadMore();}});
    };

    const onError = (err) => {
      this.setState({loadingMore: false});
    };

    if ((username !== this.state.username) || (this.state.friends === false)) {
      this.setState({
        username,
        friends     : false,
        hasMore     : false,
        loadingMore : false,
      });
      this.props.fetchUserFriends(username, onSuccess, onError);
    }
  }

  handleLoadMore = () => {
    const { match } = this.props;
    const username  = match.params.name;

    this.setState({loadingMore: true});

    const onSuccess = (data) => {
      data.shift(); // remove the first item of an array to avoid duplication
      this.setState({
        friends     : [...this.state.friends, ...data],
        hasMore     : (data.length >= 20),
        loadingMore : false,
      });
    };

    const onError = (err) => {
      this.setState({loadingMore: false});
    };

    const lastItem = this.state.friends[this.state.friends.length - 1];
    this.props.loadMore(username, lastItem, onSuccess, onError);
  };

  render() {
    const {friends, hasMore, loadingMore} = this.state;
    const columns = [
      {
        title     : '',
        dataIndex : 'account',
        key       : 'avatar',
        width     : 1,
        render    : (text, record) => <Avatar size={32} username={record.account} />
      },
      {
        title     : 'Name',
        dataIndex : 'account',
        key       : 'account',
        className : 'account',
        render    : (text, record) => <Link to={`/@${record.account}`}>{text}</Link>,
      },
    ];

    let data = [];
    if (friends) {
      data = friends.map((item) => {
          return { key: item, account: item };
        }
      );
    }

    return (
      <div className="main-body">
        <div className="main-content AccList UseList">
          {this.state.friends &&
            <ReduxInfiniteScroll
              hasMore             = {hasMore}
              loadingMore         = {loadingMore}
              loader              = {<Loading/>}
              loadMore            = {this.handleLoadMore}
              elementIsScrollable = {false}
              items               = {[<Table 
                                        key        = 'friends'
                                        pagination = {false}
                                        columns    = {columns}
                                        dataSource = {data}
                                      />
                                    ]}
            />
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
  };
};
const mapDispatchToProps = {
  fetchUserFriends,
  loadMore: (username, lastItem, onSuccess, onError) => async (dispatch, getState, {busyAPI}) => {
    try {
      const data = await busyAPI.sendAsync('database_api', 'get_friends', [username, lastItem, 20]);
      if (onSuccess) onSuccess(data);
    } catch (e) {
      if (onError) onError(e);
    }
  },
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserFriends);
