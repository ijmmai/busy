const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const configUtils = require('./configUtils');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const baseDir = path.resolve(__dirname, '..');

module.exports = {
  mode: process.env.NODE_ENV || 'production',
  name: 'server',
  target: 'node',
  node: {
    __filename: true,
    __dirname: true,
  },
  entry: {
    server: [
      require.resolve('core-js'),
      require.resolve('regenerator-runtime/runtime'),
      path.resolve(baseDir, './src/server/index.js'),
    ],
  },
  externals: [
    nodeExternals({
      // we still want imported css from external files to be bundled otherwise 3rd party packages
      // which require us to include their own css would not work properly
      whitelist: /\.css$/,
    }),
  ],
  output: {
    filename: 'busy.server.js',
  },
  resolve: {
    extensions: ['.js', '.mjs', '.json', '.jsx', '.css'],
    modules: [
      'src',
      'node_modules'
    ],
  },
  module: {
    rules: [
      {
        test: configUtils.MATCH_JS_JSX,
        exclude: [
          /node_modules/,
          /webpack/,
          /dist/
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  "modules": false
                }
              ],
              "@babel/preset-react"
            ],
            plugins: [
              [
                "@babel/plugin-proposal-decorators",
                {
                  "legacy": true
                }
              ],
              [
                "@babel/plugin-transform-runtime",
                {
                  "corejs": 2
                }
              ],
              "@babel/plugin-syntax-dynamic-import",
              [
                "import",
                {
                  "libraryName": "antd",
                  "libraryDirectory": "lib",
                  "style": false
                }
              ],
            ],
            cacheDirectory: true,
          },
        },
      },
      {
        test: configUtils.MATCH_CSS_LESS,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                require('autoprefixer')({
                  browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                  ],
                }),
              ],
            },
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true
            },
          },
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      }
    ],
  },
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/\.(css|less)$/, 'identity-obj-proxy'),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        IS_BROWSER: JSON.stringify(false),
        WLS_RPC_URL: JSON.stringify(process.env.WLS_RPC_SERVER_URL || 'https://pubrpc.whaleshares.io'),
        WALLET_URL: JSON.stringify(process.env.WALLET_URL || 'https://wallet.whaleshares.io'),
        EXPLORER_URL: JSON.stringify(process.env.EXPLORER_URL || 'https://explorer.whaleshares.io'),
      },
    }),
  ],
};
