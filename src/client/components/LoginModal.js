import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import IconLogo from '../../../assets/images/svgs/IconLogo';
import { SIGNUP_URL } from '../../common/constants/settings';
import './LoginModal.less';

class LoginModal extends React.Component {
  static propTypes = {
    handleLoginModalCancel: PropTypes.func,
    visible: PropTypes.bool,
  };

  static defaultProps = {
    handleLoginModalCancel: () => {},
    visible: false,
  };

  constructor(props) {
    super(props);
    this.handleSignup = this.handleSignup.bind(this);
  }

  handleSignup() {
    if (typeof window !== 'undefined') {
      window.open(SIGNUP_URL);
      this.props.handleLoginModalCancel();
    }
  }

  render() {
    const { handleLoginModalCancel, visible } = this.props;

    return (
      <Modal
        title=""
        visible={visible}
        onCancel={handleLoginModalCancel}
        footer={
          <div className="LoginModal__footer">
            <FormattedMessage
              id="login_modal_footer_text"
              defaultMessage="Don't have an account? Signup with {link}"
              values={{
                link: (
                  <a role="presentation" onClick={this.handleSignup}>
                    whaleshares.io
                  </a>
                ),
              }}
            />
          </div>
        }
      >
        <div className="LoginModal__body">
          <Icon component={IconLogo} className="LoginModal__icon" />
          <span className="LoginModal__login-title">
            <FormattedMessage id="login_to_busy" defaultMessage="Login" />
          </span>
          <span className="LoginModal__login-description">
            <FormattedMessage id="login_modal_description" defaultMessage="Login with your Whaleshares account" />
          </span>
          <Link className="LoginModal__login-button" to="/login">
            Login
          </Link>
        </div>
      </Modal>
    );
  }
}

export default withRouter(LoginModal);
